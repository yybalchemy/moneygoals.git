from typing import List
import json

from alibabacloud_dysmsapi20170525.client import Client as Dysmsapi20170525Client
from alibabacloud_tea_openapi import models as open_api_models
from alibabacloud_dysmsapi20170525 import models as dysmsapi_20170525_models
from alibabacloud_tea_util import models as util_models
from app.common.config import Config

def create_client() -> Dysmsapi20170525Client:
        config = open_api_models.Config(
            access_key_id=Config.access_key_id,
            access_key_secret=Config.access_key_secret
        )
        config.endpoint = f'dysmsapi.aliyuncs.com'
        return Dysmsapi20170525Client(config)

async def send_sms(
    phoneNum: str,
    params: dict
) -> None:
    client = create_client()
    send_sms_request = dysmsapi_20170525_models.SendSmsRequest(
        phone_numbers=phoneNum,
        sign_name=Config.sms_template_sign,
        template_code=Config.sms_template_code,
        template_param=json.dumps(params),
        out_id=0
    )
    try:
        res = await client.send_sms_with_options_async(send_sms_request, util_models.RuntimeOptions())
        res_body = res.to_map()['body']
        code = res_body['Code']
        message = res_body['Message']
        print(message)
        return code
    except Exception as error:
        return error.message
