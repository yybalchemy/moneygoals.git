
import os
from pathlib import Path
from dotenv import load_dotenv

dotenv_path=Path(".") / "../.env"
load_dotenv(dotenv_path)

class Config ():
    db_name = os.environ.get("db_name", "")
    db_user = os.environ.get("db_user", "")
    db_password = os.environ.get("db_password", "")
    db_host = os.environ.get("db_host", "")
    db_port = os.environ.get("db_port", "")
    db_url = f"mysql+aiomysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"

    redis_port = os.environ.get("redis_port", "")
    redis_password = os.environ.get("redis_password", "")
    
    db_migrate_url = f"mysql+pymysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}"
    
    sign_key = os.environ.get("sign_key", "")
    
    sms_template_sign = os.environ.get("sms_template_sign", "")
    sms_template_code = os.environ.get("sms_template_code", "")
    access_key_id = os.environ.get("access_key_id", "")
    access_key_secret = os.environ.get("access_key_secret", "")