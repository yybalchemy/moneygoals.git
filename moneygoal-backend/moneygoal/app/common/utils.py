
from passlib.context import CryptContext
import time, random, string, uuid

passsword_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def generate_password_hash(plain_password):
    return passsword_context.hash(plain_password)


def verify_password(plain_password, hash_password):
    return passsword_context.verify(plain_password, hash_password)


def get_random_str(
    length: int,
    is_digit: bool = True,
):
    if is_digit:
        all_char = string.digits
    else:
        all_char = string.ascii_letters + string.digits
    return "".join(random.sample(all_char, length))


def create_any_id():
    localtime = time.localtime(time.time())
    timestr_part1 = time.strftime('%Y', localtime)[2:4]
    timestr_part2 = time.strftime('%m', localtime)
    timestr_part3 = time.strftime('%d', localtime)
    timestr_part4 = time.strftime('%H', localtime)
    timestr_part5 = time.strftime('%M', localtime)
    timestr_part6 = time.strftime('%S', localtime)

    timestr_part7 = int(timestr_part4) + int(timestr_part5)
    timestr_part7 = str(timestr_part7)

    if int(timestr_part6) % 2 == 0:
        timestr_part1,timestr_part2 = timestr_part2,timestr_part1
    elif int(timestr_part6) % 3 == 0:
        timestr_part2,timestr_part3 = timestr_part3,timestr_part2
    elif int(timestr_part6) % 5 == 0:
        timestr_part1,timestr_part3 = timestr_part3,timestr_part1

    uid = random_num(1) + timestr_part2 + random_num(1) + timestr_part1 + random_num(1) + timestr_part7 + timestr_part3 + random_num(1) 
    return uid


def random_str(length):
    usable_names_char = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    username = []
    for i in range(length):
        username.append(random.choice(usable_names_char))
    return ''.join(username)


def generate_token():
    return uuid.uuid4().hex


def time_identifier():
    return time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))


def random_num(length) :
    captcha = []
    idx = 0
    while idx < length:
        temp = random.randint(0, 9)
        captcha.append(str(temp))
        idx += 1
    return ''.join(captcha)