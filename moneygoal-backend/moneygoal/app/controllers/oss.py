
from fastapi import APIRouter

from aliyunsdkcore import client
from aliyunsdksts.request.v20150401 import AssumeRoleRequest
import json
import oss2

from app.common.oss import RegionID, EndPoint, AccessKeyId, AccessKeySecret, BucketName

router = APIRouter()

@router.get('/auth')
def auth(device_info: str):
    role_arn = ''

    clt = client.AcsClient('', '', RegionID)
    req = AssumeRoleRequest.AssumeRoleRequest()
    req.set_accept_format('json')
    req.set_RoleArn(role_arn)
    req.set_RoleSessionName(device_info)
    body = clt.do_action_with_exception(req)
    token = json.loads(oss2.to_unicode(body))

    return {
        'StatusCode':'200',
        'AccessKeyId':token['Credentials']['AccessKeyId'],
        'AccessKeySecret':token['Credentials']['AccessKeySecret'],
        'SecurityToken':token['Credentials']['SecurityToken'],
        'Expiration':token['Credentials']['Expiration']
    }
    