
from datetime import datetime
from typing import Optional
from sqlmodel import Field, SQLModel
from sqlalchemy import Column, DateTime

class BaseModel(SQLModel):
    is_delete: bool = Field(default=False)
    # id: uuid.UUID = Field(default_factory=uuid.uuid4, primary_key=True)
    # id: Optional[int] = Field(default=None, primary_key=True)
    id: int = Field(primary_key=True)
    created_at: Optional[datetime] = Field(
        sa_column=Column(DateTime(timezone=True), default=datetime.utcnow)
    )
    updated_at: Optional[datetime] = Field(
        sa_column=Column(
            DateTime(timezone=True), onupdate=datetime.utcnow, default=datetime.utcnow
        )
    )