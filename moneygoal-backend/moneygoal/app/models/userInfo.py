
from typing import Optional
from app.models.base import BaseModel
from app.common.utils import create_any_id

class UserInfo(BaseModel, table=True):
    uid: Optional[str]
    info_id: Optional[str]
    app_version: Optional[str]
    device_type: Optional[str]
    app_type: Optional[str]
    
    def create_userInfo(self, headers, uid):
        self.uid = uid
        self.info_id = create_any_id()
        self.app_version = headers['app_version']
        self.device_type = headers['device_type']
        self.app_type = headers['app_type']

    def update_userInfo(self, headers):
        self.app_version = headers['app_version']
        
    class Config:
        arbitrary_types_allowed = True