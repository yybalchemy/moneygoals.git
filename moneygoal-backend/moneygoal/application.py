
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_pagination import add_pagination
from app.controllers import user, oss
from app.db.engine import init_db

app: FastAPI = FastAPI(
    title='FastAPI',
    version='1.0.0'
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(user.router, prefix='/moneygoal/user')
app.include_router(oss.router, prefix='/moneygoal/oss')

add_pagination(app)

@app.on_event("startup")
async def on_startup():
    await init_db()