//
//  MGAccountAccessor.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseAccessor.h"
#import "MGAccountModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAccountAccessor : MGBaseAccessor

+ (MGAccountAccessor *)shared;

// 最多使用的账户
- (NSArray *)mostUsedAccountModels;
- (MGAccountModel *)getAccountModelWithUniqueId:(NSUInteger)uniqueId;

- (void)riseReferenceWithUniqueId:(NSUInteger)uniqueId;

+ (NSString *)getAccountTypeName:(MGAccountType)selectType;
- (NSArray *)getAccountModelsWithAccountType:(MGAccountType)type;

// 获取默认的选择的账户
- (MGAccountModel *)getStandardAccountModel;
+ (NSArray *)getStandardAccountTypes;

@end

NS_ASSUME_NONNULL_END
