//
//  MGAccountAccessor.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountAccessor.h"

@implementation MGAccountAccessor

+ (MGAccountAccessor *)shared
{
    static MGAccountAccessor *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MGAccountAccessor alloc] init];
    });
    return manager;
}

- (Class)getAccessorClass
{
    return [MGAccountModel class];
}

- (NSString *)getAccessorDirPath
{
    return ACCOUNT_CACHE_PATH;
}

- (NSString *)getAccessorDefaultPath
{
    return @"Accounts";
}

- (NSString *)getOssPrefix
{
    return [NSString stringWithFormat:@"%@/accounts/", [MGUserModel user].uid];;
}

// ===========================================================
- (NSArray *)mostUsedAccountModels
{
    if (self.dataModels.count == 0) {
        return nil;
    }
    
    NSMutableArray *dataModels = [[self.dataModels mutableCopy] takeWhile:^BOOL(MGAccountModel *obj) {
        return obj.referenceCount > 0;
    }].mutableCopy;
    
    [dataModels sortUsingComparator:^NSComparisonResult(MGAccountModel *obj1, MGAccountModel *obj2) {
        if (obj1.referenceCount > obj2.referenceCount) {
            return NSOrderedAscending;
        }
        return NSOrderedDescending;
    }];
    return [dataModels first:3];
}

- (MGAccountModel *)getAccountModelWithUniqueId:(NSUInteger)uniqueId
{
    return [self.dataModels takeWhile:^BOOL(MGAccountModel *obj) {
        return obj.uniqueId == uniqueId;
    }].firstObject;
}

- (void)riseReferenceWithUniqueId:(NSUInteger)uniqueId
{
    MGAccountModel *accountModel = [self getAccountModelWithUniqueId:uniqueId];
    if (accountModel) {
        accountModel.referenceCount ++;
        [self saveToOssAndLocalWithDataModels:@[accountModel] completeBlock:nil errorBlock:nil];
    }
}

+ (NSArray *)getStandardAccountTypes
{
    return @[
        @{@"type": @(MGAccountTypeCash), @"name": @"现金账户"},
        @{@"type": @(MGAccountTypeTrust), @"name": @"信用卡账户"},
        @{@"type": @(MGAccountTypeOnline), @"name": @"网络账户"},
        @{@"type": @(MGAccountTypeCharge), @"name": @"储值账户"},
        @{@"type": @(MGAccountTypeInvest), @"name": @"投资账户"},
        @{@"type": @(MGAccountTypeStatic), @"name": @"固定资产"},
        @{@"type": @(MGAccountTypeShouldCharge), @"name": @"应收款"},
        @{@"type": @(MGAccountTypeDebet), @"name": @"负债"},
        @{@"type": @(MGAccountTypeOther), @"name": @"其他"}
    ];
}

- (NSArray *)getAccountModelsWithAccountType:(MGAccountType)type
{
    return [[self.dataModels takeWhile:^BOOL(MGAccountModel *obj) {
        return obj.type == type;
    }] sortedArrayUsingComparator:^NSComparisonResult(MGAccountModel *obj1, MGAccountModel *obj2) {
        return obj1.referenceCount > obj2.referenceCount;
    }];
}

+ (NSString *)getAccountTypeName:(MGAccountType)selectType
{
    for (NSDictionary *dict in [self getStandardAccountTypes]) {
        MGAccountType type = [dict[@"type"] integerValue];
        if (type == selectType) {
            return dict[@"name"];
        }
    }
    return nil;
}

- (MGAccountModel *)getStandardAccountModel
{
    NSArray *dataModels = [self mostUsedAccountModels];
    if (dataModels.count > 0) {
        return dataModels.firstObject;
    }
    return self.dataModels.firstObject;
}

@end
