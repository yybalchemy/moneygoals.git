//
//  MGCategoryAccessor.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseAccessor.h"
#import "MGCategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryAccessor : MGBaseAccessor

+ (MGCategoryAccessor *)shared;

// 使用最多次的三个类型
- (NSArray *)mostUsedCategoryModels;
- (MGCategoryModel *)getCategoryModelWithUniqueId:(NSUInteger)uniqueId;

- (void)riseReferenceWithUniqueId:(NSUInteger)uniqueId;

+ (NSArray *)getStandardCategoryTypes; // 一级分类
+ (NSArray *)getStandardIncomeCategoryTypes;

+ (NSString *)getCategoryTypeName:(MGCategoryType)selectType;
- (NSArray *)getCategoryModelsWithCategoryType:(MGCategoryType)type;

// 获取默认选择的分类
- (MGCategoryModel *)getStandardCategoryModel;

@end

NS_ASSUME_NONNULL_END
