//
//  MGCategoryAccessor.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryAccessor.h"

@implementation MGCategoryAccessor

+ (MGCategoryAccessor *)shared
{
    static MGCategoryAccessor *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MGCategoryAccessor alloc] init];
    });
    return manager;
}

- (Class)getAccessorClass
{
    return [MGCategoryModel class];
}

- (NSString *)getAccessorDirPath
{
    return CATEGORY_CACHE_PATH;
}

- (NSString *)getAccessorDefaultPath
{
    return @"Categories";
}

- (NSString *)getOssPrefix
{
    return [NSString stringWithFormat:@"%@/categories/", [MGUserModel user].uid];;
}

// ===========================================================
- (NSArray *)mostUsedCategoryModels
{
    if (self.dataModels.count == 0) {
        return nil;
    }
    
    NSMutableArray *dataModels = [[self.dataModels mutableCopy] takeWhile:^BOOL(MGCategoryModel *obj) {
        return obj.referenceCount > 0;
    }].mutableCopy;
    
    [dataModels sortUsingComparator:^NSComparisonResult(MGCategoryModel *obj1, MGCategoryModel *obj2) {
        if (obj1.referenceCount > obj2.referenceCount) {
            return NSOrderedAscending;
        }
        return NSOrderedDescending;
    }];
    return [dataModels first:3];
}

- (MGCategoryModel *)getCategoryModelWithUniqueId:(NSUInteger)uniqueId
{
    return [self.dataModels takeWhile:^BOOL(MGCategoryModel *obj) {
        return obj.uniqueId == uniqueId;
    }].firstObject;
}

- (void)riseReferenceWithUniqueId:(NSUInteger)uniqueId
{
    MGCategoryModel *categoryModel = [self getCategoryModelWithUniqueId:uniqueId];
    if (categoryModel) {
        categoryModel.referenceCount ++;
        [self saveToOssAndLocalWithDataModels:@[categoryModel] completeBlock:nil errorBlock:nil];
    }
}

+ (NSArray *)getStandardCategoryTypes
{
    return @[
        @{@"type": @(MGCategoryTypeBuy), @"name": @"购物消费"},
        @{@"type": @(MGCategoryTypeDaily), @"name": @"餐饮零食"},
        @{@"type": @(MGCategoryTypeCar), @"name": @"行车交通"},
        @{@"type": @(MGCategoryTypeFunny), @"name": @"娱乐休闲"},
        @{@"type": @(MGCategoryTypeLive), @"name": @"生活费用"},
        @{@"type": @(MGCategoryTypeMedical), @"name": @"医疗药品"},
        @{@"type": @(MGCategoryTypeStudy), @"name": @"教育学习"},
        @{@"type": @(MGCategoryTypeGift), @"name": @"礼物红包"},
        @{@"type": @(MGCategoryTypeDecorate), @"name": @"装修费用"},
        @{@"type": @(MGCategoryTypeFinical), @"name": @"金融保险"},
        @{@"type": @(MGCategoryTypeTravel), @"name": @"出差旅游"},
        @{@"type": @(MGCategoryTypeHardware), @"name": @"硬件设备"},
        @{@"type": @(MGCategoryTypeOther), @"name": @"其他"},
    ];
}

+ (NSArray *)getStandardIncomeCategoryTypes
{
    return @[
        @{@"type": @(MGIncomeCategoryTypeSalary), @"name": @"工资薪金"},
        @{@"type": @(MGIncomeCategoryTypeBonus), @"name": @"奖金"},
        @{@"type": @(MGIncomeCategoryTypeGift), @"name": @"人情礼金"},
        @{@"type": @(MGIncomeCategoryTypeManage), @"name": @"投资理财"},
        @{@"type": @(MGIncomeCategoryTypeLend), @"name": @"借贷收入"},
        @{@"type": @(MGIncomeCategoryTypeParttime), @"name": @"兼职收入"},
        @{@"type": @(MGIncomeCategoryTypeIdle), @"name": @"二手闲置"},
        @{@"type": @(MGIncomeCategoryTypeExpense), @"name": @"报销"},
        @{@"type": @(MGIncomeCategoryTypeSubsidy), @"name": @"补贴"},
        @{@"type": @(MGIncomeCategoryTypeLottery), @"name": @"中奖"},
        @{@"type": @(MGIncomeCategoryTypeOther), @"name": @"其他"}
    ];
}

+ (NSString *)getCategoryTypeName:(MGCategoryType)selectType
{
    for (NSDictionary *dict in [MGCategoryAccessor getStandardCategoryTypes]) {
        MGCategoryType type = [dict[@"type"] integerValue];
        if (type == selectType) {
            return dict[@"name"];
        }
    }
    return nil;
}

- (NSArray *)getCategoryModelsWithCategoryType:(MGCategoryType)type
{
    return [[self.dataModels takeWhile:^BOOL(MGCategoryModel *obj) {
        return obj.type == type;
    }] sortedArrayUsingComparator:^NSComparisonResult(MGCategoryModel *obj1, MGCategoryModel *obj2) {
        return obj1.referenceCount > obj2.referenceCount;
    }];
}

- (MGCategoryModel *)getStandardCategoryModel
{
    NSArray *dataModels = [self mostUsedCategoryModels];
    if (dataModels.count > 0) {
        return dataModels.firstObject;
    }
    return self.dataModels.firstObject;
}

@end
