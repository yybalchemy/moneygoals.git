//
//  MGAccessorModel.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccessorModel.h"

@implementation MGAccessorModel

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _uniqueId = [NSString createUniqueId];
    _pathUniqueId = [NSString createUniqueId];

    return self;
}

- (NSString *)getOssKey
{
    return @"";
}

- (BOOL)shouldUpload
{
    return YES;
}

- (BOOL)isStatic
{
    // 默认的不允许被上传
    if (self.uniqueId == UNIQUE_ID_CANTUPLOAD) {
        return YES;
    }
    return NO;
}

@end
