//
//  MGAccountModel.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountModel.h"

@implementation MGAccountModel

- (NSString *)getOssKey
{
    NSString *key = @(self.uniqueId).stringValue;
    // 以后共同记录的时候需要修改Key
    return [NSString stringWithFormat:@"%@/accounts/%@", self.create_userId, key];
}

@end
