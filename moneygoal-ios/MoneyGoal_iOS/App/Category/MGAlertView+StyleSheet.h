//
//  MGAlertView+StyleSheet.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/15.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertView (StyleSheet)

+ (YYBAlertView *)showLoadingWithString:(nullable NSString *)text inView:(nullable UIView *)inView;

+ (MGAlertView *)showMaskAlertViewInView:(UIView *)inView icon:(UIImage *)icon iconSize:(CGSize)size
                                    title:(nullable NSString *)title content:(nullable NSString *)content
                             marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets
                              actionTitle:(nullable NSString *)actionTitle tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;

+ (void)closeAlertView:(YYBAlertView *)alertView showString:(NSString *)showString;
+ (void)closeAlertView:(YYBAlertView *)alertView showError:(NSError *)showError;

@end

NS_ASSUME_NONNULL_END
