//
//  MGAlertView+StyleSheet.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/15.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertView+StyleSheet.h"
#import "YYBBaseViews/YYBRoundLoadingView.h"

@implementation MGAlertView (StyleSheet)

+ (YYBAlertView *)showLoadingWithString:(nullable NSString *)text inView:(nullable UIView *)inView {
    return [YYBAlertView showQueryStringAlertViewWithString:text inView:inView isResizeContainer:YES];
}

+ (MGAlertView *)showMaskAlertViewInView:(UIView *)inView icon:(UIImage *)icon iconSize:(CGSize)size
                                    title:(nullable NSString *)title content:(nullable NSString *)content
                             marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets
                              actionTitle:(nullable NSString *)actionTitle tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock
{
    MGAlertView *alertView = [[MGAlertView alloc] init];
    alertView.isDismissWhenOccurTapAction = NO;
    alertView.backgroundColor = [UIColor whiteColor];
    
    __weak typeof(alertView) weakAlertView = alertView;
    
    [alertView addContainerViewWithBlock:^(YYBAlertViewContainer *container) {
        container.maximalHeight = 500;
        container.minimalWidth = [UIScreen mainScreen].bounds.size.width;
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(0, paddingInsets.top * 2);
        }];
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = size;
            
            imageView.image = icon;
        }];
        
        if (title && title.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(30, 0, 0, 0);
                
                label.text = title;
                label.textColor = [weakAlertView mainTextColor];
                label.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
            }];
        }
        
        if (content && content.length > 0) {
            [container addLabelWithBlock:^(YYBAlertViewAction *action, UILabel *label) {
                action.margin = UIEdgeInsetsMake(5, 0, 0, 0);
                
                label.text = content;
                label.textColor = [weakAlertView thirdTextColor];
                label.font = [UIFont systemFontOfSize:12];
            }];
        }
        
        if (actionTitle && actionTitle.length > 0) {
            [container addButtonWithBlock:^(YYBAlertViewAction *action, UIButton *button) {
                action.margin = UIEdgeInsetsMake(40, 0, 0, 0);
                action.size = CGSizeMake(260, 50);
                
                [button setBackgroundImage:APP_COLOR_MAIN.colorToUIImage forState:0];
                [button setTitle:actionTitle forState:0];
                button.titleLabel.font = YYBFONT_BOLD(16);
                [button cornerRadius:25];
                
            } tapedOnBlock:^{
                if (tapedActionBlock) {
                    tapedActionBlock();
                }
            }];
        }
        
        [container addIconViewWithBlock:^(YYBAlertViewAction *action, UIImageView *imageView) {
            action.size = CGSizeMake(0, paddingInsets.bottom * 2);
        }];
    }];
    
    [inView addSubview:alertView];
    
    alertView.frame = CGRectMake(marginInsets.left,
                                 marginInsets.top,
                                 CGRectGetWidth(inView.frame) - marginInsets.left - marginInsets.right,
                                 CGRectGetHeight(inView.frame) - marginInsets.top - marginInsets.bottom);
    [alertView showAlertView];
    
    return alertView;
}

+ (void)closeAlertView:(YYBAlertView *)alertView showString:(NSString *)showString
{
    if (alertView) {
        [alertView closeAlertView];
    }
    [YYBAlertView showAlertViewWithStatusString:showString];
}

+ (void)closeAlertView:(YYBAlertView *)alertView showError:(NSError *)showError
{
    if (alertView) {
        [alertView closeAlertView];
    }
    [YYBAlertView showAlertViewWithError:showError];
}

@end
