//
//  NSDate+MGCommon.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/30.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (MGCommon)

- (NSString *)getDateOffsetString;

// 获取星期一的时间
- (NSDate *)getStartDayOfWeek;

@end

NS_ASSUME_NONNULL_END
