//
//  NSDate+MGCommon.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/30.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "NSDate+MGCommon.h"

@implementation NSDate (MGCommon)

- (NSString *)getDateOffsetString {
    NSInteger offset = [NSDate differenceWithUnit:NSCalendarUnitDay date:[NSDate date] toOtherDate:self].day;
    if (offset == 0) {
        return [self toStringWithFormatter:@"HH:mm"];
    } else if (offset == -1) {
        return [self toStringWithFormatter:@"昨天 HH:mm"];
    } else if (offset == -2) {
        return [self toStringWithFormatter:@"前天 HH:mm"];
    } else {
        return [self toStringWithFormatter:@"MM月dd日 HH:mm"];
    }
}

- (NSDate *)getStartDayOfWeek {
    // 日1 周一2 周二3
    if (self.weekday == 1) {
        return [self dateByAddingDays:-6].startOfDay;
    } else {
        return [self dateByAddingDays:2-self.weekday].startOfDay;
    }
}

@end
