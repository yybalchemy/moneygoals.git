//
//  NSString+UniqueId.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/21.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (UniqueId)

+ (NSUInteger)createUniqueId;

@end

NS_ASSUME_NONNULL_END
