//
//  NSString+UniqueId.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/21.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "NSString+UniqueId.h"

@implementation NSString (UniqueId)

+ (NSUInteger)createUniqueId
{
    int date = (int)[NSDate date].timeIntervalSince1970;
    
    NSString *str = @"";
    for (NSInteger index = 0; index < 10; index ++) {
        NSString *subStr = @(arc4random() % 10).stringValue;
        str = [str stringByAppendingString:subStr];
    }
    
    NSUInteger random = str.integerValue;
    return date + random;
}

@end
