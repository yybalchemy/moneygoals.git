//
//  YYBNavigationBarImageView+MGAdd.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBNavigationBarImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBNavigationBarImageView (MGAdd)

+ (YYBNavigationBarImageView *)createViewWithIcon:(NSString *)icon edgeInsets:(UIEdgeInsets)edgeInsets tapedActionBlock:(void (^)(YYBNavigationBarContainer *view))tapedActionBlock;

@end

NS_ASSUME_NONNULL_END
