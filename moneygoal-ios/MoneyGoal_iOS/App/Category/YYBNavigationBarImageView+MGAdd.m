//
//  YYBNavigationBarImageView+MGAdd.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBNavigationBarImageView+MGAdd.h"

@implementation YYBNavigationBarImageView (MGAdd)

+ (YYBNavigationBarImageView *)createViewWithIcon:(NSString *)icon edgeInsets:(UIEdgeInsets)edgeInsets tapedActionBlock:(void (^)(YYBNavigationBarContainer * _Nonnull))tapedActionBlock
{
    return [YYBNavigationBarImageView imageViewWithConfigureBlock:^(YYBNavigationBarImageView *container, UIImageView *view) {
        
        container.imageView.image = [UIImage imageNamed:icon];
        container.contentSize = CGSizeMake(18, 44);
        container.contentEdgeInsets = edgeInsets;
        
    } tapedActionBlock:tapedActionBlock];
}

@end
