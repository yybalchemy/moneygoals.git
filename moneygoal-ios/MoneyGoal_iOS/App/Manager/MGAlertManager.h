//
//  MGAlertManager.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertManager : NSObject

+ (void)createAlert:(MGRecordModel *)recordModel completeBlock:(YYBTapedActionBlock)completeBlock;
+ (void)deleteAlert:(MGRecordModel *)recordModel;

+ (void)startNotification;
+ (void)refreshAllAlerts;

@end

NS_ASSUME_NONNULL_END
