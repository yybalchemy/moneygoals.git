//
//  MGOssManager.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AliyunOSSiOS/AliyunOSSiOS.h>
#import "MGAccessorModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGOssManager : NSObject

+ (MGOssManager *)shared;
// =================================================
+ (BOOL)shouldUpload;

- (void)uploadData:(NSData *)data key:(NSString *)key bucket:(NSString *)bucket
     progressBlock:(nullable void (^)(int64_t, int64_t))progressBlock
     completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

- (void)deleteObjectKey:(NSString *)objectKey completeBlock:(nullable YYBEmptyCompleteBlock)completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

- (void)getContentsWithPrefix:(NSString *)prefix bucket:(NSString *)bucket
                completeBlock:(nullable void (^)(NSArray * _Nullable contents))completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

// =================================================
- (void)saveToOssWithDataModels:(NSArray *)dataModels completeBlock:(YYBEmptyCompleteBlock)completeBlock errorBlock:(YYBErrorBlock)errorBlock;
- (void)getDataModelsWithPrefix:(NSString *)prefix dataClass:(Class)dataClass completeBlock:(void (^)(NSArray * _Nonnull dataModels))completeBlock errorBlock:(YYBErrorBlock)errorBlock;

- (void)uploadAvatar:(UIImage *)avatar completeBlock:(nullable void (^)(NSString *key))completeBlock errorBlock:(nullable YYBErrorBlock)errorBlock;

@end

NS_ASSUME_NONNULL_END
