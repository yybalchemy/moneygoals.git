//
//  MGReqManager.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBRequestManager.h"
#import "NSDictionary+MGSign.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReqManager : YYBRequestManager

+ (MGReqManager *)shared;

- (NSString *)avatarURL:(NSString *)avatar;
- (NSString *)requestURL:(NSString *)router;

- (void)getWechatResultWithCode:(NSString *)code successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;
- (void)getAppleLoginResultWithParameters:(NSDictionary *)parameters successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;

- (void)putUserAvatar:(NSString *)avatarKey successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;
- (void)putUserNickName:(NSString *)nickName successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;

- (void)deleteUserWithSuccessBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;

- (void)getCaptchaWithPhoneNum:(NSString *)phoneNum successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;
- (void)handleLoginWithPhoneNum:(NSString *)phoneNum captcha:(NSString *)captcha successBlock:(YYBResponseSuccBlock)successBlock errorBlock:(YYBResponseErrorBlock)errorBlock;

@end

NS_ASSUME_NONNULL_END
