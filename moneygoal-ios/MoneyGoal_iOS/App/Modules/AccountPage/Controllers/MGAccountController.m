//
//  MGAccountController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountController.h"
#import "MGAccountIconController.h"

#import "MGBaseActionsView.h"

#import "MGBaseIconCell.h"
#import "MGBaseBorderInputCell.h"
#import "MGBaseBorderDetailCell.h"

@interface MGAccountController ()
@property (nonatomic, strong) MGBaseActionsView *actionsView;

@end

@implementation MGAccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.updateModel) {
        self.dataModel = [MGAccountModel yy_modelWithJSON:self.updateModel.yy_modelToJSONObject];
    } else {
        self.dataModel = [[MGAccountModel alloc] init];
        self.dataModel.type = self.createAccountType;
    }
}

- (void)afterConfigViewAction {
    [super afterConfigViewAction];
    
    @weakify(self);
    self.actionsView = [MGBaseActionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + [UIDevice safeAreaInsetsBottom]);
    } configureBlock:^(MGBaseActionsView *view) {
        view.createBlock = ^{
            @strongify(self);
            [self createAction];
        };
        view.updateBlock = ^{
            @strongify(self);
            [self updateAction];
        };
        view.deleteBlock = ^{
            @strongify(self);
            [self deleteAction];
        };
    }];
    
    if (self.updateModel) {
        [self.actionsView changeUpdate];
    } else {
        [self.actionsView changeCompleteCreate];
    }
}

// 选择一级分类
- (void)handleSelectAccountType {
    @weakify(self);
    [MGAlertView showAccountTypesAlertViewWithSelectType:self.dataModel.type selectedBlock:^(MGAccountType type) {
        @strongify(self);
        self.dataModel.type = type;
        [self.tableView reloadData];
    }];
}

- (void)createAction {
    @weakify(self);
    if (self.dataModel.name.length > 8 || !self.dataModel.name.isExist) {
        [YYBAlertView showAlertViewWithStatusString:@"请输入8个字符以内的名称"];
    } else {
        if (MGOssManager.shouldUpload) {
            self.refreshAlertView = [MGAlertView showLoadingWithString:@"添加中" inView:self.view];
        }
        [MGAccountAccessor.shared createDataModel:self.dataModel completeBlock:^{
            @strongify(self);
            [MGAlertView closeAlertView:self.refreshAlertView showString:@"添加成功"];
            if (self.createModelBlock) {
                self.createModelBlock(self.dataModel.uniqueId);
            }
            [self.navigationController popViewControllerAnimated:YES];
        } errorBlock:^(NSError *error) {
            @strongify(self);
            [MGAlertView closeAlertView:self.refreshAlertView showError:error];
        }];
    }
}

- (void)updateAction {
    @weakify(self);
    if (self.dataModel.name.length > 8 || !self.dataModel.name.isExist) {
        [YYBAlertView showAlertViewWithStatusString:@"请输入8个字符以内的名称"];
    } else {
        if (MGOssManager.shouldUpload) {
            self.refreshAlertView = [MGAlertView showLoadingWithString:@"修改中" inView:self.view];
        }
        [MGAccountAccessor.shared updateDataModel:self.updateModel shouldUpdateModel:self.dataModel completeBlock:^{
            @strongify(self);
            [MGAlertView closeAlertView:self.refreshAlertView showString:@"修改成功"];
            if (self.updateModelBlock) {
                self.updateModelBlock(self.dataModel.uniqueId);
            }
            [self.navigationController popViewControllerAnimated:YES];
        } errorBlock:^(NSError *error) {
            @strongify(self);
            [MGAlertView closeAlertView:self.refreshAlertView showError:error];
        }];
    }
}

- (void)deleteAction {
    @weakify(self);
    [MGAlertView showAlertViewWithTitle:nil detail:@"确定要删除吗？" firstActionTitle:@"取消" firstActionTapedBlock:^{
        
    } secondActionTitle:@"确定" secondActionTapedBlock:^{
        @strongify(self);
        self.refreshAlertView = [MGAlertView showLoadingWithString:@"删除中" inView:self.view];
        [self _deleteAction];
    }];
}

- (void)_deleteAction {
    @weakify(self);
    [MGAccountAccessor.shared deleteDataModel:self.dataModel completeBlock:^{
        @strongify(self);
        [MGAlertView closeAlertView:self.refreshAlertView showString:@"删除成功"];
        if (self.deleteModelBlock) {
            self.deleteModelBlock(self.dataModel.uniqueId);
        }
        [self.navigationController popViewControllerAnimated:YES];
    } errorBlock:^(NSError *error) {
        @strongify(self);
        [MGAlertView closeAlertView:self.refreshAlertView showError:error];
    }];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGBaseBorderInputCell", @"MGBaseBorderDetailCell", @"MGBaseIconCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    if (indexPath.row == 0) {
        MGBaseBorderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGBaseBorderDetailCell"];
        
        NSString *typeName = [MGAccountAccessor getAccountTypeName:self.dataModel.type];
        
        [cell configViewWithCornerType:MGCornerTypeTop];
        [cell configViewWithTitleValue:@"账户类型" detailValue:typeName iconName:@"ico_common_cate"];
        return cell;
    } else if (indexPath.row == 1) {
        MGBaseBorderInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGBaseBorderInputCell"];
        cell.stringChangedBlock = ^(NSString *text) {
            @strongify(self);
            self.dataModel.name = text;
        };
        
        [cell configViewWithCornerType:MGCornerTypeCenter];
        [cell configViewWithTitleValue:@"名称" detailValue:self.dataModel.name iconName:@"ico_common_name"];
        return cell;
    } else if (indexPath.row == 2) {
        MGBaseIconCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGBaseIconCell"];
        
        [cell configViewWithCornerType:MGCornerTypeBottom];
        [cell configViewWithTitleValue:@"图标" detailValue:self.dataModel.icon iconName:@"ico_common_icon"];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self handleSelectAccountType];
    } else if (indexPath.row == 2) {
        @weakify(self);
        [YYBViewRouter openOnClass:[MGAccountIconController class] configureBlock:^(MGAccountIconController *obj) {
            @strongify(self);
            obj.selectIconBlock = ^(NSString * _Nonnull iconName) {
                self.dataModel.icon = iconName;
                [self.tableView reloadData];
            };
        }];
    }
}

- (NSString *)customPageTitle {
    if (self.updateModel) {
        return @"修改账户";
    } else {
        return @"添加账户";
    }
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 10;
    return insets;
}

@end
