//
//  MGAccountIconController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCollectionController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAccountIconController : MGCollectionController

@property (nonatomic, copy) void (^ selectIconBlock)(NSString *iconName);

@end

NS_ASSUME_NONNULL_END
