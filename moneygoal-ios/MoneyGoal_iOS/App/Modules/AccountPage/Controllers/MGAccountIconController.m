//
//  MGAccountIconController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountIconController.h"

#import "MGIconCell.h"
#import "MGIconSectionCell.h"

@interface MGAccountIconController ()

@end

@implementation MGAccountIconController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerClass:[MGIconSectionCell class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGIconSectionCell"];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 7;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 10);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    MGIconSectionCell *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGIconSectionCell" forIndexPath:indexPath];
    return view;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) return 5;
    if (section == 1) return 5;
    if (section == 2) return 6;
    if (section == 3) return 7;
    if (section == 4) return 3;
    if (section == 5) return 5;
    if (section == 6) return 2;
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemSize = (kScreenWidth - 90) / 6;
    return CGSizeMake(itemSize, itemSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGIconCell"];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGIconCell" forIndexPath:indexPath];
    
    NSInteger startIndex = indexPath.section * 100 + indexPath.row + 100;
    NSString *iconName = [NSString stringWithFormat:@"acct_type_%d", (int)startIndex];
    [cell configViewWithDataModel:iconName];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger startIndex = indexPath.section * 100 + indexPath.row + 100;
    NSString *iconName = [NSString stringWithFormat:@"acct_type_%d", (int)startIndex];
    if (self.selectIconBlock) {
        self.selectIconBlock(iconName);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)customPageTitle {
    return @"选择图标";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.left = 20;
    insets.right = 20;
    insets.top += 20;
    return insets;
}

@end
