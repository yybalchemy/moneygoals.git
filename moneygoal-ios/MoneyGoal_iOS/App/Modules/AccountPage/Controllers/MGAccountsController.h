//
//  MGAccountsController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAccountsController : MGTableController

@property (nonatomic, copy) void (^ createModelBlock)(NSUInteger uniqueId);
@property (nonatomic, copy) void (^ updateModelBlock)(NSUInteger uniqueId);
@property (nonatomic, copy) void (^ deleteModelBlock)(NSUInteger uniqueId);

@end

NS_ASSUME_NONNULL_END
