//
//  MGAccountsController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountsController.h"
#import "MGAccountController.h"

#import "MGAccountCell.h"

#import "MGCategoryTypeView.h"
#import "MGBaseActionsView.h"

#import "MGAccountTypeModel.h"

@interface MGAccountsController ()
@property (nonatomic, strong) MGBaseActionsView *actionsView;

@end

@implementation MGAccountsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    
    NSArray *accountTypeModels = [MGAccountAccessor getStandardAccountTypes];
    self.dataList = [NSArray yy_modelArrayWithClass:[MGAccountModel class] json:accountTypeModels].mutableCopy;
    [self.tableView reloadData];
}

- (void)afterConfigViewAction {
    [super afterConfigViewAction];
    
    @weakify(self);
    self.actionsView = [MGBaseActionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + [UIDevice safeAreaInsetsBottom]);
    } configureBlock:^(MGBaseActionsView *view) {
        view.createBlock = ^{
            @strongify(self);
            [self createAction];
        };
    }];
    
    [self.actionsView changeCreate];
}

- (void)createActionWithAccountType:(MGAccountType)type section:(NSInteger)section {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGAccountController class] configureBlock:^(MGAccountController *obj) {
        @strongify(self);
        obj.createAccountType = type;
        obj.createModelBlock = ^(NSUInteger uniqueId) {
            [self.tableView reloadSection:section withRowAnimation:UITableViewRowAnimationNone];
            if (self.createModelBlock) {
                self.createModelBlock(uniqueId);
            }
        };
    }];
}

- (void)createAction {
    @weakify(self);
    [YYBViewRouter openOnClass:[MGAccountController class] configureBlock:^(MGAccountController *obj) {
        @strongify(self);
        obj.createModelBlock = ^(NSUInteger uniqueId) {
            [self.tableView reloadData];
            if (self.createModelBlock) {
                self.createModelBlock(uniqueId);
            }
        };
    }];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGAccountCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MGAccountModel *accountModel = self.dataList[section];
    NSArray *dataModels = [MGAccountAccessor.shared getAccountModelsWithAccountType:accountModel.type];
    return dataModels.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MGCategoryTypeView *view = [[MGCategoryTypeView alloc] init];
    
    MGAccountTypeModel *accountModel = self.dataList[section];
    @weakify(self);
    view.tapedActionBlock = ^{
        @strongify(self);
        [self createActionWithAccountType:accountModel.type section:section];
    };
    [view configViewWithTitleValue:accountModel.name];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAccountCell"];
    
    MGAccountTypeModel *accountModel = self.dataList[indexPath.section];
    NSArray *dataModels = [MGAccountAccessor.shared getAccountModelsWithAccountType:accountModel.type];
    MGAccountModel *dataModel = dataModels[indexPath.row];
    
    [cell configViewWithDataModel:dataModel];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MGAccountTypeModel *accountModel = self.dataList[indexPath.section];
    NSArray *dataModels = [MGAccountAccessor.shared getAccountModelsWithAccountType:accountModel.type];
    MGAccountModel *dataModel = dataModels[indexPath.row];
    
    if (!dataModel.isStatic) {
        @weakify(self);
        [YYBViewRouter openOnClass:[MGAccountController class] configureBlock:^(MGAccountController *obj) {
            @strongify(self);
            obj.updateModel = dataModel;
            obj.deleteModelBlock = ^(NSUInteger uniqueId) {
                [self.tableView reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];
                if (self.deleteModelBlock) {
                    self.deleteModelBlock(uniqueId);
                }
            };
            obj.updateModelBlock = ^(NSUInteger uniqueId) {
                [self.tableView reloadData];
                if (self.updateModelBlock) {
                    self.updateModelBlock(uniqueId);
                }
            };
        }];
    }
}

- (NSString *)customPageTitle {
    return @"账户列表";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.bottom += 65;
    return insets;
}

@end
