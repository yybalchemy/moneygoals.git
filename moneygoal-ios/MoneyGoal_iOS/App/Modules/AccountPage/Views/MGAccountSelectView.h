//
//  MGAccountSelectView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAccountSelectView : YYBBaseView

@property (nonatomic, copy) void (^ dataModelSelectBlock)(MGAccountModel *dataModel);
@property (nonatomic, copy) YYBTapedActionIndexBlock selectTypeBlock;
@property (nonatomic, strong) MGAccountModel *selectModel;

- (void)configViewWithDataModel:(MGAccountModel *)dataModel shouldShowAllAccounts:(BOOL)shouldShowAllAccounts selectAllAccounts:(BOOL)selectAllAccounts hidesAllActions:(BOOL)hidesAllActions;

@end

NS_ASSUME_NONNULL_END
