//
//  MGAccountSelectView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountSelectView.h"
#import "MGAccountTypeView.h"

#import "MGAccountTypeModel.h"

#import "MGAccountSelectCell.h"

@interface MGAccountSelectView () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *showAllButton;

@end

@implementation MGAccountSelectView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_add"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeCreate);
        }
    }];
    
    self.secondActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.firstActionButton);
        make.left.equalTo(self.firstActionButton.mas_right).offset(20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_update"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeList);
        }
    }];
    
    self.thirdActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.firstActionButton);
        make.right.equalTo(self).offset(-20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_dismiss"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeCancel);
        }
    }];
    
    self.showAllButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 32));
        make.centerY.equalTo(self.firstActionButton);
        make.right.equalTo(self.thirdActionButton.mas_left).offset(-20);
    } configureBlock:^(UIButton *view) {
        view.titleLabel.font = FONT_REGULAR(14);
        [view setTitle:@"全部" forState:0];
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:UIControlStateSelected];
        [view setTitleColor:TEXT_COLOR_FIRST forState:UIControlStateSelected];
        [view setBackgroundImage:APP_COLOR_SAPERATE.colorToUIImage forState:0];
        [view setTitleColor:TEXT_COLOR_SECOND forState:0];
        [view cornerRadius:16];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeShowAll);
        }
    }];
    
    self.tableView = [UITableView createAtSuperView:self delagateBlock:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(435);
    } dequeueCellIdentifiers:@[@"MGAccountSelectCell"] configureBlock:^(UITableView *view) {
        view.showsVerticalScrollIndicator = NO;
    }];
    
    if (@available(iOS 15.0, *)) {
        self.tableView.sectionHeaderTopPadding = 0;
    }
    
    [UIView createTopSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    return self;
}

- (void)configViewWithDataModel:(MGAccountModel *)dataModel shouldShowAllAccounts:(BOOL)shouldShowAllAccounts selectAllAccounts:(BOOL)selectAllAccounts hidesAllActions:(BOOL)hidesAllActions
{
    _selectModel = dataModel;
    
    if (hidesAllActions) {
        self.firstActionButton.hidden = YES;
        self.secondActionButton.hidden = YES;
    }
    
    self.showAllButton.hidden = !shouldShowAllAccounts;
    self.showAllButton.selected = selectAllAccounts;
    
    [self.gatherView reloadData];
}

- (NSArray *)getAccountModelsWithSectionIndex:(NSInteger)sectionIndex
{
    NSDictionary *data = [MGAccountAccessor getStandardAccountTypes][sectionIndex];
    MGAccountType type = [data[@"type"] integerValue];
    return [MGAccountAccessor.shared getAccountModelsWithAccountType:type];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [MGAccountAccessor getStandardAccountTypes].count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MGAccountTypeView *view = [[MGAccountTypeView alloc] init];
    NSDictionary *dataModel = [MGAccountAccessor getStandardAccountTypes][section];
    [view configViewWithTitleValue:dataModel[@"name"]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSArray *dataModels = [self getAccountModelsWithSectionIndex:section];
    if (dataModels.count == 0) return 0;
    return 35;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *dataModels = [self getAccountModelsWithSectionIndex:section];
    return dataModels.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataModels = [self getAccountModelsWithSectionIndex:indexPath.section];
    MGAccountModel *dataModel = dataModels[indexPath.row];
    
    MGAccountSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAccountSelectCell"];
    
    BOOL isSelect = _selectModel && _selectModel.uniqueId == dataModel.uniqueId;
    [cell configViewWithDataModel:dataModel status:isSelect];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataModels = [self getAccountModelsWithSectionIndex:indexPath.section];
    MGAccountModel *dataModel = dataModels[indexPath.row];
    if (self.dataModelSelectBlock) {
        self.dataModelSelectBlock(dataModel);
    }
}

@end
