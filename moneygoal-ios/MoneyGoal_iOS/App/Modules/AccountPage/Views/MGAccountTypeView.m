//
//  MGAccountTypeView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAccountTypeView.h"

@implementation MGAccountTypeView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = APP_COLOR_LIGHT_SAPERATE;
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.centerY.equalTo(self);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(14);
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    self.firstStringLabel.text = titleValue;
}

@end
