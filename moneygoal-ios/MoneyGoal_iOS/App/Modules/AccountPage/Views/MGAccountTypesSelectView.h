//
//  MGAccountTypesSelectView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/27.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAccountTypesSelectView : YYBBaseView

@property (nonatomic, copy) void (^ selectTypeBlock)(MGAccountType type);
@property (nonatomic) MGAccountType selectType;

@end

NS_ASSUME_NONNULL_END
