//
//  MGAlertSoundModel+MGAdd.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertSoundModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertSoundModel (MGAdd)

+ (NSArray *)queryAllAlertSoundModels;

@end

NS_ASSUME_NONNULL_END
