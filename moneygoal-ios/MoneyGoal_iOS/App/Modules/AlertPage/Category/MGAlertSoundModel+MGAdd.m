//
//  MGAlertSoundModel+MGAdd.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertSoundModel+MGAdd.h"

@implementation MGAlertSoundModel (MGAdd)

+ (NSArray *)queryAllAlertSoundModels
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    id soundObjects = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    return [NSArray yy_modelArrayWithClass:[MGAlertSoundModel class] json:soundObjects];
}

@end
