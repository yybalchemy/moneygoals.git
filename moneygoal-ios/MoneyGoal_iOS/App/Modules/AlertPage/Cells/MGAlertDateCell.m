//
//  MGAlertDateCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/13.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertDateCell.h"

@implementation MGAlertDateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.containerView.layer.cornerRadius = 10;
    self.containerView.layer.masksToBounds = YES;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView);
    } configureBlock:^(UIButton *view) {
        [view setTitleColor:TEXT_COLOR_FIRST forState:0];
        view.titleLabel.font = FONT_REGULAR(15);
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    return self;
}

- (void)configViewWithDate:(NSDate *)date {
    NSString *dateStr = [date toStringWithFormatter:@"HH:mm"];
    [self.firstActionButton setTitle:dateStr forState:0];
}

@end
