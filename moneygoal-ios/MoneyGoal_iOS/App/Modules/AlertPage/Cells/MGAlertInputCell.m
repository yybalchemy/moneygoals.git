//
//  MGAlertInputCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertInputCell.h"

@implementation MGAlertInputCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.containerView.backgroundColor = [UIColor clearColor];
    
    self.placeholderStringView = [YYBPlaceholderTextView createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.containerView);
    } configureBlock:^(YYBPlaceholderTextView *view) {
        view.placeholder = @"请输入...";
        [view renderWithFont:FONT_REGULAR(15) placeholderColor:[UIColor colorWithHexValue:0x999999] lineSpace:5];
        view.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
        [view cornerRadius:10];
        view.backgroundColor = [UIColor whiteColor];
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    @weakify(self);
    [self.placeholderStringView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        if (self.stringChangedBlock) {
            self.stringChangedBlock(x);
        }
    }];
    
    return self;
}

@end
