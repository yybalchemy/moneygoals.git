//
//  MGAlertSoundListCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertSoundListCell.h"
#import "MGAlertSoundCell.h"

@implementation MGAlertSoundListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.gatherView = [UICollectionView createAtSuperView:self.contentView delegeteTarget:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } dequeueCellIdentifiers:@[@"MGAlertSoundCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 6;
        layout.itemSize = CGSizeMake((kScreenWidth - 6 * 3 - 30) / 4, 36);
        view.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
        view.backgroundColor = [UIColor clearColor];
    }];
    
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [MGAlertSoundModel queryAllAlertSoundModels].count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGAlertSoundCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGAlertSoundCell" forIndexPath:indexPath];
    MGAlertSoundModel *dataModel = [MGAlertSoundModel queryAllAlertSoundModels][indexPath.row];
    BOOL status = NO;
    if (self.soundSelectedStatusBlock) {
        status = self.soundSelectedStatusBlock(dataModel.soundMode);
    }
    [cell configViewWithTitleValue:dataModel.title status:status];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MGAlertSoundModel *dataModel = [MGAlertSoundModel queryAllAlertSoundModels][indexPath.row];
    if (self.selectedSoundBlock) {
        self.selectedSoundBlock(dataModel);
    }
    [collectionView reloadData];
}

@end
