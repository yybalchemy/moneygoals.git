//
//  MGAlertTypeCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertTypeCell.h"

@implementation MGAlertTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstActionButton = [UIButton createAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(24, 24));
        make.centerY.equalTo(self.containerView);
        make.right.equalTo(self.containerView).offset(-15);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"alert_not_select"] forState:0];
        [view setBackgroundImage:[UIImage imageNamed:@"alert_select"] forState:UIControlStateSelected];
        view.userInteractionEnabled = NO;
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status {
    self.firstStringLabel.text = titleValue;
    self.firstActionButton.selected = status;
}

@end
