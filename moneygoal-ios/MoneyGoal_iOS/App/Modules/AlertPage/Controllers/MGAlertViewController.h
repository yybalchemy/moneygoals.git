//
//  MGAlertViewController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"
#import "MGAlertModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGAlertViewController : MGTableController

@property (nonatomic, strong) MGAlertModel *dataModel;
@property (nonatomic, strong) MGAlertModel *updateModel;

@property (nonatomic, copy) void (^ updateAlertModelBlock)(MGAlertModel *dataModel);

@end

NS_ASSUME_NONNULL_END
