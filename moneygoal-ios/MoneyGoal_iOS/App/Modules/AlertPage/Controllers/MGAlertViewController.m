//
//  MGAlertViewController.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertViewController.h"
#import <AVFoundation/AVFoundation.h>

#import "MGRecordsPreviousDayCell.h"
#import "MGAlertTypeCell.h"
#import "MGAlertInputCell.h"
#import "MGAlertSoundListCell.h"
#import "MGAlertDateCell.h"

@interface MGAlertViewController ()
@property (nonatomic, strong) AVPlayer *audioPlayer;

@end

@implementation MGAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = APP_COLOR_BACKGROUND;
    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void)beforeConfigViewAction {
    if (self.updateModel) {
        self.dataModel = [MGAlertModel yy_modelWithJSON:self.updateModel.yy_modelToJSONObject];
    } else {
        self.dataModel = [[MGAlertModel alloc] init];
    }
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar {
    [super configCustomNavigationBar:navigationBar];
    
    @weakify(self);
    YYBNavigationBarButton *completeButton = [YYBNavigationBarButton buttonWithConfigureBlock:^(YYBNavigationBarButton *container, UIButton *view) {
        container.contentSize = CGSizeMake(55, 28);
        container.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        
        [view cornerRadius:14];
        [view setTitle:@"保存" forState:0];
        [view setTitleColor:APP_COLOR_MAIN forState:0];
        view.backgroundColor = [APP_COLOR_MAIN colorWithAlphaComponent:0.1];
        view.titleLabel.font = FONT_REGULAR(14);
    } tapedActionBlock:^(YYBNavigationBarContainer *view) {
        @strongify(self);
        if (self.updateAlertModelBlock) {
            self.updateAlertModelBlock(self.dataModel);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    navigationBar.rightBarContainers = @[completeButton];
}

- (void)playWithSoundPath:(NSString *)soundPath {
    NSURL *audioURL = [[NSBundle mainBundle] URLForResource:soundPath withExtension:@".mp3"];
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:audioURL];
    if (_audioPlayer) {
        [_audioPlayer replaceCurrentItemWithPlayerItem:item];
    } else {
        _audioPlayer = [[AVPlayer alloc] initWithPlayerItem:item];
    }
    [_audioPlayer play];
}

- (void)changeDateHandler {
    @weakify(self);
    [YYBAlertView showDatePickerAlertViewWithSelectedBlock:^(NSDate * _Nonnull selectedDate) {
        @strongify(self);
        self.dataModel.startDate = selectedDate;
        [self.tableView reloadSection:1 withRowAnimation:UITableViewRowAnimationNone];
    } mode:UIDatePickerModeTime date:self.dataModel.startDate];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGRecordsPreviousDayCell",@"MGAlertTypeCell",@"MGAlertInputCell",@"MGAlertSoundListCell",@"MGAlertDateCell"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) return 42;
    
    if (indexPath.section == 0) {
        return 50;
    } else if (indexPath.section == 1) {
        return 50;
    } else if (indexPath.section == 2) {
        return 80;
    } else if (indexPath.section == 3) {
        NSArray *soundModels = [MGAlertSoundModel queryAllAlertSoundModels];
        NSInteger counts = (soundModels.count - 1) / 4 + 1;
        return counts * 36 + (counts - 1) * 10;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        MGRecordsPreviousDayCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGRecordsPreviousDayCell"];
        if (indexPath.section == 0) {
            cell.firstStringLabel.text = @"提醒频次";
        } else if (indexPath.section == 1) {
            cell.firstStringLabel.text = @"提醒时间";
        } else if (indexPath.section == 2) {
            cell.firstStringLabel.text = @"提醒内容";
        } else if (indexPath.section == 3) {
            cell.firstStringLabel.text = @"提醒音效";
        }
        return cell;
    }
    
    if (indexPath.section == 0) {
        MGAlertTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAlertTypeCell"];
        if (indexPath.row == 1) {
            [cell configViewWithTitleValue:@"不提醒" status:self.dataModel.type == MGAlertTypeNone];
            [cell configViewWithCornerType:MGCornerTypeTop];
        } else if (indexPath.row == 2) {
            [cell configViewWithTitleValue:@"每日提醒" status:self.dataModel.type == MGAlertTypeDay];
            [cell configViewWithCornerType:MGCornerTypeBottom];
        }
        return cell;
    } else if (indexPath.section == 1) {
        MGAlertDateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAlertDateCell"];
        @weakify(self);
        cell.tapedActionBlock = ^{
            @strongify(self);
            [self changeDateHandler];
        };
        [cell configViewWithDate:self.dataModel.startDate];
        return cell;
    } else if (indexPath.section == 2) {
        MGAlertInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAlertInputCell"];
        cell.placeholderStringView.text = self.dataModel.content;
        @weakify(self);
        cell.stringChangedBlock = ^(NSString *text) {
            @strongify(self);
            self.dataModel.content = text;
        };
        return cell;
    } else if (indexPath.section == 3) {
        MGAlertSoundListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGAlertSoundListCell"];
        @weakify(self);
        cell.soundSelectedStatusBlock = ^BOOL(NSInteger soundIndex) {
            @strongify(self);
            return self.dataModel.soundModel.soundMode == soundIndex;
        };
        
        cell.selectedSoundBlock = ^(MGAlertSoundModel * _Nonnull dataModel) {
            @strongify(self);
            self.dataModel.soundModel = dataModel;
            [self playWithSoundPath:dataModel.soundFileName];
        };
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row > 0) {
        if (indexPath.row == 1) {
            self.dataModel.type = MGAlertTypeNone;
        } else if (indexPath.row == 2) {
            self.dataModel.type = MGAlertTypeDay;
        }
        [self.tableView reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (NSString *)customPageTitle {
    return @"提醒";
}

@end
