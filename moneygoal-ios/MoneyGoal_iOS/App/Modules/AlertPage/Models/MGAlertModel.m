//
//  MGAlertModel.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/12.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGAlertModel.h"

@implementation MGAlertModel

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _startDate = [[NSDate date].startOfDay dateByAddingHours:18];
    _content = @"小主~ 您的存钱时刻到啦，赶紧来记录存钱吧";
    _soundModel = [[MGAlertSoundModel queryAllAlertSoundModels] lastObject];
    
    return self;
}

- (NSString *)getAlertType {
    if (self.type == MGAlertTypeNone) {
        return @"不提醒";
    } else {
        NSString *dateStr = [self.startDate toStringWithFormatter:@"HH:mm"];
        if (self.type == MGAlertTypeDay) {
            return [NSString stringWithFormat:@"每日提醒 %@",dateStr];
        }
    }
    return @"";
}

@end
