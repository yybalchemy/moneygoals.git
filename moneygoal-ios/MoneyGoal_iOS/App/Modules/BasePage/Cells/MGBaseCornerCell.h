//
//  MGBaseCornerCell.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

typedef NS_ENUM(NSInteger, MGCornerType) {
    MGCornerTypeTop,
    MGCornerTypeCenter,
    MGCornerTypeBottom,
    MGCornerTypeAll
};

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseCornerCell : YYBTableViewCell

- (void)configViewWithCornerType:(MGCornerType)cornerType;
- (void)configViewWithCornerType:(MGCornerType)cornerType cornerRadius:(CGFloat)cornerRadius;

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue iconName:(NSString *)iconName;

- (void)configBorderTypeWithDataModels:(NSMutableArray *)dataModels curObj:(id)curObj;

@end

NS_ASSUME_NONNULL_END
