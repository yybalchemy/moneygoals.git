//
//  MGBaseCornerCell.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

@implementation MGBaseCornerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.containerView = [UIView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(50);
    } configureBlock:^(UIView *view) {
        view.backgroundColor = [UIColor whiteColor];
    }];
    
    self.firstLineView = [UIView createViewAtSuperView:self.containerView backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.containerView);
        make.left.equalTo(self.containerView).offset(15);
        make.right.equalTo(self.containerView).offset(-15);
        make.height.mas_equalTo(1);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithCornerType:(MGCornerType)cornerType {
    [self configViewWithCornerType:cornerType cornerRadius:6];
}

- (void)configViewWithCornerType:(MGCornerType)cornerType cornerRadius:(CGFloat)cornerRadius {
    self.firstLineView.hidden = NO;
    if (cornerType == MGCornerTypeTop) {
        self.containerView.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
        self.containerView.layer.cornerRadius = cornerRadius;
    } else if (cornerType == MGCornerTypeCenter) {
        self.containerView.layer.masksToBounds = NO;
        self.containerView.layer.cornerRadius = 0;
    } else if (cornerType == MGCornerTypeBottom) {
        self.containerView.layer.maskedCorners = kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
        self.containerView.layer.cornerRadius = cornerRadius;
        self.firstLineView.hidden = YES;
    } else if (cornerType == MGCornerTypeAll) {
        self.containerView.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner | kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner;
        self.containerView.layer.cornerRadius = cornerRadius;
        self.firstLineView.hidden = YES;
    }
}

- (void)configBorderTypeWithDataModels:(NSMutableArray *)dataModels curObj:(nonnull id)curObj {
    if (dataModels.count > 0) {
        id firstObj = [dataModels firstObject];
        id finalObj = [dataModels lastObject];
        
        if (firstObj == finalObj) {
            [self configViewWithCornerType:MGCornerTypeAll cornerRadius:6];
        } else {
            if (curObj == firstObj) {
                [self configViewWithCornerType:MGCornerTypeTop cornerRadius:6];
            } else if (curObj == finalObj) {
                [self configViewWithCornerType:MGCornerTypeBottom cornerRadius:6];
            } else {
                [self configViewWithCornerType:MGCornerTypeCenter];
            }
        }
    }
}

@end
