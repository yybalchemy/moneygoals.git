//
//  MGIconCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGIconCell.h"

@implementation MGIconCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    CGFloat itemSize = (kScreenWidth - 90) / 6;
    
    self.wrapperView = [UIView createViewAtSuperView:self.contentView backgroundColor:APP_COLOR_LIGHT_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } configureBlock:^(UIView *view) {
        [view cornerRadius:itemSize / 2];
    }];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.wrapperView);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithDataModel:(NSString *)dataModel {
    self.firstIconView.image = [UIImage imageNamed:dataModel];
}

@end
