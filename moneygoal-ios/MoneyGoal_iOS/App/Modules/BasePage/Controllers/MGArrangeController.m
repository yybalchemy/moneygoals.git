//
//  MGArrangeController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGArrangeController.h"

@interface MGArrangeController ()
@property (nonatomic, strong) MGBaseFlowLayout *layout;

@end

@implementation MGArrangeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = APP_COLOR_BACKGROUND;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.layout = [[MGBaseFlowLayout alloc] initWithCellHeight:50 cellOffset:5 contentBottomInset:10];
    self.layout.dataModels = [self getArrangeDataModels];
    [self configFlowLayout:self.layout];
    
    @weakify(self);
    self.layout.arrangeSuccBlock = ^{
        @strongify(self);
        if ([self respondsToSelector:@selector(handleArrangeComplete)]) {
            [self handleArrangeComplete];
        }
    };
    self.collectionView.collectionViewLayout = self.layout;
    
    self.actionsView = [MGBaseActionsView createViewAtSuperView:self.view constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.mas_equalTo(65 + [UIDevice safeAreaInsetsBottom]);
    } configureBlock:^(MGBaseActionsView *view) {
        view.createBlock = ^{
            @strongify(self);
            if ([self respondsToSelector:@selector(handleCreate)]) {
                [self handleCreate];
            }
        };
    }];
    [self.actionsView changeCreate];
}

- (void)beginQueryData {
    [super beginQueryData];
    [self refreshData];
}

- (NSString *)emptyModelsRemindStr {
    return @"请先添加一条数据";
}

- (void)configFlowLayout:(MGBaseFlowLayout *)layout {
    
}

- (NSMutableArray *)getArrangeDataModels {
    return [NSMutableArray new];
}

- (void)refreshData {
    if ([self getArrangeDataModels].count == 0) {
        UIEdgeInsets insets = UIEdgeInsetsMake([self customPageHeaderHeight], 0, 65 + [UIDevice safeAreaInsetsBottom], 0);
        self.refreshAlertView = [MGAlertView showOnlyTextAlertViewInView:self.view insets:insets title:[self emptyModelsRemindStr]];
    } else {
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
    }
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self getArrangeDataModels].count;
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 10;
    return insets;
}

@end
