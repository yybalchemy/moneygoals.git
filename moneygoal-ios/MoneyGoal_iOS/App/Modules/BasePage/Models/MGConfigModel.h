//
//  MGConfigModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MGUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGConfigModel : NSObject

@property (nonatomic, strong) MGUserModel *userModel;

// 是否已经点过好评评价的界面
@property (nonatomic) NSInteger createCount;
@property (nonatomic) BOOL isCommented;

// 是否需要保存到oss
@property (nonatomic) BOOL enableOSS;
@property (nonatomic) BOOL enableTimeMemory;
@property (nonatomic) NSTimeInterval timeMemoryTs;

// 第一次进入app，需要显示登录页面
@property (nonatomic) BOOL isStarted;

@end

NS_ASSUME_NONNULL_END
