//
//  MGBaseFlowLayout.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <YYBBaseViews/YYBCollectionViewCell.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGBaseFlowLayout : UICollectionViewFlowLayout

- (instancetype)initWithCellHeight:(CGFloat)cellHeight cellOffset:(CGFloat)cellOffset contentBottomInset:(CGFloat)contentBottomInset;

@property (nonatomic, strong) NSMutableArray *attributes;
@property (nonatomic, strong) NSMutableArray *dataModels;

@property (nonatomic, copy) YYBTapedActionBlock arrangeSuccBlock;

@end

NS_ASSUME_NONNULL_END
