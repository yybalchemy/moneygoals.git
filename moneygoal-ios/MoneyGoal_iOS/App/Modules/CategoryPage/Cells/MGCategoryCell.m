//
//  MGCategoryCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategoryCell.h"

@implementation MGCategoryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.equalTo(self.contentView);
    } configureBlock:^(UIView *view) {
        [view cornerRadius:20];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(15);
        make.centerY.equalTo(self.contentView);
    } configureBlock:nil];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"ico_common_right" constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.firstIconView);
        make.right.equalTo(self.contentView).offset(-15);
    } configureBlock:nil];
    
    [UIView createBottomSeparateViewAtSuperView:self color:APP_COLOR_LIGHT_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    return self;
}

- (void)configViewWithDataModel:(MGCategoryModel *)dataModel
{
    self.firstStringLabel.text = dataModel.name;
    self.firstIconView.image = [UIImage imageNamed:dataModel.icon];
}

@end
