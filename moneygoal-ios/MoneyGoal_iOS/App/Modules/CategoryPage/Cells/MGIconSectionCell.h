//
//  MGIconSectionCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/2.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <UIKit/UIkit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGIconSectionCell : UICollectionReusableView

@end

NS_ASSUME_NONNULL_END
