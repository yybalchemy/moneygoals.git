//
//  MGCategoryIconController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCollectionController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCategoryIconController : MGCollectionController

@property (nonatomic, copy) void (^ selectIconBlock)(NSString *iconName);

@end

NS_ASSUME_NONNULL_END
