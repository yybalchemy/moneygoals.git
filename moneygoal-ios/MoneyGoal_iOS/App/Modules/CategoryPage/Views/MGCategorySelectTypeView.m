//
//  MGCategorySelectTypeView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategorySelectTypeView.h"

@interface MGCategorySelectTypeView ()
@property (nonatomic, strong) UILabel *firstStringLabel;

@end

@implementation MGCategorySelectTypeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.backgroundColor = APP_COLOR_LIGHT_SAPERATE;
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.centerY.equalTo(self);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(14);
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    return self;
}

- (void)configViewWithDataModel:(NSString *)dataModel
{
    self.firstStringLabel.text = dataModel;
}

@end
