//
//  MGCategorySelectView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCategorySelectView : YYBBaseView

@property (nonatomic, copy) void (^ dataModelSelectBlock)(MGCategoryModel *dataModel);
@property (nonatomic, copy) YYBTapedActionIndexBlock selectTypeBlock;
@property (nonatomic, strong) MGCategoryModel *selectModel;

- (void)configViewWithDataModel:(MGCategoryModel *)dataModel shouldShowAllCategories:(BOOL)shouldShowAllCategories selectAllCategories:(BOOL)selectAllCategories hidesAllActions:(BOOL)hidesAllActions;

@end

NS_ASSUME_NONNULL_END
