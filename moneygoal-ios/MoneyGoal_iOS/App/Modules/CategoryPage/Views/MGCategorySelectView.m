//
//  MGCategorySelectView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/26.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCategorySelectView.h"

#import "MGCategorySelectCell.h"
#import "MGAccountEmptyCell.h"

#import "MGCategorySelectTypeView.h"

@interface MGCategorySelectView ()
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *showAllButton;

@end

@implementation MGCategorySelectView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_add"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeCreate);
        }
    }];
    
    self.secondActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.firstActionButton);
        make.left.equalTo(self.firstActionButton.mas_right).offset(20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_update"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeList);
        }
    }];
    
    self.thirdActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.firstActionButton);
        make.right.equalTo(self).offset(-20);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_dismiss"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeCancel);
        }
    }];
    
    self.showAllButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 32));
        make.centerY.equalTo(self.firstActionButton);
        make.right.equalTo(self.thirdActionButton.mas_left).offset(-20);
    } configureBlock:^(UIButton *view) {
        view.titleLabel.font = FONT_REGULAR(14);
        [view setTitle:@"全部" forState:0];
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:UIControlStateSelected];
        [view setTitleColor:TEXT_COLOR_FIRST forState:UIControlStateSelected];
        [view setBackgroundImage:APP_COLOR_SAPERATE.colorToUIImage forState:0];
        [view setTitleColor:TEXT_COLOR_SECOND forState:0];
        [view cornerRadius:16];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.selectTypeBlock) {
            self.selectTypeBlock(MGAlertViewActionTypeShowAll);
        }
    }];
    
    CGFloat padding = 10;
    self.gatherView = [UICollectionView createAtSuperView:self delegeteTarget:self edgeInsets:UIEdgeInsetsMake(0, 10, 0, 10) scrollDirection:UICollectionViewScrollDirectionVertical constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(435);
    } dequeueCellIdentifiers:@[@"MGCategorySelectCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        layout.itemSize = CGSizeMake((kScreenWidth - padding * 5) / 6, 80);
        layout.minimumLineSpacing = padding;
        layout.minimumInteritemSpacing = padding;
        
        [view registerClass:[MGCategorySelectTypeView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGCategorySelectTypeView"];
    }];
    
    [UIView createTopSeparateViewAtSuperView:self color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    return self;
}

- (void)configViewWithDataModel:(MGCategoryModel *)dataModel shouldShowAllCategories:(BOOL)shouldShowAllCategories selectAllCategories:(BOOL)selectAllCategories hidesAllActions:(BOOL)hidesAllActions
{
    _selectModel = dataModel;
    
    if (hidesAllActions) {
        self.firstActionButton.hidden = YES;
        self.secondActionButton.hidden = YES;
    }
    
    self.showAllButton.hidden = !shouldShowAllCategories;
    self.showAllButton.selected = selectAllCategories;
    [self.gatherView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [MGCategoryAccessor getStandardCategoryTypes].count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    NSArray *categoryModels = [self getCategoryModelsWithSectionIndex:section];
    if (categoryModels.count == 0) return CGSizeMake(kScreenWidth, 0);
    return CGSizeMake(kScreenWidth, 35);
}

- (NSArray *)getCategoryModelsWithSectionIndex:(NSInteger)sectionIndex
{
    NSDictionary *data = [MGCategoryAccessor getStandardCategoryTypes][sectionIndex];
    MGCategoryType type = [data[@"type"] integerValue];
    return [MGCategoryAccessor.shared getCategoryModelsWithCategoryType:type];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    MGCategorySelectTypeView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MGCategorySelectTypeView" forIndexPath:indexPath];
    NSDictionary *dict = [MGCategoryAccessor getStandardCategoryTypes][indexPath.section];
    [view configViewWithDataModel:dict[@"name"]];
    return view;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *categoryModels = [self getCategoryModelsWithSectionIndex:section];
    return categoryModels.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *categoryModels = [self getCategoryModelsWithSectionIndex:indexPath.section];
    MGCategoryModel *dataModel = categoryModels[indexPath.row];
    
    MGCategorySelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGCategorySelectCell" forIndexPath:indexPath];
    
    BOOL isSelect = _selectModel && _selectModel.uniqueId == dataModel.uniqueId;
    [cell configViewWithDataModel:dataModel status:isSelect];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *categoryModels = [self getCategoryModelsWithSectionIndex:indexPath.section];
    MGCategoryModel *dataModel = categoryModels[indexPath.row];
    if (self.dataModelSelectBlock) {
        self.dataModelSelectBlock(dataModel);
    }
}

@end
