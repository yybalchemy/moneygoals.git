//
//  MGDateRecordsController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/10.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGDateRecordsController : MGTableController

@property (nonatomic, copy) YYBTapedActionBlock updateSuccBlock;
@property (nonatomic, copy) YYBTapedActionBlock deleteSuccBlock;

@end

NS_ASSUME_NONNULL_END
