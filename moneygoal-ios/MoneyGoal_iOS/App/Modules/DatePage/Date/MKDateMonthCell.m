//
//  MKDateMonthCell.m
//  365money-iOS
//
//  Created by alchemy on 2021/3/30.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MKDateMonthCell.h"
#import "MKDateMonthDayCell.h"
#import "NSDate+MKAdd.h"

@implementation MKDateMonthCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.monthCollectionView = [UICollectionView createAtSuperView:self.contentView delegeteTarget:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    } dequeueCellIdentifiers:@[@"MKDateMonthDayCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        
        view.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    }];
    
    return self;
}

- (void)configViewWithDataModel:(NSDate *)dataModel
{
    _date = dataModel;
    [self.monthCollectionView reloadData];
}

- (NSInteger)rowCountForDate
{
    NSInteger days = self.date.daysOfMonth;
    NSInteger startIndex = [self.date weekIndex];
    
    NSInteger rowCount = (days + startIndex - 1) / 7 + 1;
    
    return rowCount;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowCount = [self rowCountForDate];
    return CGSizeMake((kScreenWidth - 20) / 7, 50 * 6 / rowCount);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger rowCount = [self rowCountForDate];
    return rowCount * 7;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MKDateMonthDayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MKDateMonthDayCell" forIndexPath:indexPath];
    
    NSInteger daysDiff = [self.date weekIndex];;
    NSDate *date = [self.date dateByAddingDays:indexPath.row - daysDiff];
    
    // 超过或者没达到这个日期
    if (indexPath.row < daysDiff || indexPath.row >= daysDiff + self.date.daysOfMonth) {
        cell.firstStringLabel.textColor = TEXT_COLOR_THIRD;
        cell.firstStringLabel.font = FONT_MEDIUM(15);
        cell.firstStringLabel.text = @(date.components.day).stringValue;
        cell.secondStringLabel.text = nil;
    } else {
        if ([date isToday]) {
            cell.firstStringLabel.text = @"今";
            cell.firstStringLabel.font = FONT_MEDIUM(16);
        } else {
            cell.firstStringLabel.text = @(date.components.day).stringValue;
            cell.firstStringLabel.font = FONT_MEDIUM(15);
        }
        
        // 如果日期大于今天
        if ([NSDate compareDaysWithDate:[NSDate date] toOtherDate:date] > 0) {
            cell.firstStringLabel.text = @(date.components.day).stringValue;
            cell.firstStringLabel.font = FONT_MEDIUM(15);
            cell.firstStringLabel.textColor = TEXT_COLOR_THIRD;
            cell.secondStringLabel.text = nil;
        } else {
            BOOL selected = NO;
            if (self.dateSelectStatusBlock) {
                selected = self.dateSelectStatusBlock(date);
            }
            
            if (selected) {
                cell.firstStringLabel.textColor = APP_COLOR_MAIN;
                cell.secondStringLabel.textColor = APP_COLOR_MAIN;
            } else {
                cell.firstStringLabel.textColor = TEXT_COLOR_FIRST;
                cell.secondStringLabel.textColor = TEXT_COLOR_SECOND;
            }
        }
        
        MGRecordModel *selectModel = [MGRecordAccessor.shared getSelectedRecordModel];
        if (selectModel) {
            NSDecimalNumber *remainsMoney = [selectModel getDayMoneyWithDate:date accountUniqueId:0 categoryUniqueId:0];
            cell.secondStringLabel.text = remainsMoney.floatValue != 0 ? remainsMoney.stringValue : @"";
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger daysDiff = [self.date weekIndex];;
    NSDate *date = [self.date dateByAddingDays:indexPath.row - daysDiff];
    
    if ([NSDate compareDaysWithDate:[NSDate date] toOtherDate:date] <= 0) {
        if (self.dateSelectedBlock) {
            self.dateSelectedBlock(date);
        }
    }
}

@end
