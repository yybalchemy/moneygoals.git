//
//  MKDatePickerWeekendCell.m
//  365money-iOS
//
//  Created by alchemy on 2021/3/25.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MKDatePickerWeekendCell.h"

@implementation MKDatePickerWeekendCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    } configureBlock:^(UILabel *view) {
        
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    self.firstStringLabel.text = titleValue;
}

@end
