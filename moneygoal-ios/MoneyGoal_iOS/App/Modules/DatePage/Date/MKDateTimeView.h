//
//  MKDateTimeView.h
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/20.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKDateTimeView : YYBBaseView

@property (nonatomic, strong) UIDatePicker *datePicker;

@end

NS_ASSUME_NONNULL_END
