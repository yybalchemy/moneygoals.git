//
//  MKDateTimeView.m
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/20.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MKDateTimeView.h"

@implementation MKDateTimeView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.datePicker = [UIDatePicker createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(220);
        
    } configureBlock:^(UIDatePicker *view) {
        
        view.datePickerMode = UIDatePickerModeDateAndTime;
        if(@available(iOS 13.4, *)) {
            view.preferredDatePickerStyle = UIDatePickerStyleWheels;
        }
    }];
    
    return self;
}

@end
