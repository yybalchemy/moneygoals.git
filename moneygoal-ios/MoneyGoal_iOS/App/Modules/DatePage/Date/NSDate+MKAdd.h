//
//  NSDate+MKAdd.h
//  365money-iOS
//
//  Created by alchemy on 2021/4/4.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (MKAdd)

- (NSInteger)weekIndex;

- (NSString *)getLunarDate;
- (NSString *)getFestivalDate;

- (NSInteger)rowCountForDate;

@end

NS_ASSUME_NONNULL_END
