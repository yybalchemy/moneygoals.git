//
//  MGReceiptImageCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/30.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptImageCell.h"

@implementation MGReceiptImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.center.equalTo(self.contentView);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue {
    self.firstIconView.image = [UIImage imageNamed:titleValue];
}

@end
