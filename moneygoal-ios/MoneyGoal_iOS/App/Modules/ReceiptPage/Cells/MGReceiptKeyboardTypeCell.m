//
//  MGReceiptKeyboardTypeCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptKeyboardTypeCell.h"

@implementation MGReceiptKeyboardTypeCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstActionButton = [UIButton createAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.center.equalTo(self.contentView);
    } configureBlock:^(UIButton *view) {
        view.userInteractionEnabled = NO;
    }];
    
    return self;
}

- (void)configViewWithDataModel:(NSString *)dataModel {
    [self.firstActionButton setBackgroundImage:[UIImage imageNamed:dataModel] forState:0];
}

@end
