//
//  MGReceiptRemarkCell.h
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/12.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptRemarkCell : MGBaseCornerCell

@end

NS_ASSUME_NONNULL_END
