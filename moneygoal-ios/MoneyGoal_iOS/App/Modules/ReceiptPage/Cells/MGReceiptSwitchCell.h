//
//  MGReceiptSwitchCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/24.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptSwitchCell : YYBCollectionViewCell

@end

NS_ASSUME_NONNULL_END
