//
//  MGReceiptSwitchCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/24.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptSwitchCell.h"

@implementation MGReceiptSwitchCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(15) textColor:nil constraintBlock:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    self.firstStringLabel.textColor = status ? TEXT_COLOR_FIRST : TEXT_COLOR_SECOND;
    self.firstStringLabel.text = titleValue;
    self.firstStringLabel.font = status ? FONT_MEDIUM(18) : FONT_REGULAR(15);
}

@end
