//
//  MGReceiptDateModel.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/29.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptDateModel : NSObject

@property (nonatomic, strong) NSMutableArray *dataModels;
// 这个月的开始的第一天的第一秒
@property (nonatomic, strong) NSDate *startOfDay;
@property (nonatomic, copy) NSString *date;

@property (nonatomic, strong) NSDecimalNumber *incomeMoney, *paidMoney;
- (NSDictionary *)getMoneyValuesWithRecordModel:(MGRecordModel *)recordModel accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId;

@end

NS_ASSUME_NONNULL_END
