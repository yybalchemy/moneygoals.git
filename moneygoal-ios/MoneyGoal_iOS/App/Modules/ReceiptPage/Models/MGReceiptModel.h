//
//  MGReceiptModel.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/21.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptModel : NSObject <NSCopying>

@property (nonatomic) NSUInteger uniqueId;
@property (nonatomic) NSTimeInterval shouldSaveTime; // 应该存储的时间 365模式
@property (nonatomic) NSTimeInterval actualSaveTime; // 打卡的时间，记录收支的时间

@property (nonatomic) MGReceiptType receiptType;
@property (nonatomic, strong) NSDecimalNumber *shouldSaveMoney;
@property (nonatomic, strong) NSDecimalNumber *savedMoney;
@property (nonatomic, copy) NSString *remark, *content;

@property (nonatomic) NSUInteger recordUniqueId;
@property (nonatomic) NSUInteger categoryUniqueId; // 类型，例如早饭、晚饭
@property (nonatomic) NSUInteger accountUniqueId;

@property (nonatomic) BOOL dateOrTime;

- (NSString *)getReceiptMoneyString;

@end

NS_ASSUME_NONNULL_END
