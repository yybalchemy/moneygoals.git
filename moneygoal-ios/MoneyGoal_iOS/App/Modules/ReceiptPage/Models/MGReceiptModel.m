//
//  MGReceiptModel.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/21.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGReceiptModel.h"

@implementation MGReceiptModel

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _uniqueId = [NSString createUniqueId];
    
    return self;
}

- (NSString *)getReceiptMoneyString {
    if (_savedMoney) {
        return [self formatFloat:_savedMoney.floatValue];
    } else {
        return [self formatFloat:_shouldSaveMoney.floatValue];
    }
}

- (NSString *)formatFloat:(float)f {
    if (fmodf(f, 1)==0) { // 无有效小数位
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) { // 如果有一位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else { // 如果有两位或以上小数点
        return [NSString stringWithFormat:@"%.2f",f];
    }
}

@end
