//
//  MGReceiptKeyBoardView.h
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/21.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptKeyBoardView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock dismissTapedBlock;
@property (nonatomic, copy) YYBTapedActionBlock completeTapedBlock;
@property (nonatomic, copy) YYBTapedActionIndexBlock actionTapedBlock;

@property (nonatomic) BOOL hidesDeleteAction;

@end

NS_ASSUME_NONNULL_END
