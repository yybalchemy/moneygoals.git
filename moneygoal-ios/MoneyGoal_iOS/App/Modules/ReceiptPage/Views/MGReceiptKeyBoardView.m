//
//  MGReceiptKeyBoardView.m
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/4/21.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import "MGReceiptKeyBoardView.h"
#import "MGReceiptKeyButton.h"
#import "MGReceiptKeyboardTypeCell.h"
#import "MGReceiptImageCell.h"

@implementation MGReceiptKeyBoardView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self backgroundColor:[UIColor whiteColor] constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self).offset(2);
        make.height.mas_equalTo(45);
    } configureBlock:^(UIView *view) {
        [view cornerRadius:6];
    }];
    
    self.gatherView = [UICollectionView createAtSuperView:self.wrapperView delegeteTarget:self edgeInsets:UIEdgeInsetsZero scrollDirection:UICollectionViewScrollDirectionHorizontal constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.wrapperView);
        make.right.equalTo(self.wrapperView);
        make.bottom.equalTo(self.wrapperView);
        make.height.mas_equalTo(45);
    } dequeueCellIdentifiers:@[@"MGReceiptImageCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        
    }];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.wrapperView).offset(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.bottom.equalTo(self.wrapperView).offset(-10);
    } configureBlock:^(UIButton *view) {
        [view cornerRadius:6];
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_dismiss"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.dismissTapedBlock) {
            self.dismissTapedBlock();
        }
    }];
    
    CGFloat margin = 5;
    CGFloat paddingHoris = 10;
    // 右侧的小按钮
    CGFloat rightActionWidth = 60;
    // 数字的宽度
    CGFloat widthNumber = (kScreenWidth - rightActionWidth - 2 * paddingHoris - margin * 3) / 3;
    CGFloat heightNumber = 50;
    
    self.firstWrapperView = [UIView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(- [UIDevice safeAreaInsetsBottom] - 20);
        make.height.mas_equalTo(heightNumber * 4 + 5 * margin);
    } configureBlock:^(UIView *view) {
        view.backgroundColor = [UIColor clearColor];
    }];
    
    NSArray *keys = @[@"1",@"2",@"3",@"-",@"4",@"5",@"6",@"+",@"7",@"8",@"9",@".",@"0",@""];
    for (NSInteger index = 0; index < keys.count; index ++) {
        
        BOOL last = index == keys.count - 1;
        UIButton *button = [UIButton createAtSuperView:self.firstWrapperView cornerRadius:4 fontValue:FONT_MEDIUM(22) title:keys[index] titleColor:TEXT_COLOR_FIRST constraintBlock:nil configureBlock:^(UIButton *view) {
            if (last) {
                [view setImage:[UIImage imageNamed:@"keyboard_back"] forState:0];
            }
            
            [view setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
            [view setBackgroundImage:[[UIColor whiteColor] colorWithAlphaComponent:0.6].colorToUIImage forState:UIControlStateHighlighted];
            [view cornerRadius:6];
        } tapedBlock:^(UIButton *view) {
            @strongify(self);
            if (self.stringChangedBlock) {
                NSString *key = last ? @"C" : keys[index];
                self.stringChangedBlock(key);
            }
        }];
        
        if (index == 0) {
            button.frame = CGRectMake(paddingHoris, margin, widthNumber, heightNumber);
        } else if (index == 1) {
            button.frame = CGRectMake(paddingHoris + margin + widthNumber, margin, widthNumber, heightNumber);
        } else if (index == 2) {
            button.frame = CGRectMake(paddingHoris + margin * 2 + widthNumber * 2, margin, widthNumber, heightNumber);
        } else if (index == 3) {
            button.frame = CGRectMake(paddingHoris + margin * 3 + widthNumber * 3, margin, rightActionWidth, heightNumber);
        } else if (index == 4) {
            button.frame = CGRectMake(paddingHoris, margin * 2 + heightNumber, widthNumber, heightNumber);
        } else if (index == 5) {
            button.frame = CGRectMake(paddingHoris + margin + widthNumber, margin * 2 + heightNumber, widthNumber, heightNumber);
        } else if (index == 6) {
            button.frame = CGRectMake(paddingHoris + margin * 2 + widthNumber * 2, margin * 2 + heightNumber, widthNumber, heightNumber);
        } else if (index == 7) {
            button.frame = CGRectMake(paddingHoris + margin * 3 + widthNumber * 3, margin * 2 + heightNumber, rightActionWidth, heightNumber);
        } else if (index == 8) {
            button.frame = CGRectMake(paddingHoris, margin * 3 + heightNumber * 2, widthNumber, heightNumber);
        } else if (index == 9) {
            button.frame = CGRectMake(paddingHoris + margin + widthNumber, margin * 3 + heightNumber * 2, widthNumber, heightNumber);
        } else if (index == 10) {
            button.frame = CGRectMake(paddingHoris + margin * 2 + widthNumber * 2, margin * 3 + heightNumber * 2, widthNumber, heightNumber);
        } else if (index == 11) {
            button.frame = CGRectMake(paddingHoris, margin * 4 + heightNumber * 3, widthNumber, heightNumber);
        } else if (index == 12) {
            button.frame = CGRectMake(paddingHoris + margin + widthNumber, margin * 4 + heightNumber * 3, widthNumber, heightNumber);
        } else if (index == 13) {
            button.frame = CGRectMake(paddingHoris + margin * 2 + widthNumber * 2, margin * 4 + heightNumber * 3, widthNumber, heightNumber);
        }
    }
    
    MGReceiptKeyButton *button = [MGReceiptKeyButton createAtSuperView:self.firstWrapperView constraintBlock:nil configureBlock:^(UIButton *view) {
        
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.completeTapedBlock) {
            self.completeTapedBlock();
        }
    }];

    button.frame = CGRectMake(paddingHoris + margin * 3 + widthNumber * 3, margin * 3 + heightNumber * 2, rightActionWidth, heightNumber * 2 + margin);
    return self;
}

- (void)setHidesDeleteAction:(BOOL)hidesDeleteAction {
    _hidesDeleteAction = hidesDeleteAction;
    [self.gatherView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _hidesDeleteAction ? 0 : 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGReceiptImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGReceiptImageCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        [cell configViewWithTitleValue:@"ico_common_delete"];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.actionTapedBlock) {
        self.actionTapedBlock(indexPath.row);
    }
}

@end
