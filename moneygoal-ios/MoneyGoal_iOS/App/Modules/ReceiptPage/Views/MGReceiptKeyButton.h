//
//  MGReceiptKeyButton.h
//  SaveMoney-iOS
//
//  Created by alchemy on 2021/5/15.
//  Copyright © 2021 Dashsnake Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGReceiptKeyButton : UIButton

@end

NS_ASSUME_NONNULL_END
