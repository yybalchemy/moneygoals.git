//
//  MG365RecordsDateCell.m
//  365money-iOS
//
//  Created by alchemy on 2021/2/28.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MG365RecordsDateCell.h"

@implementation MG365RecordsDateCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(5) textColor:TEXT_COLOR_FIRST constraintBlock:nil configureBlock:^(UILabel *view) {
        view.textAlignment = NSTextAlignmentCenter;
    }];

    return self;
}

- (void)viewBeginConfigConstraints {
    [self.firstStringLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

- (void)configViewWithDataModel:(MGReceiptModel *)dataModel {
    self.firstStringLabel.text = [NSString stringWithFormat:@"%.1f",dataModel.shouldSaveMoney.floatValue];
}

@end
