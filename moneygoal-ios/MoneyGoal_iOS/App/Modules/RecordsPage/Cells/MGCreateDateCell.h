//
//  MGCreateDateCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/11.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseCornerCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCreateDateCell : MGBaseCornerCell

- (void)configViewWithDate:(NSDate *)date;

@end

NS_ASSUME_NONNULL_END
