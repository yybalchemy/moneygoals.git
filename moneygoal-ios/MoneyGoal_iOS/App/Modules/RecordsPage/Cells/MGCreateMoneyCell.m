//
//  MGCreateMoneyCell.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/11.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCreateMoneyCell.h"

@implementation MGCreateMoneyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createAtSuperView:self.containerView iconName:@"cell_money" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.containerView);
    } configureBlock:nil];
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(45);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
        view.text = @"间隔金额";
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(10) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-20);
        make.bottom.equalTo(self.firstStringLabel).offset(-1);
    } configureBlock:^(UILabel *view) {
        [view setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
        view.text = @"RMB";
    }];
        
    self.inputStringView = [UITextField createAtSuperView:self.containerView leftViewOffset:0 cornerRadius:0 fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-50);
        make.left.equalTo(self.containerView.mas_centerX);
        make.top.bottom.equalTo(self.containerView);
    } configureBlock:^(UITextField *view) {
        view.clearButtonMode = UITextFieldViewModeWhileEditing;
        view.textAlignment = NSTextAlignmentRight;
        view.placeholder = @"输入间隔金额";
        view.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    
    @weakify(self);
    [self.inputStringView.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        @strongify(self);
        if (self.stringChangedBlock) {
            self.stringChangedBlock(x);
        }
    }];
    
    [UIView createBottomSeparateViewAtSuperView:self.contentView color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 25, 0, 25)];
    
    return self;
}

- (void)configViewWithMoney:(CGFloat)money {
    NSString *moneyStr = [NSString stringWithFormat:@"%.1f", money];
    self.inputStringView.text = moneyStr;
}

@end
