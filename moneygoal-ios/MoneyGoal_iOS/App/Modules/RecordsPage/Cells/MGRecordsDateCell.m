//
//  MGRecordsDateCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsDateCell.h"

@implementation MGRecordsDateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.contentView);
    } configureBlock:nil];
    
    [UIView createBottomSeparateViewAtSuperView:self.contentView color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsZero];
    
    return self;
}

- (void)configViewWithDataModel:(MGReceiptDateModel *)dataModel incomeMoney:(NSDecimalNumber *)incomeMoney paidMoney:(NSDecimalNumber *)paidMoney {
    self.firstStringLabel.text = dataModel.date;
    
    NSString *incomeMoneyValue = [[NSString stringWithFormat:@"%.2f", incomeMoney.floatValue] formatMoneyString];
    NSString *paidMoneyValue = [[NSString stringWithFormat:@"%.2f", paidMoney.floatValue] formatMoneyString];
    NSString *str = [NSString stringWithFormat:@"收入 %@   支出 %@", incomeMoneyValue, paidMoneyValue];
    
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:str];
//    [attr addAttribute:NSForegroundColorAttributeName value:TEXT_COLOR_INCOME range:NSMakeRange(3, incomeMoneyValue.length)];
//    [attr addAttribute:NSFontAttributeName value:FONT_REGULAR(12) range:NSMakeRange(3, incomeMoneyValue.length)];
//    [attr addAttribute:NSForegroundColorAttributeName value:TEXT_COLOR_PAID range:NSMakeRange(incomeMoneyValue.length + 9, paidMoneyValue.length)];
//    [attr addAttribute:NSFontAttributeName value:FONT_REGULAR(12) range:NSMakeRange(incomeMoneyValue.length + 9, paidMoneyValue.length)];
    self.secondStringLabel.attributedText = attr;
}

@end
