//
//  MGRecordsHeaderCell.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsHeaderCell : YYBCollectionViewCell

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue iconName:(NSString *)iconName;

@end

NS_ASSUME_NONNULL_END
