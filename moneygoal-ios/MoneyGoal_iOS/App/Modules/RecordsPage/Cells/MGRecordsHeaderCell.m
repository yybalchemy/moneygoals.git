//
//  MGRecordsHeaderCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsHeaderCell.h"

@implementation MGRecordsHeaderCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:20];
        view.backgroundColor = APP_COLOR_LIGHT_SAPERATE;
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(15);
        make.top.equalTo(self.firstIconView);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(3);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue iconName:(NSString *)iconName {
    self.firstIconView.image = [UIImage imageNamed:iconName];
    self.firstStringLabel.text = titleValue;
    self.secondStringLabel.text = [detailValue formatMoneyString];
}

@end
