//
//  MGRecordsPreviousDayCell.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsPreviousDayCell : YYBTableViewCell

@end

NS_ASSUME_NONNULL_END
