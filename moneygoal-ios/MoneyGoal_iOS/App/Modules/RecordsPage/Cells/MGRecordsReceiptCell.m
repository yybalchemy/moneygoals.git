//
//  MGRecordsReceiptCell.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/29.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsReceiptCell.h"

@implementation MGRecordsReceiptCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.contentView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.contentView).offset(15);
        make.size.mas_equalTo(CGSizeMake(34, 34));
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:17];
        view.backgroundColor = [UIColor colorWithHexValue:0xE6E6E6 alpha:0.3];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(15);
        make.bottom.equalTo(self.firstIconView.mas_centerY).offset(-2);
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.contentView).offset(35);
    } configureBlock:nil];
    
    self.thirdStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_MEDIUM(14) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-30);
        make.bottom.equalTo(self.firstIconView.mas_centerY).offset(-2);
    } configureBlock:nil];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.thirdStringLabel.mas_right).offset(2);
        make.centerY.equalTo(self.thirdStringLabel);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    self.forthStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(12) textColor:APP_COLOR_MAIN constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.secondStringLabel.mas_bottom).offset(4);
        make.right.equalTo(self.contentView).offset(-20);
    } configureBlock:nil];
    
    [UIView createBottomSeparateViewAtSuperView:self.contentView color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 65, 0, 0)];
    
    return self;
}

- (void)configViewWithDataModel:(MGReceiptModel *)dataModel {
    MGCategoryModel *categoryModel = [MGCategoryAccessor.shared getCategoryModelWithUniqueId:dataModel.categoryUniqueId];
    self.firstStringLabel.text = categoryModel.name;
    
    MGAccountModel *accountModel = [MGAccountAccessor.shared getAccountModelWithUniqueId:dataModel.accountUniqueId];
    NSDate *createDate = [NSDate dateWithTimeIntervalSince1970:dataModel.actualSaveTime];
    NSString *createDateStr = [createDate getDateOffsetString];
    self.secondStringLabel.text = [NSString stringWithFormat:@"%@ %@", accountModel.name, createDateStr];
    
    NSString *moneyValue = dataModel.savedMoney.stringValue;
    NSString *attachValue = @"";
    if (dataModel.receiptType == MGReceiptTypePaid) {
        attachValue = @"-";
    }
    
    self.thirdStringLabel.text = [NSString stringWithFormat:@"%@￥%@",attachValue, [moneyValue formatMoneyString]];
    self.firstIconView.image = [UIImage imageNamed:categoryModel.icon];
    
    self.forthStringLabel.text = dataModel.remark;
}

+ (CGFloat)queryViewHeightWithDataModel:(MGReceiptModel *)dataModel {
    if (dataModel.remark.isExist) return 85;
    return 65;
}

@end
