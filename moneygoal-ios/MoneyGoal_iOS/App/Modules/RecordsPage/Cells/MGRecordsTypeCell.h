//
//  MGRecordsTypeCell.h
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsTypeCell : YYBCollectionViewCell

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue iconName:(NSString *)iconName;

@end

NS_ASSUME_NONNULL_END
