//
//  MGReceiptController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordController : MGTableController

@property (nonatomic, strong) MGRecordModel *dataModel;
@property (nonatomic, strong) MGRecordModel *updateModel;
@property (nonatomic) MGRecordType recordType;

@property (nonatomic, copy) YYBTapedActionBlock createRecordModelBlock;
@property (nonatomic, copy) YYBTapedActionBlock updateRecordModelBlock;
@property (nonatomic, copy) YYBTapedActionBlock deleteRecordModelBlock;

@end

NS_ASSUME_NONNULL_END
