//
//  MGRecordsController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/8.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGTableController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsController : MGTableController

@end

NS_ASSUME_NONNULL_END
