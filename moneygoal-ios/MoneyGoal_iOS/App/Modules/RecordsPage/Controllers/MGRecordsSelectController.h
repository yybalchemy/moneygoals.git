//
//  MGRecordsSelectController.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/3.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGCollectionController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsSelectController : MGCollectionController

@property (nonatomic, copy) YYBTapedActionBlock updateRecordsBlock;
@property (nonatomic, copy) YYBTapedActionBlock changeRecordBlock;

@end

NS_ASSUME_NONNULL_END
