//
//  MG365RecordsGridView.h
//  365money-iOS
//
//  Created by alchemy on 2021/10/16.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MG365RecordsGridView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) MGRecordModel *dataModel;

@property (nonatomic, copy) YYBTapedActionBlock updateMoneyBlock;
@property (nonatomic, copy) void (^ handleReceiptBlock)(MGReceiptModel *receiptModel);

@end

NS_ASSUME_NONNULL_END
