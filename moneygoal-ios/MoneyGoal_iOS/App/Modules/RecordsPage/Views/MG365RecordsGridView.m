//
//  MG365RecordsGridView.m
//  365money-iOS
//
//  Created by alchemy on 2021/10/16.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MG365RecordsGridView.h"
#import "MG365RecordsHeartView.h"
#import "MG365RecordsDateCell.h"

#define MAX_UNDEFINE_NUM 99999
#define MAX_UNDEFINE_END 100000

@interface MG365RecordsGridView ()
@property (nonatomic, strong) MG365RecordsHeartView *backgroundGridView;

@end

@implementation MG365RecordsGridView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.collectionView = [UICollectionView createAtSuperView:self delegeteTarget:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 10, 0, 10));
    } dequeueCellIdentifiers:[self dequeueCellIdentifiers] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        view.backgroundColor = [UIColor clearColor];
    }];
    
    self.backgroundGridView = [MG365RecordsHeartView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self).offset(0);
        CGFloat width = (kScreenWidth - 20) / 25;
        make.height.mas_equalTo(width * 23);
    } configureBlock:^(MG365RecordsHeartView *view) {
        view.backgroundColor = [UIColor clearColor];
        view.userInteractionEnabled = NO;
    }];
    
    return self;
}

- (void)setDataModel:(MGRecordModel *)dataModel
{
    _dataModel = dataModel;
    [_collectionView reloadData];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers
{
    return @[@"MG365RecordsDateCell"];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = (kScreenWidth - 20) / 25;
    return CGSizeMake(width, width);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataModel ? 22 * 25 : 0;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MG365RecordsDateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MG365RecordsDateCell" forIndexPath:indexPath];
    
    NSInteger actualIndex = [self actualIndexWithIndexPath:indexPath.row];
    if (actualIndex == MAX_UNDEFINE_NUM) {
        cell.firstStringLabel.text = @"";
        cell.backgroundColor = [UIColor clearColor];
        
    } else if (actualIndex == MAX_UNDEFINE_END) {
        cell.firstStringLabel.text = @"❤️";
        cell.backgroundColor = [UIColor whiteColor];
        
    } else {
        MGReceiptModel *dataModel = nil;
        if (self.dataModel && self.dataModel.receiptModels.count > actualIndex) {
            dataModel = [self.dataModel.receiptModels objectAtIndex:actualIndex];
        }

        CGFloat money = dataModel.shouldSaveMoney.floatValue;
        if (fmodf(money, 1) != 0) { // 有小数
            cell.firstStringLabel.text = [NSString stringWithFormat:@"%.1f",money];
        } else {
            cell.firstStringLabel.text = dataModel.shouldSaveMoney.stringValue;
        }

        if (dataModel.actualSaveTime == 0) {
            cell.backgroundColor = [UIColor whiteColor];
        } else {
            cell.backgroundColor = [APP_COLOR_MAIN colorWithAlphaComponent:0.5];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger actualIndex = [self actualIndexWithIndexPath:indexPath.row];
    if (actualIndex != MAX_UNDEFINE_NUM &&
        actualIndex != MAX_UNDEFINE_END) {
        MGReceiptModel *dataModel = [self.dataModel.receiptModels objectAtIndex:actualIndex];
        if (self.handleReceiptBlock) {
            self.handleReceiptBlock(dataModel);
        }
    }
}

- (NSInteger)actualIndexWithIndexPath:(NSInteger)index
{
    NSInteger count = 3;
    NSInteger offset = 3; // 空白的格数
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 6;
    if (index < count) {
        return index - offset;
    }
    count += 7;
    offset += 7;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 6;
    if (index < count) {
        return index - offset;
    }
    count += 5;
    offset += 5;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 8;
    if (index < count) {
        return index - offset;
    }
    count += 5;
    offset += 5;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 8;
    if (index < count) {
        return index - offset;
    }
    count += 3;
    offset += 3;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 10;
    if (index < count) {
        return index - offset;
    }
    count += 3;
    offset += 3;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 10;
    if (index < count) {
        return index - offset;
    }
    count += 1;
    offset += 1;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 12;
    if (index < count) {
        return index - offset;
    }
    count += 1;
    offset += 1;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 12 + 6 * 25;
    if (index < count) {
        return index - offset;
    }
    count += 1;
    offset += 1;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 23;
    if (index < count) {
        return index - offset;
    }
    count += 3;
    offset += 3;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 21;
    if (index < count) {
        return index - offset;
    }
    count += 5;
    offset += 5;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 19;
    if (index < count) {
        return index - offset;
    }
    count += 7;
    offset += 7;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 17;
    if (index < count) {
        return index - offset;
    }
    count += 9;
    offset += 9;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 15;
    if (index < count) {
        return index - offset;
    }
    count += 11;
    offset += 11;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 13;
    if (index < count) {
        return index - offset;
    }
    count += 13;
    offset += 13;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 11;
    if (index < count) {
        return index - offset;
    }
    count += 15;
    offset += 15;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 9;
    if (index < count) {
        return index - offset;
    }
    count += 17;
    offset += 17;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 7;
    if (index < count) {
        return index - offset;
    }
    count += 19;
    offset += 19;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 5;
    if (index < count) {
        return index - offset;
    }
    count += 21;
    offset += 21;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 3;
    if (index < count) {
        return index - offset;
    }
    count += 23;
    offset += 23;
    if (index < count) {
        return MAX_UNDEFINE_NUM;
    }
    count += 1;
    if (index < count) {
        return MAX_UNDEFINE_END;
    }
    return MAX_UNDEFINE_NUM;
}


@end
