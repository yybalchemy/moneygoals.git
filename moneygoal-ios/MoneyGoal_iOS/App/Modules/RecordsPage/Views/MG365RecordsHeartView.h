//
//  MG365RecordsHeartView.h
//  365money-iOS
//
//  Created by alchemy on 2021/10/16.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MG365RecordsHeartView : UIView
// 左右的间距
@property (nonatomic) CGFloat horiPadding;
// 单个单元格的长度
@property (nonatomic) CGFloat gridSize;
// 线段的颜色
@property (nonatomic, strong) UIColor *strokeColor;


@end

NS_ASSUME_NONNULL_END
