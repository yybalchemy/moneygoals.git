//
//  MG365RecordsHeartView.m
//  365money-iOS
//
//  Created by alchemy on 2021/10/16.
//  Copyright © 2021 Cosmic Co,.Ltd. All rights reserved.
//

#import "MG365RecordsHeartView.h"

@implementation MG365RecordsHeartView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _horiPadding = 10;
    _gridSize = (kScreenWidth - _horiPadding * 2) / 25;
    
    _strokeColor = [UIColor blackColor];
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGFloat lineWidth = 0.4;
    CGFloat halfLineWidth = lineWidth / 2;
    
    // 横向的线段
    CGContextRef cxt = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(cxt, _strokeColor.CGColor);
    CGContextSetLineWidth(cxt, lineWidth);
    
    // 0
    CGContextMoveToPoint(cxt, _horiPadding + 3 * _gridSize, halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 9 * _gridSize, halfLineWidth);
    CGContextMoveToPoint(cxt, _horiPadding + 16 * _gridSize, halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 22 * _gridSize, halfLineWidth);
    
    // 1
    CGContextMoveToPoint(cxt, _horiPadding + 2 * _gridSize, _gridSize + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 10 * _gridSize, _gridSize + halfLineWidth);
    CGContextMoveToPoint(cxt, _horiPadding + 15 * _gridSize, _gridSize + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 23 * _gridSize, _gridSize + halfLineWidth);
    
    // 2
    CGContextMoveToPoint(cxt, _horiPadding + 1 * _gridSize, _gridSize * 2 + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 11 * _gridSize, _gridSize * 2 + halfLineWidth);
    CGContextMoveToPoint(cxt, _horiPadding + 14 * _gridSize, _gridSize * 2 + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 24 * _gridSize, _gridSize * 2 + halfLineWidth);
    
    // 3
    CGContextMoveToPoint(cxt, _horiPadding, _gridSize * 3 + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 12 * _gridSize, _gridSize * 3 + halfLineWidth);
    CGContextMoveToPoint(cxt, _horiPadding + 13 * _gridSize, _gridSize * 3 + halfLineWidth);
    CGContextAddLineToPoint(cxt, _horiPadding + 25 * _gridSize, _gridSize * 3 + halfLineWidth);
    
    // 4 - 11
    for (NSInteger i = 0; i < 7; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding, _gridSize * (4 + i) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + 25 * _gridSize, _gridSize * (4 + i) + halfLineWidth);
    }
    
    // 接下来，每级减少2个
    for (NSInteger i = 0; i < 12; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (1 + i) * _gridSize, _gridSize * (11 + i) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (25 - i - 1) * _gridSize, _gridSize * (11 + i) + halfLineWidth);
    }
    
    // 纵向的线段
    for (NSInteger i = 0; i < 3; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + i * _gridSize, _gridSize * (3 - i) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + i * _gridSize, _gridSize * (10 + i) + halfLineWidth);
    }
    
    for (NSInteger i = 0; i < 7; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (3 + i) * _gridSize, halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (3 + i) * _gridSize, _gridSize * (13 + i) + halfLineWidth);
    }
    
    for (NSInteger i = 0; i < 3; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (10 + i) * _gridSize, _gridSize * (i + 1) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (10 + i) * _gridSize, _gridSize * (20 + i) + halfLineWidth);
    }
    
    for (NSInteger i = 0; i < 4; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (13 + i) * _gridSize, _gridSize * (3 - i) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (13 + i) * _gridSize, _gridSize * (22 - i) + halfLineWidth);
    }
    
    for (NSInteger i = 0; i < 6; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (17 + i) * _gridSize, halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (17 + i) * _gridSize, _gridSize * (18 - i) + halfLineWidth);
    }
    
    for (NSInteger i = 0; i < 3; i ++) {
        CGContextMoveToPoint(cxt, _horiPadding + (23 + i) * _gridSize, _gridSize * (i + 1) + halfLineWidth);
        CGContextAddLineToPoint(cxt, _horiPadding + (23 + i) * _gridSize, _gridSize * (12 - i) + halfLineWidth);
    }
    
    CGContextStrokePath(cxt);
}


@end
