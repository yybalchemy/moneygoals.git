//
//  MGGradientView.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/22.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGGradientView.h"

@implementation MGGradientView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.gradientLayer = [CAGradientLayer layer];
    [self.layer addSublayer:self.gradientLayer];
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    _gradientLayer.frame = self.bounds;
    _gradientLayer.startPoint = CGPointMake(0.5f, 0);
    _gradientLayer.endPoint = CGPointMake(0.5f, 1);
}

@end
