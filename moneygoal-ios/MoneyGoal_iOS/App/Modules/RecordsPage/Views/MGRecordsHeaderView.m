//
//  MGRecordsHeaderView.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/20.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsHeaderView.h"
#import "MGRecordsSwitchView.h"

#import "MGRecordsHeaderCell.h"

#import <YYBBaseViews/YYBShadowView.h>

#define RecordsItemHeight 60

@interface MGRecordsHeaderView ()
@property (nonatomic, strong) YYBShadowView *shadowView;
@property (nonatomic, strong) MGRecordsSwitchView *categorySwitchView, *accountSwitchView;
@property (nonatomic, strong) UILabel *incomeMoneyLabel, *restMoneyLabel;

@end

@implementation MGRecordsHeaderView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = APP_COLOR_MAIN_LIGHT;
    
    @weakify(self);
    self.accountSwitchView = [MGRecordsSwitchView createViewAtSuperView:self constraintBlock:nil configureBlock:nil];
    self.accountSwitchView.tapedActionBlock = ^{
        @strongify(self);
        if (self.accountTapedBlock) {
            self.accountTapedBlock();
        }
    };
    
    self.categorySwitchView = [MGRecordsSwitchView createViewAtSuperView:self constraintBlock:nil configureBlock:nil];
    self.categorySwitchView.tapedActionBlock = ^{
        @strongify(self);
        if (self.categoryTapedBlock) {
            self.categoryTapedBlock();
        }
    };
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.top.equalTo(self).offset(80);
    } configureBlock:^(UILabel *view) {
        view.text = @"本月支出";
    }];
    
    self.moneyStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(5);
    } configureBlock:nil];
    
    self.incomeMoneyLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.bottom.equalTo(self).offset(-20);
    } configureBlock:nil];
    
    self.firstLineView = [UILabel createViewAtSuperView:self backgroundColor:APP_COLOR_MAIN constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.incomeMoneyLabel.mas_right).offset(10);
        make.size.mas_equalTo(CGSizeMake(1, 12));
        make.centerY.equalTo(self.incomeMoneyLabel);
    } configureBlock:nil];
    
    self.restMoneyLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstLineView.mas_right).offset(10);
        make.bottom.equalTo(self.incomeMoneyLabel);
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithRecordModel:(MGRecordModel *)recordModel accountUniqueId:(NSUInteger)accountUniqueId categoryUniqueId:(NSUInteger)categoryUniqueId {
    self.recordModel = recordModel;
    self.accountUniqueId = accountUniqueId;
    self.categoryUniqueId = categoryUniqueId;
    
    [self.gatherView reloadData];
    
    NSString *accountStr = @"全部账户";
    if (accountUniqueId != 0) {
        MGAccountModel *accountModel = [MGAccountAccessor.shared getAccountModelWithUniqueId:accountUniqueId];
        if (accountModel) {
            accountStr = accountModel.name;
        }
    }
    
    [self.accountSwitchView configViewWithTitleValue:accountStr];
    CGFloat accountWidth = [accountStr sizeWithFont:FONT_REGULAR(12) lineSpacing:0 size:CGSIZE_MAX].width;
    [self.accountSwitchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(20);
        make.size.mas_equalTo(CGSizeMake(accountWidth + 32, 28));
    }];
    
    NSString *categoryStr = @"全部分类";
    if (categoryUniqueId != 0) {
        MGCategoryModel *categoryModel = [MGCategoryAccessor.shared getCategoryModelWithUniqueId:categoryUniqueId];
        if (categoryModel) {
            categoryStr = categoryModel.name;
        }
    }
    
    [self.categorySwitchView configViewWithTitleValue:categoryStr];
    CGFloat categoryWidth = [categoryStr sizeWithFont:FONT_REGULAR(12) lineSpacing:0 size:CGSIZE_MAX].width;
    [self.categorySwitchView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.accountSwitchView.mas_right).offset(10);
        make.top.equalTo(self.accountSwitchView);
        make.size.mas_equalTo(CGSizeMake(categoryWidth + 32, 28));
    }];
    
    // 金额计算
    NSDecimalNumber *monthlyPaidMoney = [self.recordModel getMonthlyMoneyWithDate:[NSDate date] isIncome:NO accountUniqueId:self.accountUniqueId categoryUniqueId:self.categoryUniqueId];
    NSDecimalNumber *monthlyIncomeMoney = [self.recordModel getMonthlyMoneyWithDate:[NSDate date] isIncome:YES accountUniqueId:self.accountUniqueId categoryUniqueId:self.categoryUniqueId];

    NSString *incomeStr = [NSString stringWithFormat:@"本月收入 %@", [monthlyIncomeMoney.stringValue formatMoneyString]];
    NSMutableAttributedString *incomeAttr = [[NSMutableAttributedString alloc] initWithString:incomeStr];
    [incomeAttr addAttribute:NSForegroundColorAttributeName value:TEXT_COLOR_FIRST range:NSMakeRange(4, incomeStr.length - 4)];
    self.incomeMoneyLabel.attributedText = incomeAttr;
    
    NSDecimalNumber *remainMoney = [monthlyIncomeMoney decimalNumberBySubtracting:monthlyPaidMoney];
    NSString *remainStr = [NSString stringWithFormat:@"本月盈余 %@", [remainMoney.stringValue formatMoneyString]];
    NSMutableAttributedString *remainAttr = [[NSMutableAttributedString alloc] initWithString:remainStr];
    [remainAttr addAttribute:NSForegroundColorAttributeName value:TEXT_COLOR_FIRST range:NSMakeRange(4, remainStr.length - 4)];
    self.restMoneyLabel.attributedText = remainAttr;
    
    NSString *paidMoneyStr = [NSString stringWithFormat:@"%.2f", monthlyPaidMoney.floatValue];
    // 整数
    NSString *paidMoneyTotalStr = [paidMoneyStr componentsSeparatedByString:@"."].firstObject;
    NSMutableAttributedString *paidAttr = [[NSMutableAttributedString alloc] initWithString:paidMoneyStr];
    [paidAttr addAttribute:NSFontAttributeName value:FONT_MEDIUM(28) range:NSMakeRange(0, paidMoneyTotalStr.length)];
    self.moneyStringLabel.attributedText = paidAttr;
}

@end
