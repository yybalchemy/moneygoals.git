//
//  MGRecordsNavigationView.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/5/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsNavigationView : YYBBaseView

@property (nonatomic, copy) YYBTapedActionBlock recordTapedBlock;
@property (nonatomic, copy) YYBTapedActionBlock dateTapedBlock;

@end

NS_ASSUME_NONNULL_END
