//
//  MGRecordsNavigationView.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/5/25.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsNavigationView.h"

@implementation MGRecordsNavigationView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor whiteColor];
    
    // 选择账本
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_MEDIUM(16) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.bottom.equalTo(self).offset(-7);
    } configureBlock:nil];
    
    self.firstIconView = [UIImageView createAtSuperView:self iconName:@"account_down" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel.mas_right).offset(3);
        make.centerY.equalTo(self.firstStringLabel);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.right.equalTo(self.firstIconView);
        make.top.bottom.equalTo(self.firstStringLabel);
    } configureBlock:^(UIButton *view) {
        
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.recordTapedBlock) {
            self.recordTapedBlock();
        }
    }];
    
    self.secondActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.equalTo(self.firstStringLabel);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"navibar_date"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.dateTapedBlock) {
            self.dateTapedBlock();
        }
    }];
    
    return self;
}

- (void)configViewWithDataModel:(MGRecordModel *)dataModel {
    if (dataModel) {
        self.firstStringLabel.text = dataModel.name;
        self.firstStringLabel.hidden = NO;
        self.firstIconView.hidden = NO;
        self.wrapperView.hidden = NO;
        self.firstActionButton.hidden = NO;
        
        if (dataModel.recordType == MGRecordType365Money) {
            self.secondActionButton.hidden = YES;
        } else {
            self.secondActionButton.hidden = NO;
        }
    } else {
        self.firstStringLabel.hidden = YES;
        self.firstIconView.hidden = YES;
        self.wrapperView.hidden = YES;
        self.firstActionButton.hidden = YES;
        self.secondActionButton.hidden = YES;
    }
}

@end
