//
//  MGRecordsSelectLayout.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/2/4.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGRecordsSelectLayout : UICollectionViewFlowLayout

@property (nonatomic, strong) NSMutableArray *attributes;
@property (nonatomic, strong) NSMutableArray *dataModels;

@property (nonatomic, copy) YYBTapedActionBlock updateBlock;

@end

NS_ASSUME_NONNULL_END
