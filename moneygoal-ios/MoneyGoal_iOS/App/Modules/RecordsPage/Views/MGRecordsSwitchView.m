//
//  MGRecordsSwitchView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/7/24.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsSwitchView.h"

@implementation MGRecordsSwitchView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.wrapperView = [UIView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:^(UIView *view) {
        view.backgroundColor = [UIColor whiteColor];
        [view cornerRadius:14];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.wrapperView fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.centerY.equalTo(self);
    } configureBlock:nil];
    
    self.firstIconView = [UIImageView createAtSuperView:self.wrapperView iconName:@"switch_down" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel.mas_right);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self.wrapperView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.wrapperView);
    } configureBlock:nil tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue {
    self.firstStringLabel.text = titleValue;
}

@end
