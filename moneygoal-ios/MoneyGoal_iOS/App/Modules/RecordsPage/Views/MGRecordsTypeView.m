//
//  MGRecordsTypeView.m
//  MoneyRecord_iOS
//
//  Created by yyb on 2022/7/23.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGRecordsTypeView.h"
#import "MGRecordsTypeCell.h"

@implementation MGRecordsTypeView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).offset(20);
    } configureBlock:^(UILabel *view) {
        view.text = @"选择账本类型";
    }];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self.firstStringLabel);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"ico_receipt_dismiss"] forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.closeTapedBlock) {
            self.closeTapedBlock();
        }
    }];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 15, 0, 15);
    self.gatherView = [UICollectionView createAtSuperView:self delegeteTarget:self edgeInsets:insets scrollDirection:UICollectionViewScrollDirectionVertical constraintBlock:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(60);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self);
    } dequeueCellIdentifiers:@[@"MGRecordsTypeCell"] configureBlock:^(UICollectionView *view, UICollectionViewFlowLayout *layout) {
        CGFloat itemWidth = (kScreenWidth - 40) / 2;
        CGFloat itemHeight = 80;
        
        layout.itemSize = CGSizeMake(itemWidth, itemHeight);
        layout.minimumLineSpacing = 15;
        layout.minimumInteritemSpacing = 10;
    }];
    
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MGRecordsTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MGRecordsTypeCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        [cell configViewWithTitleValue:@"普通账本" detailValue:@"日常消费、收支" iconName:@"recordType_01"];
    } else if (indexPath.row == 1) {
        [cell configViewWithTitleValue:@"365存钱账本" detailValue:@"365存钱法电子表格" iconName:@"recordType_02"];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectRecordTypeBlock) {
        self.selectRecordTypeBlock(indexPath.row);
    }
}

@end
