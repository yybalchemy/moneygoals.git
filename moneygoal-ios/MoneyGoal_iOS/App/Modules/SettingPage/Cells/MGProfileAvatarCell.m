//
//  MGProfileAvatarCell.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/12.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGProfileAvatarCell.h"

@implementation MGProfileAvatarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self.containerView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.text = @"头像";
    }];
    
    self.firstIconView = [UIImageView createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-35);
        make.centerY.equalTo(self.containerView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    } configureBlock:^(UIImageView *view) {
        view.contentMode = UIViewContentModeScaleAspectFill;
        [view cornerRadius:20];
    }];
    
    self.secondIconView = [UIImageView createAtSuperView:self.containerView iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.containerView).offset(-10);
        make.centerY.equalTo(self.containerView);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    } configureBlock:nil];
    
    return self;
}

- (void)configViewWithDataModel:(NSString *)dataModel {
    [self.firstIconView sd_setImageWithURL:[NSURL URLWithString:dataModel] placeholderImage:[UIImage imageNamed:@"logo_mini"]];
}

@end
