//
//  MGProfileSwitchCell.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/14.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGProfileSwitchCell.h"

@interface MGProfileSwitchCell ()
@property (nonatomic, strong) UISwitch *switchView;

@end

@implementation MGProfileSwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.containerView).offset(15);
        make.centerY.equalTo(self.containerView);
    } configureBlock:^(UILabel *view) {
        view.font = FONT_REGULAR(15);
        view.textColor = TEXT_COLOR_FIRST;
    }];
    
    self.switchView = [UISwitch createViewAtSuperView:self.containerView constraintBlock:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.containerView);
        make.right.equalTo(self.containerView).offset(-15);
    } configureBlock:^(UISwitch *view) {
        
    }];
    
    @weakify(self);
    [[self.switchView rac_signalForControlEvents:UIControlEventValueChanged] subscribeNext:^(__kindof UISwitch * _Nullable x) {
        @strongify(self);
        if (self.changeValueBlock) {
            self.changeValueBlock(x.on);
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    self.firstStringLabel.text = titleValue;
    self.switchView.on = status;
}

@end
