//
//  DBSettingCell.m
//  Docorate-iOS
//
//  Created by alchemy on 2021/4/23.
//  Copyright © 2021 Moneyease Co., LTD. All rights reserved.
//

#import "MGSettingCell.h"

@implementation MGSettingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createAtSuperView:self.contentView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.contentView);
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:4];
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(15);
        make.centerY.equalTo(self.firstIconView);
    } configureBlock:nil];
    
    self.secondIconView = [UIImageView createAtSuperView:self.contentView iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-20);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    self.secondStringLabel = [UILabel createAtSuperView:self.contentView fontValue:FONT_REGULAR(15) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.secondIconView.mas_left).offset(-5);
        make.centerY.equalTo(self.secondIconView);
    } configureBlock:^(UILabel *view) {
        view.text = @"登录后开启";
    }];
    
    [UIView createBottomSeparateViewAtSuperView:self.contentView color:APP_COLOR_SAPERATE height:1 edgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.secondStringLabel.hidden = YES;
}

- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon hidesWhenLogin:(BOOL)hidesWhenLogin
{
    self.firstIconView.image = [UIImage imageNamed:icon];
    self.firstStringLabel.text = titleValue;
    self.secondStringLabel.hidden = hidesWhenLogin;
}

@end
