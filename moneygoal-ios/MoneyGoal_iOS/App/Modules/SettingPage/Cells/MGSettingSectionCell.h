//
//  MGSettingSectionCell.h
//  Docorate-iOS
//
//  Created by alchemy on 2021/4/23.
//  Copyright © 2021 Moneyease Co., LTD. All rights reserved.
//

#import "YYBTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingSectionCell : YYBTableViewCell

@end

NS_ASSUME_NONNULL_END
