//
//  MGSettingSectionCell.m
//  Docorate-iOS
//
//  Created by alchemy on 2021/4/23.
//  Copyright © 2021 Moneyease Co., LTD. All rights reserved.
//

#import "MGSettingSectionCell.h"

@implementation MGSettingSectionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    [UIView createTopSeparateViewAtSuperView:self.contentView color:APP_COLOR_BACKGROUND height:10 edgeInsets:UIEdgeInsetsZero];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    self.firstStringLabel.text = titleValue;
}

@end
