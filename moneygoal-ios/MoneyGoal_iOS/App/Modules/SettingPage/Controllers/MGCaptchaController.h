//
//  MGCaptchaController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/2.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCaptchaController : MGBaseController

@property (nonatomic, copy) NSString *phoneNum;

@end

NS_ASSUME_NONNULL_END
