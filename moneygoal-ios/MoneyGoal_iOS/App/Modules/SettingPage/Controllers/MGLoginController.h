//
//  MGLoginController.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/5.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGLoginController : MGBaseController

@end

NS_ASSUME_NONNULL_END
