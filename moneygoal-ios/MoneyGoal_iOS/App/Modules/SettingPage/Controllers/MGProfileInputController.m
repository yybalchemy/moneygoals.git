//
//  MGProfileInputController.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/12.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGProfileInputController.h"

#import "MGProfileInputViewCell.h"

@interface MGProfileInputController ()
@property (nonatomic, copy) NSString *text;

@end

@implementation MGProfileInputController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.text = [MGUserModel user].nick_name;
}

- (void)configCustomNavigationBar:(YYBNavigationBar *)navigationBar {
    [super configCustomNavigationBar:navigationBar];
    
    @weakify(self);
    YYBNavigationBarButton *completeButton = [YYBNavigationBarButton buttonWithConfigureBlock:^(YYBNavigationBarButton *container, UIButton *view) {
        container.contentSize = CGSizeMake(55, 28);
        container.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        
        [view cornerRadius:14];
        [view setTitle:@"保存" forState:0];
        [view setTitleColor:APP_COLOR_MAIN forState:0];
        view.backgroundColor = [APP_COLOR_MAIN colorWithAlphaComponent:0.1];
        view.titleLabel.font = FONT_REGULAR(14);
    } tapedActionBlock:^(YYBNavigationBarContainer *view) {
        @strongify(self);
        [self _handleComplete];
    }];
    
    navigationBar.rightBarContainers = @[completeButton];
}

- (void)_handleComplete {
    if (![self.text isExist]) {
        [YYBAlertView showAlertViewWithStatusString:@"请输入昵称"];
    } else {
        @weakify(self);
        self.refreshAlertView = [YYBAlertView showQueryStringAlertViewWithString:@"更新中" inView:self.view isResizeContainer:NO];
        [MGReqManager.shared putUserNickName:self.text successBlock:^(id data, NSDictionary *params) {
            @strongify(self);
            [self.refreshAlertView closeAlertView];
            [MGUserModel updateUserNickName:self.text];
            [self.navigationController popViewControllerAnimated:YES];
        } errorBlock:^(NSError *error, NSDictionary *params) {
            @strongify(self);
            [self.refreshAlertView closeAlertView];
            [YYBAlertView showAlertViewWithError:error];
        }];
    }
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGProfileInputViewCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGProfileInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGProfileInputViewCell"];
    
    @weakify(self);
    cell.stringChangedBlock = ^(NSString *text) {
        @strongify(self);
        self.text = text;
    };
    
    [cell configViewWithCornerType:MGCornerTypeAll];
    [cell configViewWithTitleValue:@"昵称" detailValue:[MGUserModel user].nick_name];
    return cell;
}

- (NSString *)customPageTitle {
    return @"修改昵称";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 15;
    return insets;
}

@end
