//
//  MGSettingConfigController.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/9/1.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGSettingConfigController.h"

#import "MGSettingConfigSwitchCell.h"
#import "MGProfileInputCell.h"

@interface MGSettingConfigController ()

@end

@implementation MGSettingConfigController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGSettingConfigSwitchCell", @"MGProfileInputCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 80;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        MGSettingConfigSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGSettingConfigSwitchCell"];
        cell.changeValueBlock = ^(BOOL status) {
            [MGConfigManager.shared configWithBlock:^(MGConfigModel * _Nonnull dataModel) {
                dataModel.enableTimeMemory = !dataModel.enableTimeMemory;
            }];
        };
        
        [cell configViewWithCornerType:MGCornerTypeAll];
        [cell configViewWithTitleValue:@"时间记忆" detailValue:@"自动记录上一次添加的账单时间，下次再添加时，使用该时间自动填充新的账单" status:MGConfigManager.shared.dataModel.enableTimeMemory];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        
    }
}

- (NSString *)customPageTitle {
    return @"应用设置";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 15;
    return insets;
}

@end
