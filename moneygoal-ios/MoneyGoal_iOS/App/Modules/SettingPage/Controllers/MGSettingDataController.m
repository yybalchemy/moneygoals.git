//
//  MGSettingDataController.m
//  Decorate-iOS
//
//  Created by yyb on 2022/8/14.
//  Copyright © 2022 Moneyease Co., LTD. All rights reserved.
//

#import "MGSettingDataController.h"

#import "MGProfileSwitchCell.h"
#import "MGProfileInputCell.h"

@interface MGSettingDataController ()

@end

@implementation MGSettingDataController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)syncRecordModels {
    @weakify(self);
    [YYBAlertView showAlertViewWithTitle:nil detail:@"同步数据时，所有本地的数据会先被清除。" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"确定" secondActionTapedBlock:^{
        @strongify(self);
        self.refreshAlertView = [YYBAlertView showQueryStringAlertViewWithString:@"正在同步" inView:self.view isResizeContainer:NO];
        // 删除本地数据
        [MGCommonAccessor.shared clearAllLocalData:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:MGNOTI_REFRESH_DATA object:nil];
            // 加载线上数据，并保存在本地
            [MGCommonAccessor.shared getAndSaveAllOssDataWithCompleteBlock:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:MGNOTI_REFRESH_DATA object:nil];
                [MGAlertView closeAlertView:self.refreshAlertView showString:@"同步成功"];
            } errorBlock:^(NSError *error) {
                [MGAlertView closeAlertView:self.refreshAlertView showError:error];
            }];
        } errorBlock:^(NSError *error) {
            [MGAlertView closeAlertView:self.refreshAlertView showError:error];
        }];
    }];
}

- (void)uploadRecordModels {
    @weakify(self);
    [YYBAlertView showAlertViewWithTitle:nil detail:@"上传所有的本地数据到云端吗?" firstActionTitle:@"取消" firstActionTapedBlock:nil secondActionTitle:@"确定" secondActionTapedBlock:^{
        @strongify(self);
        self.refreshAlertView = [YYBAlertView showQueryStringAlertViewWithString:@"正在同步" inView:self.view isResizeContainer:NO];
        [MGCommonAccessor.shared saveAllDataToOssWithCompleteBlock:^{
            [MGAlertView closeAlertView:self.refreshAlertView showString:@"上传成功"];
        } errorBlock:^(NSError *error) {
            [MGAlertView closeAlertView:self.refreshAlertView showError:error];
        }];
    }];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers {
    return @[@"MGProfileSwitchCell", @"MGProfileInputCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        MGProfileSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGProfileSwitchCell"];
        cell.changeValueBlock = ^(BOOL status) {
            [MGConfigManager.shared configWithBlock:^(MGConfigModel * _Nonnull dataModel) {
                dataModel.enableOSS = status;
            }];
        };
        
        [cell configViewWithCornerType:MGCornerTypeTop];
        [cell configViewWithTitleValue:@"自动上传" status:MGConfigManager.shared.dataModel.enableOSS];
        return cell;
    } else if (indexPath.row == 1) {
        MGProfileInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGProfileInputCell"];
        
        [cell configViewWithCornerType:MGCornerTypeCenter];
        [cell configViewWithTitleValue:@"上传数据" detailValue:nil];
        return cell;
    } else if (indexPath.row == 2) {
        MGProfileInputCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MGProfileInputCell"];
        
        [cell configViewWithCornerType:MGCornerTypeBottom];
        [cell configViewWithTitleValue:@"同步数据" detailValue:nil];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [self uploadRecordModels];
    } else if (indexPath.row == 2) {
        [self syncRecordModels];
    }
}

- (NSString *)customPageTitle {
    return @"数据";
}

- (UIEdgeInsets)handleEdgeInsets {
    UIEdgeInsets insets = [super handleEdgeInsets];
    insets.top += 15;
    return insets;
}

@end
