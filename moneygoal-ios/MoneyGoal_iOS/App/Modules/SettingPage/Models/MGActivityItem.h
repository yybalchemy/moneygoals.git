//
//  MGActivityItem.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/3.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGActivityItem : UIActivityItemProvider

+ (instancetype)activityItemSourceUrlWithIcon:(UIImage *)icon;
@property (nonatomic, strong) UIImage *icon;

@end

NS_ASSUME_NONNULL_END
