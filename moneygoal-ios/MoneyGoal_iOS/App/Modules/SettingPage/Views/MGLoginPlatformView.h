//
//  MGLoginPlatformView.h
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/2.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "YYBBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGLoginPlatformView : YYBBaseView

@end

NS_ASSUME_NONNULL_END
