//
//  MGLoginPlatformView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/2/2.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGLoginPlatformView.h"

@implementation MGLoginPlatformView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.firstIconView = [UIImageView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.mas_equalTo(50);
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:25];
        view.backgroundColor = YYBCOLOR(0xF7F8FA);
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(12) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.firstIconView.mas_bottom).offset(5);
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:nil tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    return self;
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue
{
    self.firstIconView.image = [UIImage imageNamed:titleValue];
    self.firstStringLabel.text = detailValue;
}

@end
