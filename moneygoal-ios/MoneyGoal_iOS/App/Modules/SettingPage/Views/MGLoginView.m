//
//  MGLoginView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2023/1/31.
//  Copyright © 2023 Moneyease CO,. LTD. All rights reserved.
//

#import "MGLoginView.h"
#import <YYText/YYText.h>
#import "MGLoginPlatformView.h"

@interface MGLoginView ()
@property (nonatomic, strong) YYLabel *permisLabel;
@property (nonatomic, strong) MGLoginPlatformView *appleLoginView, *wxLoginView;

@end

@implementation MGLoginView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_MEDIUM(25) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.top.equalTo(self).offset(30);
    } configureBlock:^(UILabel *view) {
        view.text = @"验证码登录";
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstStringLabel.mas_bottom).offset(10);
    } configureBlock:^(UILabel *view) {
        view.text = @"手机号如未注册，首次登录就会自动注册";
    }];
    
    self.inputStringView = [UITextField createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(30);
        make.right.equalTo(self).offset(-30);
        make.top.equalTo(self).offset(180);
        make.height.mas_equalTo(50);
    } configureBlock:^(UITextField *view) {
        view.placeholder = @"输入手机号";
        view.font = FONT_REGULAR(15);
        view.keyboardType = UIKeyboardTypeNumberPad;
    }];
    
    self.firstLineView = [UIView createViewAtSuperView:self backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.inputStringView);
        make.height.mas_equalTo(1);
        make.top.equalTo(self.inputStringView.mas_bottom);
    } configureBlock:nil];
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self cornerRadius:25 fontValue:FONT_MEDIUM(16) title:@"获取验证码" titleColor:APP_COLOR_MAIN constraintBlock:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.inputStringView);
        make.top.equalTo(self.inputStringView.mas_bottom).offset(60);
        make.height.mas_equalTo(50);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:0];
        [view setBackgroundImage:APP_COLOR_MAIN_LIGHT.colorToUIImage forState:UIControlStateHighlighted];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.getCaptchaBlock) {
            if (self.secondActionButton.selected) {
                if (self.inputStringView.text.length == 11) {
                    self.getCaptchaBlock(self.inputStringView.text);
                } else {
                    [YYBAlertView showAlertViewWithStatusString:@"请输入正确的手机号"];
                }
            } else {
                [YYBAlertView showAlertViewWithStatusString:@"请阅读并勾选《隐私协政策》"];
            }
        }
    }];
    
    self.permisLabel = [YYLabel createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self).offset(17);
        make.top.equalTo(self.firstActionButton.mas_bottom).offset(20);
    } configureBlock:^(YYLabel *view) {
        NSString *text = @"阅读并同意《隐私政策》";
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:text];
        attr.yy_font = FONT_REGULAR(14);
        attr.yy_color = TEXT_COLOR_SECOND;
        
//        NSRange protocolRange = [text rangeOfString:@"《用户协议》"];
//        [attr yy_setTextHighlightRange:protocolRange color:YYBCOLOR(0x6F78C3) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
//
//        }];
        
        NSRange pravicyRange = [text rangeOfString:@"《隐私政策》"];
        [attr yy_setTextHighlightRange:pravicyRange color:YYBCOLOR(0x6F78C3) backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            [NSObject openURL:@"https://www.moneyease.ltd/privacy-policy"];
        }];
        view.attributedText = attr;
    }];
    
    
    self.secondActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self.permisLabel.mas_left).offset(-3);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(self.permisLabel);
    } configureBlock:^(UIButton *view) {
        [view setBackgroundImage:[UIImage imageNamed:@"permiss_check"] forState:0];
        [view setBackgroundImage:[UIImage imageNamed:@"permiss_check"] forState:UIControlStateHighlighted];
        [view setBackgroundImage:[UIImage imageNamed:@"permiss_checked"] forState:UIControlStateSelected];
    } tapedBlock:^(UIButton *view) {
        view.selected = !view.selected;
    }];
    
    self.secondLineView = [UIView createViewAtSuperView:self backgroundColor:APP_COLOR_SAPERATE constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(180, 1));
        make.bottom.equalTo(self).offset(-200);
    } configureBlock:nil];
    
    self.thirdStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(14) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.secondLineView);
        make.centerY.equalTo(self.secondLineView);
        make.width.mas_equalTo(100);
    } configureBlock:^(UILabel *view) {
        view.text = @"其他登录方式";
        view.backgroundColor = [UIColor whiteColor];
        view.textAlignment = NSTextAlignmentCenter;
    }];
    
    self.appleLoginView = [MGLoginPlatformView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_centerX).offset(-20);
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.thirdStringLabel.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(50, 80));
    } configureBlock:^(MGLoginPlatformView *view) {
        [view configViewWithTitleValue:@"apple_login" detailValue:@"苹果登录"];
    }];
    
//    self.wxLoginView = [MGLoginPlatformView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.mas_centerX).offset(20);
//        make.size.top.equalTo(self.appleLoginView);
//    } configureBlock:^(MGLoginPlatformView *view) {
//        [view configViewWithTitleValue:@"wx_login" detailValue:@"微信登录"];
//    }];
    
    self.appleLoginView.tapedActionBlock = ^{
        @strongify(self);
        if (self.platformSelectBlock) {
            self.platformSelectBlock(0);
        }
    };
    
//    self.wxLoginView.tapedActionBlock = ^{
//        @strongify(self);
//        if (self.platformSelectBlock) {
//            self.platformSelectBlock(1);
//        }
//    };
    
    return self;
}

@end
