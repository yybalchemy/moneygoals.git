//
//  MGNotLoginView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGNotLoginView.h"

@implementation MGNotLoginView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor whiteColor];

    @weakify(self);
    self.firstIconView = [UIImageView createAtSuperView:self iconName:@"logo" constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    } configureBlock:^(UIImageView *view) {
        [view cornerRadius:30];
        view.backgroundColor = APP_COLOR_MAIN_LIGHT;
    }];
    
    self.firstStringLabel = [UILabel createAtSuperView:self fontValue:FONT_MEDIUM(18) textColor:TEXT_COLOR_FIRST constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstIconView.mas_right).offset(20);
        make.bottom.equalTo(self.firstIconView.mas_centerY).offset(-2);
    } configureBlock:^(UILabel *view) {
        view.text = @"登录/注册";
    }];
    
    self.secondStringLabel = [UILabel createAtSuperView:self fontValue:FONT_REGULAR(13) textColor:TEXT_COLOR_SECOND constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstStringLabel);
        make.top.equalTo(self.firstIconView.mas_centerY).offset(5);
    } configureBlock:^(UILabel *view) {
        view.text = @"多多记账，多多好生活";
    }];
    
    self.secondIconView = [UIImageView createAtSuperView:self iconName:@"record_next" constraintBlock:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-20);
        make.centerY.equalTo(self.firstIconView);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    } configureBlock:nil];
    
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:^(UIButton *view) {
        
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.doLoginBlock) {
            self.doLoginBlock();
        }
    }];

    return self;
}

@end
