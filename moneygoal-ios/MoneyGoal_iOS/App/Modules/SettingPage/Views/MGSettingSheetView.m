//
//  MGSettingSheetView.m
//  MoneyGoal_iOS
//
//  Created by yyb on 2022/8/31.
//  Copyright © 2022 Moneyease CO,. LTD. All rights reserved.
//

#import "MGSettingSheetView.h"

@implementation MGSettingSheetView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.firstActionButton = [UIButton createAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.equalTo(self).offset(50);
        make.width.mas_equalTo(220);
        make.centerX.equalTo(self);
    } configureBlock:^(UIButton *view) {
        view.titleLabel.font = FONT_REGULAR(16);
        [view setTitle:@"退出登录" forState:0];
        [view setTitleColor:APP_COLOR_MAIN forState:0];
        [view cornerRadius:25];
        [view setBackgroundImage:[UIColor whiteColor].colorToUIImage forState:0];
    } tapedBlock:^(UIButton *view) {
        @strongify(self);
        if (self.tapedActionBlock) {
            self.tapedActionBlock();
        }
    }];
    
    return self;
}

@end
