//
//  AppDelegate.h
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import <UIKit/UIKit.h>
//#import "WXApi.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

