//
//  AppDelegate.m
//  MoneyRecord_iOS
//
//  Created by alchemy on 2022/1/7.
//

#import "AppDelegate.h"
#import <Bugly/Bugly.h>
#import "JPUSHService.h"
#import <UserNotifications/UserNotifications.h>

#import "MGTabBarController.h"
#import "MGAlertManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Bugly startWithAppId:@"5cc50cfa77"];
//    [WXApi registerApp:@"" universalLink:@"https://moneyease/lotsmoney/app/"];
    
    JPUSHRegisterEntity *entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert | JPAuthorizationOptionBadge | JPAuthorizationOptionSound | JPAuthorizationOptionProvidesAppNotificationSettings;
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    [JPUSHService setupWithOption:launchOptions appKey:@"797727393e025ba877bedc6a"
                           channel:@"App Store"
                  apsForProduction:YES
             advertisingIdentifier:nil];
    
    // 配置
    [[MGFileManager shared] initWithCompleteBlock:^{
        [MGConfigManager.shared initialize];     
        [MGRecordAccessor.shared getLocalDataModels];
        [MGAccountAccessor.shared getLocalDataModels];
        [MGCategoryAccessor.shared getLocalDataModels];
    }];
    
    [MGAlertManager startNotification];
    
    // ==========
    MGTabBarController *vc = [[MGTabBarController alloc] init];
    _window = [[UIWindow alloc] init];
    _window.rootViewController = vc;
    _window.backgroundColor = [UIColor whiteColor];
    [_window makeKeyAndVisible];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [JPUSHService registerDeviceToken:deviceToken];
}

//- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
//    return [WXApi handleOpenURL:url delegate:self];
//}
//
//- (void)onReq:(BaseReq *)reqonReq {
//
//}
//
//- (void)onResp:(BaseResp *)resp {
//    SendAuthResp *respObj = (SendAuthResp *)resp;
//
//    YYBAlertView *refreshAlertView = [YYBAlertView showQueryStringAlertViewWithString:@"正在登录" inView:nil isResizeContainer:NO];
//    [MGReqManager.shared getWechatResultWithCode:respObj.code successBlock:^(id data, NSDictionary *params) {
//        [refreshAlertView closeAlertView];
//        [MGUserModel configUserInfo:data];
//    } errorBlock:^(NSError *error, NSDictionary *params) {
//        [refreshAlertView closeAlertView];
//        [YYBAlertView showAlertViewWithError:error];
//    }];
//}

@end
