//
//  YYBAlertViewActionModel.m
//  YYBBaseViews
//
//  Created by yyb on 2022/8/17.
//  Copyright © 2022 Univease Co., Ltd. All rights reserved.
//

#import "YYBAlertViewActionModel.h"

@implementation YYBAlertViewActionModel

- (UIColor *)getActionTypeColor {
    if (_actionType == YYBAlertViewActionTypeNormal) {
        return [UIColor colorWithHexValue:0x292C35];
    } else if (_actionType == YYBAlertViewActionTypeWarning) {
        return [UIColor colorWithHexValue:0xA70D18];
    }
    return [UIColor colorWithHexValue:0x292C35];
}

@end
