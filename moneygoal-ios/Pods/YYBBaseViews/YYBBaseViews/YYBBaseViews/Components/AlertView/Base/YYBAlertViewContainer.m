//
//  YYBAlertViewContainer.m
//  YYBAlertView
//
//  Created by alchemy on 2018/8/30.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBAlertViewContainer.h"
#import "UIView+Responder.h"

@implementation YYBAlertViewContainer {
    CGFloat _stretchTotalUsableSize;
    UITapGestureRecognizer *_taped;
}

- (instancetype)init
{
    return [self initWithFlexDirection:YYBAlertViewFlexDirectionVertical isUsingActionsContainer:YES];
}

- (YYBAlertViewContainer *)initWithFlexDirection:(YYBAlertViewFlexDirection)direction
{
    return [self initWithFlexDirection:direction isUsingActionsContainer:YES];
}

- (YYBAlertViewContainer *)initWithFlexDirection:(YYBAlertViewFlexDirection)direction isUsingActionsContainer:(BOOL)isUsingActionsContainer
{
    self = [super init];
    if (!self) return nil;
    
    _flexDirection = direction;
    
    _minimalWidth = 0;
    _maximalWidth = 300;
    _minimalHeight = 0;
    _maximalHeight = 300;
    
    _innerViews = [NSMutableArray new];
    _innerActions = [NSMutableArray new];
    
    _shadowView = [[UIView alloc] init];
    [self addSubview:_shadowView];
    
    _contentView = [[UIImageView alloc] init];
    _contentView.userInteractionEnabled = YES;
    [_shadowView addSubview:_contentView];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    [_contentView addSubview:_scrollView];
    
    _scrollViewScrollable = YES;
    _rectCorner = UIRectCornerAllCorners;
    
    if (isUsingActionsContainer)
    {
        _actionsContainer = [[YYBAlertViewContainer alloc] initWithFlexDirection:YYBAlertViewFlexDirectionVertical isUsingActionsContainer:NO];
        _actionsContainer.clipsToBounds = YES;
        [_contentView addSubview:_actionsContainer];
        [_actionsContainer removeContainerContentTapedBlock];
    }
    
    _taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewTapedOnAction)];
    [_contentView addGestureRecognizer:_taped];
    
    return self;
}

- (void)removeContainerContentTapedBlock
{
    [_contentView removeGestureRecognizer:_taped];
}

- (void)contentViewTapedOnAction
{
    UIView *responder = [self keyboardResponder];
    if (responder)
    {
        [responder resignFirstResponder];
    }
}

- (UIView *)keyboardResponder
{
    return [self.scrollView searchKeyboardResponder];
}

- (void)setMaximalWidth:(CGFloat)maximalWidth
{
    _maximalWidth = maximalWidth;
    if (_actionsContainer)
    {
        _actionsContainer.maximalWidth = maximalWidth;
    }
}

- (void)setMaximalHeight:(CGFloat)maximalHeight
{
    _maximalHeight = maximalHeight;
    if (_actionsContainer)
    {
        _actionsContainer.maximalHeight = maximalHeight;
    }
}

- (void)setMinimalWidth:(CGFloat)minimalWidth
{
    _minimalWidth = minimalWidth;
    if (_actionsContainer)
    {
        _actionsContainer.minimalWidth = minimalWidth;
    }
}

- (void)setMinimalHeight:(CGFloat)minimalHeight
{
    _minimalHeight = minimalHeight;
    if (_actionsContainer)
    {
        _actionsContainer.minimalHeight = minimalHeight;
    }
}

- (CGSize)contentSize
{
    if (CGSizeEqualToSize(CGSizeZero, _contentSize))
    {
        [self createContentViewsWithCalculationStatus:YES];
    }
    return _contentSize;
}

- (CGSize)contentSizeWithActions
{
    if (CGSizeEqualToSize(CGSizeZero, _contentSizeWithActions))
    {
        CGSize contentSize = _actionsContainer.contentSize;
        if (_innerActions.count > 0)
        {
            CGFloat width_action = 0, height_action = 0;
            if (_flexDirection == YYBAlertViewFlexDirectionVertical)
            {
                width_action = MAX(self.contentSize.width, contentSize.width);
                height_action = self.contentSize.height + contentSize.height;
            }
            else if (_flexDirection == YYBAlertViewFlexDirectionHorizonal)
            {
                width_action = self.contentSize.width + contentSize.width;
                height_action = MAX(self.contentSize.height, contentSize.height);
            }
            
            _contentSizeWithActions = CGSizeMake(width_action, height_action);
        }
        else
        {
            _contentSizeWithActions = self.contentSize;
        }
    }
    return _contentSizeWithActions;
}

- (void)createActionViews
{
    // 先计算一遍轮廓尺寸以确定子视图位置
    [self createContentViewsWithCalculationStatus:YES];
    [self createContentViewsWithCalculationStatus:NO];
}

- (void)createContentViewsWithCalculationStatus:(BOOL)isCalculateContentSize
{
    if (_maximalWidth < _minimalWidth)
    {
        _maximalWidth = _minimalWidth;
    }
    
    if (_minimalWidth > _maximalWidth)
    {
        _minimalWidth = _maximalWidth;
    }
    
    if (_maximalHeight < _minimalHeight)
    {
        _maximalHeight = _minimalHeight;
    }
    
    if (_minimalHeight > _maximalHeight)
    {
        _minimalHeight = _maximalHeight;
    }
    
    // 依次排列视图
    CGFloat height_max = 0, width_max = 0;
    if (isCalculateContentSize == YES)
    {
        _stretchTotalUsableSize = 0;
    }
    
    for (NSInteger idx = 0; idx < _innerViews.count; idx ++)
    {
        CGFloat x = 0,y = 0,width = 0,height = 0;
        UIEdgeInsets padding = UIEdgeInsetsZero, margin = UIEdgeInsetsZero;
        UIView *view = nil;
        NSObject *innerViewObject = [_innerViews objectAtIndex:idx];
        
        if ([innerViewObject isKindOfClass:[YYBAlertViewContainer class]])
        {
            YYBAlertViewContainer *container = (YYBAlertViewContainer *)innerViewObject;
            view = container;
            
            padding = container.padding;
            margin = container.margin;
            
            CGSize container_contentSize = container.contentSize;
            height = (container_contentSize.height == _maximalHeight) ? container_contentSize.height - margin.top - margin.bottom : container_contentSize.height;
            width = (container_contentSize.width == _maximalWidth) ? container_contentSize.width - margin.left - margin.right : container_contentSize.width;
        }
        else
        {
            YYBAlertViewAction *action = (YYBAlertViewAction *)innerViewObject;
            view = [action actionView];
            
            padding = action.padding;
            margin = action.margin;
            
            CGSize size = [action actionSizeWithContainerMaxWidth:_maximalWidth maxHeight:_maximalHeight];
            height = (size.height == _maximalHeight) ? size.height - margin.top - margin.bottom : size.height;
            width = (size.width == _maximalWidth) ? size.width - margin.left - margin.right : size.width;
            
            if (action.label)
            {
                if (width == 0)
                {
                    width = _maximalWidth - margin.left - margin.right;
                }
                
                if (_flexDirection == YYBAlertViewFlexDirectionVertical)
                {
                    CGSize label_size = [action labelSizeWithMaxWidth:width];
                    width = MIN(label_size.width, _maximalWidth);
                    height = label_size.height;
                }
                else if (_flexDirection == YYBAlertViewFlexDirectionHorizonal)
                {
                    CGSize label_size = [action labelSizeWithMaxWidth:action.size.width];
                    width = label_size.width;
                    height = MIN(label_size.height, _maximalHeight);
                }
            }
        }
        
        if (_flexDirection == YYBAlertViewFlexDirectionVertical)
        {
            y = height_max + padding.top + margin.top;
            if (_flexPosition == YYBAlertViewFlexPositionCenter)
            {
                x = (_contentSize.width - margin.left - margin.right - width) / 2 + padding.left - padding.right + margin.left;
            }
            else if (_flexPosition == YYBAlertViewFlexPositionEnd)
            {
                x = _contentSize.width - margin.left - margin.right - width - padding.right + padding.left - margin.right;
            }
            else if (_flexPosition == YYBAlertViewFlexPositionStart)
            {
                x = padding.left + margin.left - margin.right - padding.right;
            }
            else if (_flexPosition == YYBAlertViewFlexPositionStretch)
            {
                x = (_contentSize.width - width) / 2;
            }
            
            if (isCalculateContentSize == YES)
            {
                _stretchTotalUsableSize += height;
            }
            
            // 压缩尺寸
            if (_flexPosition == YYBAlertViewFlexPositionStretch && isCalculateContentSize == NO)
            {
                height = height / _stretchTotalUsableSize * (CGRectGetHeight(self.frame) - (_innerViews.count - 1) * _stretchValue);
            }
            
            width_max = MAX(width_max, width + margin.left + margin.right);
            height_max += height + padding.bottom + padding.top + margin.top + margin.bottom;
            
            if (_flexPosition == YYBAlertViewFlexPositionStretch && isCalculateContentSize == NO && idx != _innerViews.count - 1)
            {
                height_max += _stretchValue;
            }
        }
        else
        {
            x = width_max + padding.left + margin.left;
            if (_flexPosition == YYBAlertViewFlexPositionCenter || _flexPosition == YYBAlertViewFlexPositionStretch)
            {
                y = (_contentSize.height - height - margin.bottom - margin.top) / 2 + padding.top - padding.bottom + margin.top;
            }
            else if (_flexPosition == YYBAlertViewFlexPositionEnd)
            {
                y = _contentSize.height - margin.bottom - margin.top - height - padding.bottom + padding.top - margin.bottom;
            }
            else if (_flexPosition == YYBAlertViewFlexPositionStart)
            {
                y = padding.top - padding.bottom + margin.top - margin.bottom;
            }
            
            if (isCalculateContentSize == YES)
            {
                _stretchTotalUsableSize += width;
            }
            
            if (_flexPosition == YYBAlertViewFlexPositionStretch && isCalculateContentSize == NO)
            {
                CGFloat stretchSize = _stretchTotalUsableSize == 0 ? 1 : _stretchTotalUsableSize;
                width = width / stretchSize * (CGRectGetWidth(self.frame) - (_innerViews.count - 1) * _stretchValue);
            }
            
            height_max = MAX(height_max, height + margin.top + margin.bottom);
            width_max += width + padding.right + padding.left + margin.left + margin.right;
            
            if (_flexPosition == YYBAlertViewFlexPositionStretch && isCalculateContentSize == NO && idx != _innerViews.count - 1)
            {
                width_max += _stretchValue;
            }
        }
        
        if (isCalculateContentSize == NO)
        {
            view.frame = CGRectMake(x, y, width, height);
            
            [_scrollView addSubview:view];
            
            if ([innerViewObject isKindOfClass:[YYBAlertViewContainer class]])
            {
                YYBAlertViewContainer *container = (YYBAlertViewContainer *)innerViewObject;
                [container createContentViewsWithCalculationStatus:NO];
            }
        }
    }
    
    if (isCalculateContentSize == NO)
    {
        _scrollView.scrollEnabled = NO;
        if (_flexDirection == YYBAlertViewFlexDirectionVertical)
        {
            if (_maximalHeight < height_max)
            {
                if (_scrollViewScrollable)
                {
                    _scrollView.scrollEnabled = YES;
                }
            }
        }
        else if (_flexDirection == YYBAlertViewFlexDirectionHorizonal)
        {
            if (_maximalWidth < width_max)
            {
                if (_scrollViewScrollable)
                {
                    _scrollView.scrollEnabled = YES;
                }
            }
        }
        
        _scrollView.contentSize = CGSizeMake(width_max, height_max);
        
        width_max = MAX(_minimalWidth, MIN(width_max, _maximalWidth));
        height_max = MAX(_minimalHeight, MIN(height_max, _maximalHeight));
        
        _scrollView.frame = CGRectMake(0, 0, width_max, height_max);
        
        // 添加按钮
        if (_innerActions.count > 0)
        {
            CGSize contentSize = _actionsContainer.contentSize;
            UIEdgeInsets actionPadding = _actionsContainer.padding;
            
            CGFloat width_action = 0, height_action = 0;
            if (_flexDirection == YYBAlertViewFlexDirectionVertical)
            {
                width_action = MAX(contentSize.width, width_max);
                height_action = height_max + contentSize.height + actionPadding.top + actionPadding.bottom;
                
                _actionsContainer.frame = CGRectMake(actionPadding.left,
                                                     height_max + actionPadding.top,
                                                     width_action + actionPadding.right,
                                                     contentSize.height);
                
            }
            else if (_flexDirection == YYBAlertViewFlexDirectionHorizonal)
            {
                width_action = width_max + contentSize.width + actionPadding.left + actionPadding.right;
                height_action = MAX(contentSize.height, height_max);
                
                _actionsContainer.frame = CGRectMake(width_max + actionPadding.left,
                                                     actionPadding.top,
                                                     contentSize.width,
                                                     contentSize.height + actionPadding.bottom);
                
            }
            
            [_actionsContainer createContentViewsWithCalculationStatus:NO];
            _shadowView.frame = CGRectMake((CGRectGetWidth(self.frame) - width_action) / 2, (CGRectGetHeight(self.frame) - height_action) / 2, width_action, height_action);
            _contentView.frame = _shadowView.bounds;
        }
        else
        {
            _shadowView.frame = CGRectMake((CGRectGetWidth(self.frame) - width_max) / 2, (CGRectGetHeight(self.frame) - height_max) / 2, width_max, height_max);
            _contentView.frame = _shadowView.bounds;
        }
    }
    else
    {
        width_max = MAX(_minimalWidth, MIN(width_max, _maximalWidth));
        height_max = MAX(_minimalHeight, MIN(height_max, _maximalHeight));
        
        // 计算当前container contentsize的时候 需要考虑actions的大小
        if (_actionsContainer)
        {
            CGSize contentSize = _actionsContainer.contentSize;
            if (_flexDirection == YYBAlertViewFlexDirectionVertical)
            {
                _contentSize = CGSizeMake(MAX(contentSize.width, width_max), height_max);
            }
            else
            {
                _contentSize = CGSizeMake(width_max, MAX(contentSize.height, height_max));
            }
        }
        else
        {
            _contentSize = CGSizeMake(width_max, height_max);
        }
    }
}

- (void)addLabelWithBlock:(void (^)(YYBAlertViewAction *, UILabel *))block
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleLabel];
        block(action,action.label);
        
        [_innerViews addObject:action];
    }
}

- (void)addIconViewWithBlock:(void (^)(YYBAlertViewAction *, UIImageView *))block
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleImageView];
        block(action,action.imageView);
        
        [_innerViews addObject:action];
    }
}

- (void)addTextFieldWithBlock:(void (^)(YYBAlertViewAction *, UITextField *))block editingChangedBlock:(void (^)(NSString *))editBlock
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleTextField];
        action.stringBlock = editBlock;
        block(action,action.textField);
        
        [_innerViews addObject:action];
    }
}

- (void)addTextViewWithBlock:(void (^)(YYBAlertViewAction *, YYBPlaceholderTextView *))block editingChangedBlock:(void (^)(NSString *))editBlock
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleTextView];
        action.stringBlock = editBlock;
        block(action,action.textView);
        
        [_innerViews addObject:action];
    }
}

- (void)addButtonWithBlock:(void (^)(YYBAlertViewAction *, UIButton *))block tapedOnBlock:(void (^)(void))tapedOnBlock
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleButton];
        action.actionBlankBlock = tapedOnBlock;
        block(action,action.button);
        
        [_innerViews addObject:action];
    }
}

- (void)addCustomView:(UIView *)customView configureBlock:(void (^)(YYBAlertViewAction *, UIView *))block
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleCustomView];
        if (customView)
        {
            action.view = customView;
        }
        
        block(action,action.view);
        
        [_innerViews addObject:action];
    }
}

- (void)addActionWithBlock:(void (^)(YYBAlertViewAction *, UIButton *))block tapedOnBlock:(void (^)(NSInteger))tapedOnBlock
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleAction];
        action.actionBlock = tapedOnBlock;
        action.index = _innerActions.count;
        block(action,action.button);
        
        __weak typeof(self) wself = self;
        [_actionsContainer addButtonWithBlock:block tapedOnBlock:^{
            if (tapedOnBlock)
            {
                tapedOnBlock(action.index);
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"YYBALERTVIEW_ACTION_NOTIFICATION" object:nil userInfo:@{@"hash":@(wself.hash)}];
        }];
        [_innerActions addObject:action];
    }
}

- (void)addActivityIndicatorWithBlock:(void (^)(YYBAlertViewAction *, UIActivityIndicatorView *))block
{
    if (block)
    {
        YYBAlertViewAction *action = [[YYBAlertViewAction alloc] initWithStyle:YYBAlertViewActionStyleActivityIndicator];
        block(action,action.indicator);
        
        [_innerViews addObject:action];
    }
}

- (void)addContainerViewWithBlock:(void (^)(YYBAlertViewContainer *))block
{
    if (block)
    {
        YYBAlertViewContainer *container = [[YYBAlertViewContainer alloc] init];
        block(container);
        
        [_innerViews addObject:container];
    }
}

@end
