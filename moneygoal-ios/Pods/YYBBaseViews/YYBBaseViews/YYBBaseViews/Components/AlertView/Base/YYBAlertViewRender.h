//
//  YYBAlertViewRender.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/1/27.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YYBAlertViewRender <NSObject>

// 取消按钮颜色
- (UIColor *)cancelTintColor;
// 确定按钮颜色
- (UIColor *)renderTintColor;
- (UIColor *)backgroundViewColor;

// container的背景颜色 例如用于渲染ActionSheet的背景色，否则要一条线一条线添加
- (UIColor *)containerViewBackgroundColor;

// 主题文字颜色
- (UIColor *)mainTextColor;
// 次标题文字颜色
- (UIColor *)secondTextColor;
- (UIColor *)thirdTextColor;

// 输入框文字颜色
- (UIColor *)inputTextColor;

// 普通白色颜色的背景图
- (UIImage *)normalStateBackgroundImage;
- (UIImage *)highlightStateBackgroundImage;

- (CGFloat)commonCornerRadius;

@end

NS_ASSUME_NONNULL_END
