//
//  YYBAlertView+YYBCommon.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBAlertView.h"
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBAlertView (YYBCommon)

// 底部出来的视图
+ (void)showAlertViewWithActionModels:(NSArray *)actionModels title:(nullable NSString *)title actionTapedBlock:(YYBAlertViewTapedBlock)actionTapedBlock;

// ==================================
+ (void)showAlertViewWithTitle:(nullable NSString *)title detail:(nullable NSString *)detail
              firstActionTitle:(nullable NSString *)firstActionTitle firstActionTapedBlock:(nullable YYBAlertViewBlock)firstActionTapedBlock
             secondActionTitle:(nullable NSString *)secondActionTitle secondActionTapedBlock:(nullable YYBAlertViewBlock)secondActionTapedBlock;

// ==================================
// 全局遮罩 中心带菊花
+ (YYBAlertView *)showIndicatorAlertViewWithView:(UIView *)view backgroundColor:(UIColor *)backgroundColor;

// ==================================
// 加载视图
// 中间显示一个自定义的刷新图
+ (YYBAlertView *)showQueryAlertView;
+ (YYBAlertView *)showQueryAlertViewAtSuperView:(UIView *)superView;
// 上同+文案【加载中】
+ (YYBAlertView *)showQueryStringAlertView;
+ (YYBAlertView *)showQueryStringAlertViewWithString:(nullable NSString *)string inView:(nullable UIView *)inView isResizeContainer:(BOOL)isResizeContainer;

// ==================================
// 时间选择视图
+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * selectedDate))dateSelectedBlock;
+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * selectedDate))dateSelectedBlock mode:(UIDatePickerMode)mode date:(NSDate *)date;
+ (void)showDatePickerAlertViewWithSelectedBlock:(void (^)(NSDate * selectedDate))dateSelectedBlock date:(NSDate *)date datePickerSetupBlock:(void (^)(UIDatePicker *datePicker))datePickerSetupBlock;

// ==================================
// 文案显示
+ (void)showAlertViewWithStatusString:(NSString *)status;

+ (void)showSuccessStatusViewWithString:(NSString *)text duration:(NSTimeInterval)duration;
+ (void)showErrorStatusViewWithString:(NSString *)text duration:(NSTimeInterval)duration;

+ (void)showAlertViewWithError:(NSError *)error;
+ (void)showAlertViewWithError:(NSError *)error dismissAlertView:(YYBAlertView *)dismissAlertView;

// ==================================
// 遮罩图
+ (YYBAlertView *)showEmptyDataAlertViewInView:(UIView *)inView;
+ (YYBAlertView *)showNetworkErrorAlertViewInView:(UIView *)inView;
+ (YYBAlertView *)showRefreshDataLoadingAlertViewInView:(UIView *)inView;

// 文字遮罩图
+ (YYBAlertView *)showEmptyDataAlertViewWithTitle:(NSString *)title inView:(UIView *)inView margin:(UIEdgeInsets)margin;

+ (YYBAlertView *)showMaskAlertViewInView:(UIView *)inView icon:(UIImage *)icon iconSize:(CGSize)size
                                    title:(nullable NSString *)title content:(nullable NSString *)content
                             marginInsets:(UIEdgeInsets)marginInsets paddingInsets:(UIEdgeInsets)paddingInsets
                              actionTitle:(nullable NSString *)actionTitle tapedActionBlock:(nullable YYBAlertViewBlock)tapedActionBlock;

// ==================================
// 输入框
+ (void)showInputAlertViewWithTitle:(NSString *)title content:(NSString *)content placeholder:(NSString *)placeholder
                 stringChangedBlock:(void (^)(NSString * _Nonnull string))stringChangedBlock
                  cancelActionTitle:(NSString *)cancelActionTitle cancelTapedBlock:(void (^)(void))cancelTapedBlock
                 confirmActionTitle:(NSString *)confirmActionTitle confirmTapedBlock:(void (^)(void))confirmTapedBlock;
@end

NS_ASSUME_NONNULL_END
