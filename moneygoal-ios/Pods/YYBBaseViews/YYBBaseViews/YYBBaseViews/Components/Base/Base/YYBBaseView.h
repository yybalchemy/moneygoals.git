//
//  YYBBaseView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "YYBBaseTypedef.h"
#import "YYBPlaceholderTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBBaseView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIView *wrapperView, *firstWrapperView, *secondWrapperView;
@property (nonatomic, strong) UIView *firstLineView, *secondLineView, *thirdLineView;

@property (nonatomic, strong) UICollectionView *gatherView;

@property (nonatomic, strong) UIImageView *backgroundIconView;
@property (nonatomic, strong) UIImageView *avatorView;
@property (nonatomic, strong) UIImageView *firstIconView, *secondIconView, *thirdIconView;

@property (nonatomic, strong) UILabel *userNameLabel, *createTimeLabel, *updateTimeLabel, *moneyStringLabel;
@property (nonatomic, strong) UILabel *firstStringLabel, *secondStringLabel, *thirdStringLabel, *forthStringLabel;
@property (nonatomic, strong) UILabel *firstValueStringLabel, *secondValueStringLabel, *thirdValueStringLabel, *forthValueStringLabel;

@property (nonatomic, strong) UIButton *firstActionButton, *secondActionButton, *thirdActionButton;
@property (nonatomic, strong) UITextField *inputStringView;
@property (nonatomic, strong) YYBPlaceholderTextView *placeholderStringView;

// ========================================================

@property (nonatomic, strong) NSArray *dataList;

@property (nonatomic, copy) YYBTapedActionBlock tapedActionBlock;
@property (nonatomic, copy) YYBStringChangedBlock stringChangedBlock;
@property (nonatomic, copy) YYBStatusBlock statusBlock;
@property (nonatomic, copy) YYBTextBlock textBlock;

- (void)configViewWithDataList:(NSArray *)dataList;
- (void)configViewWithDataModel:(id)dataModel;
- (void)configViewWithDataModel:(id)dataModel contentList:(NSArray *)contentList;
- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status;

- (void)configViewWithTitleValue:(NSString *)titleValue;
- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue;
- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon;
- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status;

// ===============================
- (void)viewBeginConfigConstraints;
- (void)viewBeginConfigData;
- (void)viewBeginConfigBlocks;

@end

NS_ASSUME_NONNULL_END
