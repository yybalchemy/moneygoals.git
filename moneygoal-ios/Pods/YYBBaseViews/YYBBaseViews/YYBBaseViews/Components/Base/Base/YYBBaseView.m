//
//  YYBBaseView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBBaseView.h"

@implementation YYBBaseView

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    YYBBaseView *view = [super allocWithZone:zone];
    
    @weakify(view);
    [[view rac_signalForSelector:@selector(init)]
     subscribeNext:^(id x) {
        @strongify(view);
        [view viewBeginConfigConstraints];
        [view viewBeginConfigData];
        [view viewBeginConfigBlocks];
     }];
    
    return view;
}

- (void)configViewWithDataList:(NSArray *)dataList
{
    _dataList = dataList;
}

- (void)viewBeginConfigConstraints
{
    
}

- (void)viewBeginConfigData
{
    
}

- (void)viewBeginConfigBlocks
{
    
}

- (void)configViewWithDataModel:(id)dataModel
{
    
}

- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status
{
    
}

- (void)configViewWithDataModel:(id)dataModel contentList:(NSArray *)contentList
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue
{
    
}

@end
