//
//  YYBCollectionViewCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBCollectionViewCell.h"

@implementation YYBCollectionViewCell

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    YYBCollectionViewCell *view = [super allocWithZone:zone];
    
    @weakify(view);
    [[view rac_signalForSelector:@selector(initWithFrame:)]
     subscribeNext:^(id x) {
        @strongify(view);
        [view viewBeginConfigConstraints];
        [view viewBeginConfigData];
        [view viewBeginConfigBlocks];
     }];
    
    return view;
}

- (void)viewBeginConfigConstraints
{
    
}

- (void)viewBeginConfigData
{
    
}

- (void)viewBeginConfigBlocks
{
    
}

- (void)configViewWithDataModel:(id)dataModel
{
    
}

- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    
}

@end
