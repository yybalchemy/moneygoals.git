//
//  YYBCollectionViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/10/23.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewController.h"
#import "YYBAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBCollectionViewController : YYBViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong, readonly, nullable) UICollectionView *collectionView;
@property (nonatomic, strong, nullable) NSMutableArray *dataList;

- (UIEdgeInsets)handleEdgeInsets;
- (Class)handleUserCollectionViewClass;

- (NSArray <NSString *> *)dequeueCellIdentifiers;

- (void)handleBackgroundTapedAction;

@end

NS_ASSUME_NONNULL_END
