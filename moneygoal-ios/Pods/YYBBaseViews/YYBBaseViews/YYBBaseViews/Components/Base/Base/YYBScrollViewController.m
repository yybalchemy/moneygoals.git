//
//  YYBScrollViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBScrollViewController.h"

@interface YYBScrollViewController ()

@end

@implementation YYBScrollViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)beforeConfigViewAction
{
    [super beforeConfigViewAction];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _scrollView.contentSize = self.view.frame.size;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.alwaysBounceVertical = YES;
    [self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTaped:)];
    taped.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:taped];
}

- (void)handleTaped:(UITapGestureRecognizer *)gesture
{
    [self.scrollView endEditing:YES];
}

@end
