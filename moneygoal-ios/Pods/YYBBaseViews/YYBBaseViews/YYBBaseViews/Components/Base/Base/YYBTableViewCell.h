//
//  YYBTableViewCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "YYBPlaceholderTextView.h"
#import "YYBBaseTypedef.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YYBContainerRadiiType)
{
    YYBContainerRadiiTypeNone,
    YYBContainerRadiiTypeTop,
    YYBContainerRadiiTypeBottom,
    YYBContainerRadiiTypeSingleLine
};

@interface YYBTableViewCell : UITableViewCell <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic) YYBContainerRadiiType radiiType;
@property (nonatomic) CGFloat containerRadius;

@property (nonatomic, strong) UIView *wrapperView, *firstWrapperView, *secondWrapperView;
@property (nonatomic, strong) UIView *firstLineView, *secondLineView, *thirdLineView;

@property (nonatomic, strong) UICollectionView *gatherView;

@property (nonatomic, strong) UIImageView *backgroundIconView;
@property (nonatomic, strong) UIImageView *avatorView;
@property (nonatomic, strong) UIImageView *firstIconView, *secondIconView, *thirdIconView;

@property (nonatomic, strong) UILabel *userNameLabel, *createTimeLabel, *updateTimeLabel, *moneyStringLabel;
@property (nonatomic, strong) UILabel *firstStringLabel, *secondStringLabel, *thirdStringLabel, *forthStringLabel;
@property (nonatomic, strong) UILabel *firstValueStringLabel, *secondValueStringLabel, *thirdValueStringLabel, *forthValueStringLabel;

@property (nonatomic, strong) UIButton *firstActionButton, *secondActionButton, *thirdActionButton;
@property (nonatomic, strong) UITextField *inputStringView;
@property (nonatomic, strong) YYBPlaceholderTextView *placeholderStringView;

@property (nonatomic, strong) NSMutableArray *contentList;

@property (nonatomic, copy) YYBTapedActionBlock tapedActionBlock;
@property (nonatomic, copy) YYBStringChangedBlock stringChangedBlock;
@property (nonatomic, copy) YYBStatusBlock statusBlock;
@property (nonatomic, copy) YYBTextBlock textBlock;

- (void)configViewWithDataModel:(id)dataModel;
- (void)configViewWithDataModel:(id)dataModel contentList:(NSArray *)contentList;
- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status;

- (void)configViewWithTitleValue:(NSString *)titleValue;
- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue;
- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon;
- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status;

- (void)configViewWithContentList:(NSArray *)contentList;

+ (CGFloat)queryViewHeightWithDataModel:(id)dataModel;
+ (CGFloat)contentMaxWidth;

// =====================
@property (nonatomic, strong) CADisplayLink *timer;
// 最大时间 默认为60s
@property (nonatomic) NSInteger seconds, maxCountSeconds;

- (void)beginScheduleTimeAction;
- (void)endScheduleTimeCallbackAction;
- (NSInteger)scheduleTimeCallbackActionWithSeconds:(NSInteger)seconds;

// ===============================
- (void)viewBeginConfigConstraints;
- (void)viewBeginConfigData;
- (void)viewBeginConfigBlocks;

@end

NS_ASSUME_NONNULL_END
