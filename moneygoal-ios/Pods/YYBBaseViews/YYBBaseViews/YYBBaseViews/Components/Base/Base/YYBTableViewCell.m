//
//  YYBTableViewCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTableViewCell.h"

@implementation YYBTableViewCell

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    YYBTableViewCell *view = [super allocWithZone:zone];
    
    @weakify(view);
    [[view rac_signalForSelector:@selector(initWithStyle:reuseIdentifier:)]
     subscribeNext:^(id x) {
        @strongify(view);
        [view viewBeginConfigConstraints];
        [view viewBeginConfigData];
        [view viewBeginConfigBlocks];
     }];
    
    return view;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    _maxCountSeconds = 60;
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (_containerView && _radiiType != YYBContainerRadiiTypeNone) {
        UIBezierPath *bezier = nil;
        switch (_radiiType) {
            case YYBContainerRadiiTypeTop: {
                bezier = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(_containerRadius, _containerRadius)];
            }
                break;
            case YYBContainerRadiiTypeBottom: {
                bezier = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(_containerRadius, _containerRadius)];
            }
                break;
            case YYBContainerRadiiTypeSingleLine: {
                bezier = [UIBezierPath bezierPathWithRoundedRect:self.containerView.bounds cornerRadius:_containerRadius];
            }
                break;
            default:
                break;
        }
        
        CAShapeLayer *mask = [CAShapeLayer layer];
        mask.path = bezier.CGPath;
        _containerView.layer.mask = mask;
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)beginScheduleTimeAction
{
    _seconds = 0;
    
    _timer = [CADisplayLink displayLinkWithTarget:self selector:@selector(scheduleTimerCallback)];
    [_timer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    _timer.preferredFramesPerSecond = 1;
}

- (void)scheduleTimerCallback
{
    _seconds ++;
    if (_seconds > _maxCountSeconds) {
        [self endScheduleTimeCallbackAction];
        [_timer invalidate];
        _timer = nil;
    } else {
        [self scheduleTimeCallbackActionWithSeconds:_seconds];
    }
}

- (void)endScheduleTimeCallbackAction
{
    
}

- (NSInteger)scheduleTimeCallbackActionWithSeconds:(NSInteger)seconds
{
    return seconds;
}

- (void)viewBeginConfigConstraints
{
    
}

- (void)viewBeginConfigData
{
    
}

- (void)viewBeginConfigBlocks
{
    
}

- (void)configViewWithDataModel:(id)dataModel
{
    
}

- (void)configViewWithDataModel:(id)dataModel status:(BOOL)status
{
    
}

- (void)configViewWithDataModel:(id)dataModel contentList:(NSArray *)contentList
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue icon:(NSString *)icon
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue status:(BOOL)status
{
    
}

- (void)configViewWithTitleValue:(NSString *)titleValue detailValue:(NSString *)detailValue
{
    
}

- (void)configViewWithContentList:(NSArray *)contentList
{
    
}

+ (CGFloat)contentMaxWidth
{
    return 0;
}

+ (CGFloat)queryViewHeightWithDataModel:(id)dataModel
{
    return 0;
}

@end
