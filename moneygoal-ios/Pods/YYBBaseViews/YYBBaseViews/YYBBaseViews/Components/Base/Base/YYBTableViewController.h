//
//  YYBTableViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewController.h"
#import "YYBAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBTableViewController : YYBViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong, readonly) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataList;

- (UIEdgeInsets)handleEdgeInsets;

- (Class)handleUserTableViewClass;
- (NSArray <NSString *> *)dequeueCellIdentifiers;

@end

NS_ASSUME_NONNULL_END
