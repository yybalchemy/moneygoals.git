//
//  YYBTableViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBTableViewController.h"

@interface YYBTableViewController ()

@end

@implementation YYBTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataList = [NSMutableArray new];
    
    _tableView = [[[self handleUserTableViewClass] alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.contentInset = [self handleEdgeInsets];
    _tableView.estimatedRowHeight = 0;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    for (NSString *className in [self dequeueCellIdentifiers]) {
        [_tableView registerClass:NSClassFromString(className) forCellReuseIdentifier:className];
    }
    
    UITapGestureRecognizer *taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTaped:)];
    taped.cancelsTouchesInView = NO;
    [_tableView addGestureRecognizer:taped];
}

- (void)beginQueryData
{
    [super beginQueryData];
    
    [_tableView setContentOffset:CGPointMake(0, -_tableView.contentInset.top) animated:NO];
}

- (void)handleTaped:(UITapGestureRecognizer *)gesture
{
    [self.view endEditing:YES];
}

- (NSArray<NSString *> *)dequeueCellIdentifiers
{
    return @[];
}

- (Class)handleUserTableViewClass
{
    return [UITableView class];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    YYBNavigationTitleStyle style = [self customHeaderStyle];
    if (style == YYBNavigationTitleStyleScroll ||
        style == YYBNavigationTitleStyleOnlyScroll) {
        
        CGFloat customPageHeaderHeight = [self customPageHeaderHeight];
        CGFloat scrollTitleViewHeight = [self scrollTitleViewHeight];
        
        CGFloat offset = customPageHeaderHeight + scrollTitleViewHeight + scrollView.contentOffset.y;
        offset = MAX(offset, 0);
        offset = MIN(offset, scrollTitleViewHeight);
        
        self.navigationBar.titleBarLabel.alpha = offset / scrollTitleViewHeight;
        self.scrollTitleView.transform = CGAffineTransformMakeTranslation(0, -offset);
    }
}

- (UIEdgeInsets)handleEdgeInsets
{
    CGFloat top = [self customPageHeaderHeight] + [self scrollTitleViewHeight];
    return UIEdgeInsetsMake(top, 0, [UIDevice safeAreaInsetsBottom], 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataList.count;
}

@end
