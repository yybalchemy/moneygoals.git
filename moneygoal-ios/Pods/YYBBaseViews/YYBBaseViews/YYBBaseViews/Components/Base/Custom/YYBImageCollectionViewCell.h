//
//  YYBImageCollectionViewCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBCollectionViewCell.h"
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYBImageCollectionViewCell : YYBCollectionViewCell

- (void)configViewWithIconURL:(NSString *)iconURL cacheImage:(UIImage *)cacheImage placeholderImage:(nonnull UIImage *)placeholderImage;

@end

NS_ASSUME_NONNULL_END
