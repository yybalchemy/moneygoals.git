//
//  YYBRefreshCollectionViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2019/2/15.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBCollectionViewController.h"
#import "YYBAlertView+YYBCommon.h"
#import "YYBRefreshView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshCollectionViewController : YYBCollectionViewController

/// ======================= Refresh 相关
@property (nonatomic) BOOL isPreferRefreshHeaderView, isPreferRefreshSheetView;

@property (nonatomic) NSInteger initialPage;
@property (nonatomic) NSInteger page;

- (Class)customRefreshHeaderViewClass;
- (Class)customRefreshSheetViewClass;

- (void)handleCreateHeaderView;
- (void)handleCreateSheetView;

- (NSArray *)handleAppendData:(id)data;

/// ======================= 加载数据相关
@property (nonatomic, strong) YYBAlertView *dataLoadingAlertView;

- (YYBAlertView *)dataQueryAlertView;
- (YYBAlertView *)emptyDataAlertView;

// 是否加载空数据的时候显示无数据遮罩
@property (nonatomic) BOOL isEmptyDataViewEnable;

// 请求结束后的回调
// 首先处理这个方法,然后再执行默认的方法
- (void)handleDataBeforeCompleteWithTag:(BOOL)headerViewTag data:(NSArray *)data;
- (void)handleDataWhenCompleteWithTag:(BOOL)headerViewTag data:(NSArray *)data;

- (void)handleRequestWithCompletionBlock:(void (^)(id responseObject, NSDictionary *params))completionBlock errorBlock:(void (^)(NSError *error, NSDictionary *params))errorBlock headerViewTag:(BOOL)headerViewTag;

// 获取当前的页数
- (NSInteger)queryPageWithTag:(BOOL)headerViewTag;

// 获取第一次的数据
- (void)queryPremierData;

@end

NS_ASSUME_NONNULL_END
