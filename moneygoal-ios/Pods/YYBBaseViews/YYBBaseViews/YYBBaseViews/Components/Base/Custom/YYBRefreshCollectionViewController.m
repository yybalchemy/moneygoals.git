//
//  YYBRefreshCollectionViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2019/2/15.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshCollectionViewController.h"

@interface YYBRefreshCollectionViewController ()

@end

@implementation YYBRefreshCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _initialPage = 0;
    _page = 0;
}

- (void)beginConfigViewAction
{
    [super beginConfigViewAction];
    
    if (self.isPreferRefreshHeaderView) {
        [self handleCreateHeaderView];
    }
    
    if (self.isPreferRefreshSheetView) {
        [self handleCreateSheetView];
    }
    
    [self queryPremierData];
}

- (BOOL)isPreferRefreshHeaderView
{
    return YES;
}

- (BOOL)isPreferRefreshSheetView
{
    return YES;
}

- (void)handleCreateHeaderView
{
    @weakify(self);
    [self.collectionView handleCreateHeaderViewWithClass:[self customRefreshHeaderViewClass] block:^{
        @strongify(self);
        [self beginRefreshHeaderDataAction];
    }];
}

- (Class)customRefreshHeaderViewClass
{
    return [YYBRefreshRoundHeaderView class];
}

- (void)handleCreateSheetView
{
    @weakify(self);
    [self.collectionView handleCreateSheetViewWithClass:[self customRefreshSheetViewClass] block:^{
        @strongify(self);
        [self beginRefreshSheetDataAction];
    }];
}

- (Class)customRefreshSheetViewClass
{
    return [YYBRefreshRoundSheetView class];
}

- (YYBAlertView *)dataQueryAlertView
{
    return [YYBAlertView showRefreshDataLoadingAlertViewInView:self.view];
}

- (YYBAlertView *)emptyDataAlertView
{
    return [YYBAlertView showEmptyDataAlertViewInView:self.view];
}

- (NSArray *)handleAppendData:(id)responseObject
{
    return nil;
}

- (void)handleDataBeforeCompleteWithTag:(BOOL)headerViewTag data:(nonnull NSArray *)data
{
    [self.refreshAlertView closeAlertView];
    
    if (headerViewTag == YES) {
        if (self.dataList.count == 0) {
            self.refreshAlertView = [YYBAlertView showEmptyDataAlertViewInView:self.collectionView];
        } else {
            self.page = self.initialPage + 1;
        }
    } else {
        if (data.count != 0) {
            self.page ++;
        }
    }
}

- (void)handleDataWhenCompleteWithTag:(BOOL)headerViewTag data:(NSArray *)data
{
    
}

- (void)handleRequestWithCompletionBlock:(void (^)(id _Nonnull, NSDictionary * _Nonnull))completionBlock errorBlock:(void (^)(NSError * _Nonnull, NSDictionary * _Nonnull))errorBlock headerViewTag:(BOOL)headerViewTag
{
    
}

- (NSInteger)queryPageWithTag:(BOOL)headerViewTag
{
    NSInteger page = 1;
    if (!headerViewTag) {
        page = self.page + 1;
    }
    return page;
}

- (void)queryPremierData
{
    @weakify(self);
    self.refreshAlertView = [self dataQueryAlertView];
    
    [self handleRequestWithCompletionBlock:^(id responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *results = [self handleAppendData:responseObject];
        
        self.dataList = [NSMutableArray new];
        if (results.count > 0) {
            self.dataList = results.mutableCopy;
        }
        
        [self handleDataBeforeCompleteWithTag:YES data:results];
        
        [self.collectionView reloadData];
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        
        [self handleDataWhenCompleteWithTag:YES data:results];
        
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:YES];
}

- (void)beginRefreshHeaderDataAction
{
    @weakify(self);
    [self handleRequestWithCompletionBlock:^(id  _Nonnull responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *results = [self handleAppendData:responseObject];
        if (results.count > 0) {
            self.dataList = results.mutableCopy;
        } else {
            self.dataList = [NSMutableArray new];
        }
        
        [self handleDataBeforeCompleteWithTag:YES data:results];
        
        [CATransaction begin];
        [self.collectionView reloadData];
        [CATransaction setDisableActions:YES];
        [CATransaction commit];
        
        if (self.collectionView.emptyView) {
            [self.collectionView removeRefreshEmptyView];
            [self handleCreateSheetView];
        }
        
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        
        [self.collectionView.headerView endRefreshAnimationWithBlock:^{
            [self handleDataWhenCompleteWithTag:YES data:results];
        }];
        
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        [self.collectionView.headerView endRefreshAnimation];
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:YES];
}

- (void)beginRefreshSheetDataAction
{
    @weakify(self);
    [self handleRequestWithCompletionBlock:^(id  _Nonnull responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *results = [self handleAppendData:responseObject];
        if (results.count > 0) {
            [self.dataList addObjectsFromArray:results];
        }
        
        [self handleDataBeforeCompleteWithTag:NO data:results];
        
        [self.collectionView.sheetView endRefreshAnimationWithBlock:^{
            if (results.count > 0) {
                [self.collectionView reloadData];
            }
            
            [self handleDataWhenCompleteWithTag:NO data:results];
        }];
        
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        [self.collectionView.sheetView endRefreshAnimation];
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:NO];
}

@end
