//
//  YYBRefreshTableViewController.m
//  YYBBaseViews
//
//  Created by alchemy on 2019/1/27.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshTableViewController.h"

@interface YYBRefreshTableViewController ()

@end

@implementation YYBRefreshTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _initialPage = 0;
    _page = 0;
    _isEmptyDataViewEnable = YES;
}

- (void)beginConfigViewAction
{
    [super beginConfigViewAction];
    
    if (self.isPreferRefreshHeaderView) {
        [self handleCreateHeaderView];
    }
    
    if (self.isPreferRefreshSheetView) {
        [self handleCreateSheetView];
    }
    
    [self queryPremierData];
}

- (void)handleCreateHeaderView
{
    @weakify(self);
    [self.tableView handleCreateHeaderViewWithClass:[self customRefreshHeaderViewClass] block:^{
        @strongify(self);
        [self beginRefreshHeaderDataAction];
    }];
}

- (Class)customRefreshHeaderViewClass
{
    return [YYBRefreshRoundHeaderView class];
}

- (void)handleCreateSheetView
{
    @weakify(self);
    [self.tableView handleCreateSheetViewWithClass:[self customRefreshSheetViewClass] block:^{
        @strongify(self);
        [self beginRefreshSheetDataAction];
    }];
}

- (Class)customRefreshSheetViewClass
{
    return [YYBRefreshRoundSheetView class];
}

- (BOOL)isPreferRefreshSheetView
{
    return YES;
}

- (BOOL)isPreferRefreshHeaderView
{
    return YES;
}

- (YYBAlertView *)dataQueryAlertView
{
    return [YYBAlertView showRefreshDataLoadingAlertViewInView:self.view];
}

- (YYBAlertView *)emptyDataAlertView
{
    return [YYBAlertView showEmptyDataAlertViewInView:self.tableView];
}

- (NSArray *)handleAppendData:(id)responseObject
{
    return nil;
}

- (void)handleDataBeforeCompleteWithTag:(BOOL)headerViewTag data:(NSArray *)data
{
    
}

- (void)handleDataWhenCompleteWithTag:(BOOL)headerViewTag data:(NSArray *)data
{
    [self.refreshAlertView closeAlertView];
    
    if (headerViewTag == YES) {
        if (self.dataList.count == 0) {
            if (self.isEmptyDataViewEnable) {
                self.refreshAlertView = [self emptyDataAlertView];
            }
        } else {
            self.page = self.initialPage + 1;
        }
    } else {
        if (data.count != 0) {
            self.page ++;
        }
    }
}

- (void)handleRequestWithCompletionBlock:(void (^)(id _Nonnull, NSDictionary * _Nonnull))completionBlock errorBlock:(void (^)(NSError * _Nonnull, NSDictionary * _Nonnull))errorBlock headerViewTag:(BOOL)headerViewTag
{
    
}

- (NSInteger)queryPageWithTag:(BOOL)headerViewTag
{
    NSInteger page = 1;
    if (!headerViewTag) {
        page = self.page + 1;
    }
    return page;
}

- (void)queryPremierData
{
    @weakify(self);
    self.refreshAlertView = [self dataQueryAlertView];
    [self handleRequestWithCompletionBlock:^(id responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *dataList = [self handleAppendData:responseObject];
        if (dataList.count > 0) {
            self.dataList = dataList.mutableCopy;
        } else {
            self.dataList = [NSMutableArray new];
        }
        
        [self handleDataBeforeCompleteWithTag:YES data:dataList];
        
        [self.tableView reloadData];
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        
        [self handleDataWhenCompleteWithTag:YES data:dataList];
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:YES];
}

- (void)beginRefreshHeaderDataAction
{
    @weakify(self);
    [self handleRequestWithCompletionBlock:^(id  _Nonnull responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *results = [self handleAppendData:responseObject];
        if (results.count > 0) {
            self.dataList = results.mutableCopy;
        } else {
            self.dataList = [NSMutableArray new];
        }
        
        [self handleDataBeforeCompleteWithTag:YES data:results];
        
        [self.tableView reloadData];
        
        if (self.tableView.emptyView) {
            [self.tableView removeRefreshEmptyView];
            [self handleCreateSheetView];
        }
        
        if (self.refreshAlertView) {
            [self.refreshAlertView closeAlertView];
        }
        [self.tableView.headerView endRefreshAnimation];
        
        [self handleDataWhenCompleteWithTag:YES data:results];
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        [self.tableView.headerView endRefreshAnimation];
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:YES];
}

- (void)beginRefreshSheetDataAction
{
    @weakify(self);
    [self handleRequestWithCompletionBlock:^(id  _Nonnull responseObject, NSDictionary *params) {
        @strongify(self);
        
        NSArray *results = [self handleAppendData:responseObject];
        if (results.count > 0) {
            [self.dataList addObjectsFromArray:results];
        }
        
        [self handleDataBeforeCompleteWithTag:NO data:results];
        
        [self.tableView.sheetView endRefreshAnimationWithBlock:^{
            if (results.count > 0) {
                [self.tableView reloadData];
            } else {
                if (self.tableView.sheetView) {
                    [self.tableView removeRefreshSheetView];
                    [self.tableView createRefreshEmptyView];
                }
            }
            
            [self handleDataWhenCompleteWithTag:NO data:results];
        }];
        
    } errorBlock:^(NSError * _Nonnull error, NSDictionary *params) {
        
        @strongify(self);
        [self.tableView.sheetView endRefreshAnimation];
        [YYBAlertView showAlertViewWithError:error];
        
    } headerViewTag:NO];
}

@end
