//
//  YYBTabBarViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/10/28.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>
#import "YYBViewController.h"
#import "YYBTabBar.h"
#import "YYBNavigationBarContainer.h"

typedef NS_ENUM(NSInteger, YYBTabBarStyle)
{
    YYBTabBarStyleCustomize,
    YYBTabBarStyleDefault
};

NS_ASSUME_NONNULL_BEGIN

@interface YYBTabBarViewController : UITabBarController <YYBTabBarDelegate>

// ============== 以下为必填
- (NSArray *)customViewControllerNames;
- (NSMutableArray <YYBNavigationBarContainer *>*)customTabBarContainerList;
// ==============

@property (nonatomic, strong) YYBTabBar *customTabBar;
@property (nonatomic, strong) NSMutableArray *tabBarContainerList;

// tabbar component构建样式
// 默认是YYBTabBarStyleDefault
@property (nonatomic) YYBTabBarStyle style;

// 以下当style = YYBTabBarStyleCustomize 不起作用
// 可以是image 也可以是 NSString
@property (nonatomic, strong) NSMutableArray *plainIconList;
@property (nonatomic, strong) NSMutableArray *selectedIconList;

@property (nonatomic, strong) NSMutableArray *titleList;
@property (nonatomic) CGSize iconSize;
@property (nonatomic, strong) UIColor *plainTitleColor;
@property (nonatomic, strong) UIColor *selectedTitleColor;
@property (nonatomic, strong) UIFont *titleFont;

- (YYBNavigationBarControl *)commonContainerWithPlainStateIcon:(id)plainStateIcon selectedStateIcon:(id)selectedStateIcon title:(NSString *)title;

// ===============
- (void)afterConfigViewAction;
- (void)beginQueryData;
- (void)beginCreateNotifications;

@end

NS_ASSUME_NONNULL_END
