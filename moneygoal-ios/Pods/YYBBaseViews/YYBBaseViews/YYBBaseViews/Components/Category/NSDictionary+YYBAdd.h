//
//  NSDictionary+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/2/14.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary<__covariant KeyType, __covariant ObjectType> (YYBAdd)

- (BOOL)isEmpty;

- (void)each:(nullable void (^)(KeyType _Nullable key,ObjectType _Nullable obj))block;
- (nullable id)match:(nullable BOOL (^)(KeyType _Nullable key, ObjectType _Nullable obj))block;
- (nullable NSDictionary *)select:(nullable BOOL (^)(KeyType _Nullable key, ObjectType _Nullable obj))block;
- (nullable NSDictionary *)reject:(nullable BOOL (^)(KeyType _Nullable key, ObjectType _Nullable obj))block;
- (nullable NSDictionary *)map:(nullable id (^)(KeyType _Nullable key, ObjectType _Nullable obj))block;

/**
 按照URL拼接的方式进行拼接
 @return A=123&B=234&C=代码 ...
 */
- (NSString *)stringByURLAppending;

/**
 添加另外的字典的数据
 */
- (nullable NSDictionary *)appendingParameters:(nullable NSDictionary *)parameters;

/**
 根据文件名获取字典数据
 */
+ (nullable NSDictionary *)toJSONObject:(NSString *)JSONFile;

@end

NS_ASSUME_NONNULL_END
