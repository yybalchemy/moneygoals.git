//
//  UIButton+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

typedef void (^ YYBButtonConfigBlock) (UIButton *view);
typedef void (^ YYBButtonTapedBlock) (UIButton *view);

@interface UIButton (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBButtonTapedBlock)configureBlock;

+ (instancetype)createAtSuperView:(UIView *)superView constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBButtonConfigBlock)configureBlock tapedBlock:(YYBButtonTapedBlock)tapedBlock;

// ==========
+ (instancetype)createAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius fontValue:(UIFont *)fontValue title:(NSString *)title titleColor:(UIColor *)titleColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBButtonConfigBlock)configureBlock tapedBlock:(YYBButtonTapedBlock)tapedBlock;

- (void)configBackgroundImageWithNormalState:(NSString *)firstImage selectedImage:(NSString *)secondImage;
- (void)configCornerRadius:(CGFloat)cornerRadius string:(NSString *)string fontValue:(UIFont *)fontValue backgroundColor:(UIColor *)backgroundColor titleColor:(UIColor *)titleColor;

@end
