//
//  UIButton+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIButton+YYBLayout.h"
#import <objc/runtime.h>

static const void *YYBTapedBlockKey = &YYBTapedBlockKey;

@interface UIButton ()
@property (nonatomic, copy) void (^ tapedBlock)(UIButton *sender);

@end

@implementation UIButton (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius fontValue:(UIFont *)fontValue title:(NSString *)title titleColor:(UIColor *)titleColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBButtonConfigBlock)configureBlock tapedBlock:(YYBButtonTapedBlock)tapedBlock
{
    return [self createAtSuperView:superView constraintBlock:constraintBlock configureBlock:^(UIButton *button) {
        
        button.layer.cornerRadius = cornerRadius;
        button.clipsToBounds = YES;
        
        button.titleLabel.font = fontValue;
        [button setTitle:title forState:0];
        [button setTitleColor:titleColor forState:0];
        
        if (configureBlock) {
            configureBlock(button);
        }
        
    } tapedBlock:tapedBlock];
}

+ (instancetype)createAtSuperView:(UIView *)superView constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBButtonConfigBlock)configureBlock
{
    return [self createAtSuperView:superView constraintBlock:constraintBlock configureBlock:configureBlock tapedBlock:nil];
}

+ (instancetype)createAtSuperView:(UIView *)superView constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBButtonConfigBlock)configureBlock tapedBlock:(YYBButtonTapedBlock)tapedBlock
{
    UIButton *view = [[[self class] alloc] init];
    if (superView) {
        [superView addSubview:view];
        
        if (constraintBlock) {
            [view mas_makeConstraints:constraintBlock];
        }
        if (configureBlock) {
            configureBlock(view);
        }
        
        if (tapedBlock) {
            view.tapedBlock = tapedBlock;
            
            [view addTarget:view action:@selector(buttonActionBlock:) forControlEvents:1<<6];
        }
    }
    return view;
}

- (void)buttonActionBlock:(UIButton *)sender
{
    if (self.tapedBlock) {
        self.tapedBlock(sender);
    }
}

- (void)setTapedBlock:(void (^)(UIButton *))tapedBlock
{
    objc_setAssociatedObject(self, YYBTapedBlockKey, tapedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (void (^)(UIButton *))tapedBlock
{
    return objc_getAssociatedObject(self, YYBTapedBlockKey);
}

// ===================
- (void)configBackgroundImageWithNormalState:(NSString *)firstImage selectedImage:(NSString *)secondImage
{
    [self setBackgroundImage:[UIImage imageNamed:firstImage] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:secondImage] forState:UIControlStateSelected];
}

- (void)configCornerRadius:(CGFloat)cornerRadius string:(NSString *)string fontValue:(UIFont *)fontValue backgroundColor:(UIColor *)backgroundColor titleColor:(UIColor *)titleColor
{
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds = YES;
    
    [self setTitle:string forState:0];
    self.titleLabel.font = fontValue;
    self.backgroundColor = backgroundColor;
    [self setTitleColor:titleColor forState:0];
}

@end
