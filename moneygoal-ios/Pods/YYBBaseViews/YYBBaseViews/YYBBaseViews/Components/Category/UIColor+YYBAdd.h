//
//  UIColor+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/2/14.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (YYBAdd)

// 十六进制转化UIColor
+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;

+ (UIColor *)colorWithHexValue:(NSUInteger)hexInteger;
+ (UIColor *)colorWithHexValue:(NSUInteger)hexInteger alpha:(CGFloat)alpha;

// 颜色转化成UIImage
- (UIImage *)colorToUIImage;
- (UIImage *)colorToUIImageWithSize:(CGSize)specSize;

@end
