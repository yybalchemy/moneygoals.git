//
//  UIDevice+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/12/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (YYBAdd)

+ (CGFloat)safeAreaInsetsTop;
+ (CGFloat)safeAreaInsetsBottom;

@end

NS_ASSUME_NONNULL_END
