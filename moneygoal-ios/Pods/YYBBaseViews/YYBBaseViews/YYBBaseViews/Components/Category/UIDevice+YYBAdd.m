//
//  UIDevice+YYBAdd.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/12/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "UIDevice+YYBAdd.h"

@implementation UIDevice (YYBAdd)

+ (CGFloat)safeAreaInsetsTop {
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    BOOL hasApplication = UIApplicationClass && [UIApplicationClass respondsToSelector:@selector(sharedApplication)];
    if (hasApplication) {
        UIApplication * app = [UIApplicationClass performSelector:@selector(sharedApplication)];
        
        if (@available(iOS 11.0, *)) {
            UIWindow *window = app.delegate.window;
            if (window.safeAreaInsets.bottom > 0) {
                return app.delegate.window.safeAreaInsets.top;
            }
        }
    }

    return 20;
}

+ (CGFloat)safeAreaInsetsBottom {
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    BOOL hasApplication = UIApplicationClass && [UIApplicationClass respondsToSelector:@selector(sharedApplication)];
    if (hasApplication) {
        UIApplication * app = [UIApplicationClass performSelector:@selector(sharedApplication)];
        
        if (@available(iOS 11.0, *)) {
            UIWindow *window = app.delegate.window;
            if (window.safeAreaInsets.bottom > 0) {
                return app.delegate.window.safeAreaInsets.bottom;
            }
        }
    }
    return 0;
}

@end
