//
//  UIGestureRecognizer+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIGestureRecognizer (YYBAdd)

+ (instancetype)recognizerWithBlock:(void (^)(UIGestureRecognizer *gesture))block;
- (instancetype)initWithBlock:(void (^)(UIGestureRecognizer *gesture))block;

@end
