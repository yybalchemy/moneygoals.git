//
//  UIGestureRecognizer+YYBAdd.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIGestureRecognizer+YYBAdd.h"
#import <objc/runtime.h>

static const void *YYBRecognizerBlockKey = &YYBRecognizerBlockKey;

@interface UIGestureRecognizer ()

@property (nonatomic, copy) void (^ recognizerBlock)(UIGestureRecognizer *recognier);

@end

@implementation UIGestureRecognizer (YYBAdd)

+ (instancetype)recognizerWithBlock:(void (^)(UIGestureRecognizer *))block
{
    return [[[self class] alloc] initWithBlock:block];
}

- (instancetype)initWithBlock:(void (^)(UIGestureRecognizer *))block
{
    self = [self initWithTarget:self action:@selector(handleAtion:)];
    if (!self) return nil;
    
    self.recognizerBlock = block;
    
    return self;
}

- (void)handleAtion:(UIGestureRecognizer *)recognizer
{
    if (self.recognizerBlock) {
        self.recognizerBlock(recognizer);
    }
}

- (void)setRecognizerBlock:(void (^)(UIGestureRecognizer *))recognizerBlock
{
    objc_setAssociatedObject(self, YYBRecognizerBlockKey, recognizerBlock, OBJC_ASSOCIATION_COPY);
}

- (void (^)(UIGestureRecognizer *))recognizerBlock
{
    return objc_getAssociatedObject(self, YYBRecognizerBlockKey);
}

@end
