//
//  UIImageView+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIImageView+YYBLayout.h"

@implementation UIImageView (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView iconName:(NSString *)iconName constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBImageViewConfigBlock)configureBlock
{
    UIImageView *view = [UIImageView new];
    if (iconName) {
        view.image = [UIImage imageNamed:iconName];
    }
    
    if (superView) {
        [superView addSubview:view];
        if (constraintBlock) {
            [view mas_makeConstraints:constraintBlock];
        }
        if (configureBlock) {
            configureBlock(view);
        }
    }
    return view;
}

+ (instancetype)createAvatarAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBImageViewConfigBlock)configureBlock
{
    return [self createAtSuperView:superView iconName:nil constraintBlock:constraintBlock configureBlock:^(UIImageView *view) {
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds = YES;
        view.layer.cornerRadius = cornerRadius;
        
        if (configureBlock) {
            configureBlock(view);
        }
    }];
}

@end
