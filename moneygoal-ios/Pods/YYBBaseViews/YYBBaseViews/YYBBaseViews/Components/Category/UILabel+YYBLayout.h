//
//  UILabel+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "NSMutableAttributedString+YYBAdd.h"

typedef void (^ YYBLabelConfigBlock) (UILabel *view);

@interface UILabel (YYBLayout)

+ (instancetype)createAtSuperView:(UIView *)superView fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBLabelConfigBlock)configureBlock;

+ (instancetype)createAtSuperView:(UIView *)superView string:(NSString *)string lineSpacingValue:(CGFloat)lineSpacingValue alignment:(NSTextAlignment)alignment fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBLabelConfigBlock)configureBlock;
 
@end
