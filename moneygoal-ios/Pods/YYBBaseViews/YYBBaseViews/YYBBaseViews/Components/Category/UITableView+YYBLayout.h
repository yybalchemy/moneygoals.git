//
//  UITableView+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/24.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

typedef void (^ YYBTableViewConfigBlock) (UITableView *view);

@interface UITableView (YYBLayout)

+ (UITableView *)createAtSuperView:(UIView *)superView delagateBlock:(id)delegeteTarget constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock dequeueCellIdentifiers:(NSArray *)dequeueCellIdentifiers configureBlock:(YYBTableViewConfigBlock)configureBlock;

@end
