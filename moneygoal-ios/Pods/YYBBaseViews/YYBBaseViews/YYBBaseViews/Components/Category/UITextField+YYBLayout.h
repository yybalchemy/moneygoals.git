//
//  UITextField+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

typedef void (^ YYBTextFieldConfigBlock) (UITextField *view);

@interface UITextField (YYBLayout)

+ (UITextField *)createAtSuperView:(UIView *)superView leftViewOffset:(CGFloat)leftViewOffset cornerRadius:(CGFloat)cornerRadius fontValue:(UIFont *)fontValue textColor:(UIColor *)textColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBTextFieldConfigBlock)configureBlock;

@end
