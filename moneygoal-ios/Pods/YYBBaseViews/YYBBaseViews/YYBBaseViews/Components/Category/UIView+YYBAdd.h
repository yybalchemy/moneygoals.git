//
//  UIView+YYBAdd.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/2/14.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (YYBAdd)

// 角标
- (void)cornerRadius:(CGFloat)radius width:(CGFloat)width color:(UIColor *)color;
- (void)cornerRadius:(CGFloat)radius;

// 截屏
- (UIImage *)snap;

- (void)whenTouches:(NSUInteger)numberOfTouches tapped:(NSUInteger)numberOfTaps block:(void (^)(void))block;
- (void)whenTapped:(void (^)(void))block;
- (void)whenDoubleTapped:(void (^)(void))block;
- (void)eachSubview:(void (^)(UIView *subview))block;

@end
