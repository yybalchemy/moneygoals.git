//
//  UIView+YYBLayout.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

@interface UIView (YYBLayout)

typedef void (^ YYBViewConfigBlock) (UIView *view);

+ (void)createBottomSeparateViewAtSuperView:(UIView *)superView color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets;
+ (void)createTopSeparateViewAtSuperView:(UIView *)superView color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets;

+ (instancetype)createViewAtSuperView:(UIView *)superView backgroundColor:(UIColor *)backgroundColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBViewConfigBlock)configureBlock;

+ (instancetype)createViewAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius backgroundColor:(UIColor *)backgroundColor constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(YYBViewConfigBlock)configureBlock;

+ (id)createViewAtSuperView:(UIView *)superView constraintBlock:(void(^)(MASConstraintMaker *make))constraintBlock configureBlock:(void (^)(id view))configureBlock;

@end
