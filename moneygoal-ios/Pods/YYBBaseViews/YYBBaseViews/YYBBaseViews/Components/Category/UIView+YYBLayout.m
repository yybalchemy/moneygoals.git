//
//  UIView+YYBLayout.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIView+YYBLayout.h"

@implementation UIView (YYBLayout)

+ (void)createBottomSeparateViewAtSuperView:(UIView *)superView color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets
{
    UIView *view = [UIView new];
    if (color) {
        view.backgroundColor = color;
    } else {
        view.backgroundColor = [UIColor lightGrayColor];
    }
    
    if (superView) {
        [superView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(superView).offset(edgeInsets.left);
            make.right.equalTo(superView).offset(-edgeInsets.right);
            make.bottom.equalTo(superView).offset(-edgeInsets.bottom);
            make.height.mas_equalTo(height);
        }];
    }
}

+ (void)createTopSeparateViewAtSuperView:(UIView *)superView color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets
{
    UIView *view = [UIView new];
    if (color) {
        view.backgroundColor = color;
    } else {
        view.backgroundColor = [UIColor lightGrayColor];
    }
    
    if (superView) {
        [superView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(superView).offset(edgeInsets.left);
            make.right.equalTo(superView).offset(-edgeInsets.right);
            make.top.equalTo(superView).offset(edgeInsets.top);
            make.height.mas_equalTo(height);
        }];
    }
}

+ (id)createViewAtSuperView:(UIView *)superView constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(void (^)(id))configureBlock
{
    id view = [[[self class] alloc] init];
    if (superView) {
        [superView addSubview:view];
    }
    if (constraintBlock) {
        [view mas_makeConstraints:constraintBlock];
    }
    if (configureBlock) {
        configureBlock(view);
    }
    return view;
}

+ (instancetype)createViewAtSuperView:(UIView *)superView backgroundColor:(UIColor *)backgroundColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBViewConfigBlock)configureBlock
{
    return [self createViewAtSuperView:superView cornerRadius:0 backgroundColor:backgroundColor constraintBlock:constraintBlock configureBlock:configureBlock];
}

+ (instancetype)createViewAtSuperView:(UIView *)superView cornerRadius:(CGFloat)cornerRadius backgroundColor:(UIColor *)backgroundColor constraintBlock:(void (^)(MASConstraintMaker *))constraintBlock configureBlock:(YYBViewConfigBlock)configureBlock
{
    UIView *view = [[[self class] alloc] init];
    if (superView) {
        [superView addSubview:view];
        
        view.layer.cornerRadius = cornerRadius;
        view.clipsToBounds = YES;
        
        if (backgroundColor) {
            view.backgroundColor = backgroundColor;
        }
        if (constraintBlock) {
            [view mas_makeConstraints:constraintBlock];
        }
        if (configureBlock) {
            configureBlock(view);
        }
    }
    return view;
}

@end
