//
//  YYBCategoryHeader.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/1/26.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#ifndef YYBCategoryHeader_h
#define YYBCategoryHeader_h

#import "UIColor+YYBAdd.h"

static inline BOOL isIphoneX () {
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    BOOL hasApplication = UIApplicationClass && [UIApplicationClass respondsToSelector:@selector(sharedApplication)];
    if (hasApplication) {
        UIApplication * app = [UIApplicationClass performSelector:@selector(sharedApplication)];
        
        if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {
            return NO;
        }
        if (app.delegate.window.safeAreaInsets.bottom > 0) {
            return YES;
        }
    }
    
    return NO;
}

static inline BOOL isIpad ()
{
    return UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

static inline UIFont * YYBFONT_NORMAL(CGFloat x)
{
    return [UIFont systemFontOfSize:x];
}

static inline UIFont * YYBFONT_MEDIUM(CGFloat x)
{
    return [UIFont systemFontOfSize:x weight:UIFontWeightMedium];
}

static inline UIFont * YYBFONT_BOLD(CGFloat x)
{
    return [UIFont systemFontOfSize:x weight:UIFontWeightBold];
}

static inline UIColor * YYBCOLOR(NSInteger hex)
{
    return [UIColor colorWithHexValue:hex];
}

#endif /* YYBCategoryHeader_h */
