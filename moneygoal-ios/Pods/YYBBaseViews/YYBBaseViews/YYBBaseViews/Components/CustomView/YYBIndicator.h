//
//  YYBIndicator.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/5/29.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBIndicator : UIView

@property (nonatomic,readwrite) UIColor *activeTintColor;
@property (nonatomic,readwrite) UIColor *deactiveTintColor;

@property (nonatomic,readwrite) float progress;

@end
