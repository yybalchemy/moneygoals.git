//
//  YYBMultiScrollContainersView.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/13.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBMultiScrollContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@class YYBMultiScrollContainersView;

@protocol YYBMultiScrollContainersViewDelegate <NSObject>

@optional

- (NSInteger)numbersOfContainersInContainersView:(YYBMultiScrollContainersView *)containersView;
- (NSInteger)numbersOfSectionInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex;
/// itemIndex sectonView的序号
- (void)configureTableViewInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex;

- (void)containersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex scrollToItemAtIndex:(NSInteger)itemIndex;

- (NSInteger)numbersOfSectionsInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex;
- (NSInteger)numbersOfRowsInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex section:(NSInteger)section;

- (CGFloat)heightForRowsInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (UITableViewCell *)cellForRowInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (void)didSelectedRowInContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableView:(UITableView *)tableView itemIndex:(NSInteger)itemIndex indexPath:(NSIndexPath *)indexPath;

- (void)containersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex tableViewDidScrollWithOffset:(CGFloat)offset itemIndex:(NSInteger)itemIndex;

// 当前选中的tab, refresh的时候根据这个创建第一个Container
- (NSInteger)selectedContainerIndexAtContainersView:(YYBMultiScrollContainersView *)containersView;
// 初始化选中的container中的tableView的index
- (NSInteger)selectedItemIndexAtContainersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex;
// 滚动动画结束后
- (void)containersView:(YYBMultiScrollContainersView *)containersView containerIndex:(NSInteger)containerIndex didEndScrollAnimationAtItemIndex:(NSInteger)itemIndex;

@end

@interface YYBMultiScrollContainersView : UIView

@property (nonatomic, weak) id<YYBMultiScrollContainersViewDelegate> delegate;

- (void)refreshScrollContainers;

// 滚动到index的位置
- (void)scrollContainerToIndex:(NSInteger)index;

- (YYBMultiScrollContainerView *)currentContainerView;
- (YYBMultiScrollContainerView *)containerViewAtIndex:(NSInteger)containerIndex;

- (void)refreshContainer:(NSInteger)containerIndex containerItemIndex:(NSInteger)itemIndex scroll:(BOOL)scroll;
- (void)refreshAndScrollContainerViewAtItemIndex:(NSInteger)itemIndex;

@end

NS_ASSUME_NONNULL_END
