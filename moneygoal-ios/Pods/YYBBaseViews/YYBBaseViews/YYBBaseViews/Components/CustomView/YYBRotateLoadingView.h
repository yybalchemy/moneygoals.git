//
//  YYBRotateLoadingView.h
//  YYBBaseViews
//
//  Created by yyb on 2023/1/7.
//  Copyright © 2023 Univease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRotateLoadingView : UIView

@property (nonatomic, strong) UIColor *strokeColor;

- (void)beginAnimation;
- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
