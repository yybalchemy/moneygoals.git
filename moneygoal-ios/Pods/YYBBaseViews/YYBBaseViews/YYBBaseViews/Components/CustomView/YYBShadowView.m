//
//  YYBShadowView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/10/7.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBShadowView.h"

@implementation YYBShadowView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _shadowView = [UIView createViewAtSuperView:self constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    } configureBlock:nil];
    
    _contentView = [UIView createViewAtSuperView:self.shadowView constraintBlock:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.shadowView);
    } configureBlock:^(UIView *view) {
        view.backgroundColor = [UIColor whiteColor];
    }];
    
    return self;
}

@end
