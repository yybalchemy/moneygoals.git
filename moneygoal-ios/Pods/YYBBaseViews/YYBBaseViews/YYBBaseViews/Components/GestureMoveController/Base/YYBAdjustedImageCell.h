//
//  YYBAdjustedImageCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBAdjustedImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBAdjustedImageCell : UICollectionViewCell

@property (nonatomic, strong) YYBAdjustedImageView *iconView;

@end

NS_ASSUME_NONNULL_END
