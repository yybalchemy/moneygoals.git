//
//  YYBAdjustedImageModel.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYBAdjustedImageModel : NSObject

@property (nonatomic, strong) UIImage *placeholderImage;

@property (nonatomic, copy) NSString *imageURL;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) PHAsset *imageAsset;

@property (nonatomic) CGRect beginRect;
@property (nonatomic) CGRect endRect;
@property (nonatomic) CGRect movedEndRect;

@end

NS_ASSUME_NONNULL_END
