//
//  YYBAdjustedImageView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBAdjustedImageModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YYBImageAdjustedType)
{
    YYBImageAdjustedTypeNone,
    YYBImageAdjustedTypeAspectFill,
    YYBImageAdjustedTypeAspectFit
};

@interface YYBAdjustedImageView : UIView

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic) YYBImageAdjustedType aspectMode;

@property (nonatomic, strong) YYBAdjustedImageModel *dataModel;

@end

NS_ASSUME_NONNULL_END
