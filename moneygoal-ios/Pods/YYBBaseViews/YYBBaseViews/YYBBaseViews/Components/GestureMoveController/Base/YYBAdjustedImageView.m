//
//  YYBAdjustedImageView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import "YYBAdjustedImageView.h"

@implementation YYBAdjustedImageView

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.clipsToBounds = YES;
    
    _aspectMode = YYBImageAdjustedTypeAspectFill;
    
    _imageView = [[UIImageView alloc] init];
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    [self addSubview:_imageView];
    
    return self;
}

- (void)setDataModel:(YYBAdjustedImageModel *)dataModel {
    _dataModel = dataModel;
    
    if (dataModel.imageURL) {
        NSURL *imageURL = [NSURL URLWithString:dataModel.imageURL];
        [_imageView sd_setImageWithURL:imageURL placeholderImage:dataModel.placeholderImage completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [self refreshImageViewLayout];
        }];
    } else if (dataModel.image) {
        _imageView.image = dataModel.image;
        [self refreshImageViewLayout];
    } else if (dataModel.imageAsset) {
        CGSize targetSize = CGSizeMake(dataModel.imageAsset.pixelWidth, dataModel.imageAsset.pixelHeight);
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.networkAccessAllowed = YES;
        
        __weak typeof(self) weakself = self;
        [[PHImageManager defaultManager] requestImageForAsset:dataModel.imageAsset targetSize:targetSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            
            BOOL finished = ![[info objectForKey:PHImageCancelledKey] boolValue] &&
            ![info objectForKey:PHImageErrorKey] &&
            ![[info objectForKey:PHImageResultIsDegradedKey] boolValue] &&
            result;
            
            if (finished) {
                weakself.imageView.image = result;
                [self refreshImageViewLayout];
            }
        }];
    }
}

- (void)refreshImageViewLayout {
    if (_aspectMode == YYBImageAdjustedTypeNone) return;
    
    CGFloat contentWidth = self.frame.size.width;
    CGFloat contentHeight = self.frame.size.height;
    
    if (CGSizeEqualToSize(CGSizeZero, self.frame.size) || !_imageView.image) {
        _imageView.frame = CGRectZero;
        return;
    } else {
        CGSize size = CGSizeZero;
        if (_aspectMode == YYBImageAdjustedTypeAspectFit) {
            size = [self aspectedFitImageSizeWithContentSize:self.frame.size];
        } else if (_aspectMode == YYBImageAdjustedTypeAspectFill) {
            size = [self aspectedImageSizeWithContentSize:self.frame.size];
        }
        
        _imageView.frame = CGRectMake((contentWidth - size.width) / 2, (contentHeight - size.height) / 2,
                                      size.width, size.height);
    }
    
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    if (CGRectEqualToRect(CGRectZero, _dataModel.beginRect)) {
        _dataModel.beginRect = [self.superview convertRect:self.frame toView:keyWindow];
    }
    
    if (CGRectEqualToRect(CGRectZero, _dataModel.endRect)) {
        CGSize endSize = [self aspectedFitImageSizeWithContentSize:keyWindow.frame.size];
        CGFloat keyWindowWidth = keyWindow.frame.size.width;
        CGFloat keyWindowHeight = keyWindow.frame.size.height;
        
        _dataModel.endRect = CGRectMake((keyWindowWidth - endSize.width) / 2, (keyWindowHeight - endSize.height) / 2,
                                        endSize.width, endSize.height);
    }
}

// aspect fill
- (CGSize)aspectedImageSizeWithContentSize:(CGSize)contentSize {
    CGFloat imageWidth = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
    
    CGFloat contentWidth = contentSize.width;
    CGFloat contentHeight = contentSize.height;
    
    CGFloat imageRatio = imageWidth / imageHeight;
    CGFloat contentRatio = contentWidth / contentHeight;
    
    if (imageRatio > contentRatio) {
        imageHeight = contentHeight;
        imageWidth = imageRatio * imageHeight;
    } else {
        imageWidth = contentWidth;
        imageHeight = imageWidth / imageRatio;
    }
    
    return CGSizeMake(imageWidth, imageHeight);
}

// aspect fit
- (CGSize)aspectedFitImageSizeWithContentSize:(CGSize)contentSize {
    CGFloat imageWidth = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
    
    CGFloat contentWidth = contentSize.width;
    CGFloat contentHeight = contentSize.height;
    
    CGFloat imageRatio = imageWidth / imageHeight;
    CGFloat contentRatio = contentWidth / contentHeight;
    
    if (imageRatio > contentRatio) {
        imageWidth = contentWidth;
        imageHeight = imageWidth / imageRatio;
    } else {
        imageHeight = contentHeight;
        imageWidth = imageRatio * imageHeight;
    }
    
    return CGSizeMake(imageWidth, imageHeight);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self refreshImageViewLayout];
}

@end
