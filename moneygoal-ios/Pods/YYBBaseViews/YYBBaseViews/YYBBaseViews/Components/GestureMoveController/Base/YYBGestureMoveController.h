//
//  YYBGestureMoveController.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewController.h"
#import "YYBGestureMoveTransition.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBGestureMoveController : YYBViewController

@property (nonatomic, strong, readonly) UIView *containerView;
@property (nonatomic, strong) YYBGestureMoveTransition *transition;

@property (nonatomic, strong) YYBAdjustedImageModel *placeholderImageModel;
// 手势移动的时候，显示的视图
@property (nonatomic, strong) YYBAdjustedImageView *adjustedImageView;

@property (nonatomic) BOOL invalidPanGesture;
- (void)handlePanGestureBegin;
- (void)handlePanGestureChangedWithScale:(CGFloat)scale offset:(CGPoint)offset;
- (void)handlePanGestureEndWithScale:(CGFloat)scale;
- (void)handlePanGestureEndAnimation;

@end

NS_ASSUME_NONNULL_END
