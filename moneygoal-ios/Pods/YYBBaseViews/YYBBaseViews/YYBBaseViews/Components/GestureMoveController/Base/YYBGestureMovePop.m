//
//  YYBGestureMovePop.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMovePop.h"

@interface YYBGestureMovePop ()
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) YYBAdjustedImageView *adjustedImageView;

@end

@implementation YYBGestureMovePop

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _adjustedImageView = [[YYBAdjustedImageView alloc] init];
    _adjustedImageView.aspectMode = YYBImageAdjustedTypeNone;
    
    _backgroundView = [[UIView alloc] init];
    _backgroundView.backgroundColor = [UIColor blackColor];
    
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIView *contentView = transitionContext.containerView;
    
    UIViewController *targetView = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [contentView addSubview:targetView.view];
    
    _backgroundView.frame = contentView.bounds;
    _backgroundView.alpha = 1;
    [contentView addSubview:_backgroundView];
    
    _adjustedImageView.frame = targetView.view.bounds;
    _adjustedImageView.imageView.frame = _imageModel.endRect;
    _adjustedImageView.dataModel = _imageModel;
    [contentView addSubview:_adjustedImageView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        self.backgroundView.alpha = 0;
        self.adjustedImageView.imageView.frame = self.imageModel.beginRect;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.backgroundView removeFromSuperview];
            [self.adjustedImageView removeFromSuperview];
            
            [transitionContext completeTransition:YES];
        }
    }];
}

@end
