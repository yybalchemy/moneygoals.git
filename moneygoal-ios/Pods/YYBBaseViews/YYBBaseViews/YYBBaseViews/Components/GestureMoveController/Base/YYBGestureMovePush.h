//
//  YYBGestureMovePush.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYBAdjustedImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBGestureMovePush : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, strong) YYBAdjustedImageModel *imageModel;

@end

NS_ASSUME_NONNULL_END
