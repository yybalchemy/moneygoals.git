//
//  YYBGestureMoveTransition.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/7.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBGestureMoveTransition.h"

@interface YYBGestureMoveTransition ()
@property (nonatomic,strong) YYBGestureMovePop *pop;
@property (nonatomic,strong) YYBGestureMovePush *push;
@property (nonatomic,strong) YYBGestureMovePercentTransition *transition;

@end

@implementation YYBGestureMoveTransition

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    _push = [[YYBGestureMovePush alloc] init];
    _pop = [[YYBGestureMovePop alloc] init];
    _transition = [[YYBGestureMovePercentTransition alloc] init];
    
    return self;
}

- (void)setPan:(UIPanGestureRecognizer *)pan {
    _pan = pan;
    _transition.pan = pan;
}

- (void)setImageModel:(YYBAdjustedImageModel *)imageModel {
    _imageModel = imageModel;
    
    _push.imageModel = imageModel;
    _pop.imageModel = imageModel;
    _transition.imageModel = imageModel;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return _push;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return _pop;
}

- (nullable id <UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id <UIViewControllerAnimatedTransitioning>)animator {
    return nil;
}

- (nullable id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator {
    if (_pan) {
        return _transition;
    } else {
        return nil;
    }
}

@end
