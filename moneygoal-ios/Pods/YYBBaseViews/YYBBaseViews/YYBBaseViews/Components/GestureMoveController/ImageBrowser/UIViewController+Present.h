//
//  UIViewController+YYBImageBrowserController.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/11/10.
//  Copyright © 2020 Univease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBImageBrowserController.h"
#import "YYBImageTapedBrowserController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Present)

- (void)showImageBrowserWithImageModels:(NSMutableArray *)imageModels selectedIndex:(NSInteger)selectedIndex;

// 展示一张图片
- (void)showImageBrowserWithImagePath:(NSString *)imagePath;

@end

NS_ASSUME_NONNULL_END
