//
//  YYBImageBrowserCollectionViewCell.h
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/8.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "YYBAdjustedImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBImageBrowserCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) YYBAdjustedImageView *contentIconView;

@end

NS_ASSUME_NONNULL_END
