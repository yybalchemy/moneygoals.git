//
//  YYBImageBrowserCollectionViewCell.m
//  Taotao-iOS
//
//  Created by alchemy on 2020/10/8.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBImageBrowserCollectionViewCell.h"

@interface YYBImageBrowserCollectionViewCell () <UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation YYBImageBrowserCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) return nil;
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    _scrollView.delegate = self;
    _scrollView.contentSize = frame.size;
    [self.contentView addSubview:_scrollView];
    
    _scrollView.maximumZoomScale = 3;
    _scrollView.minimumZoomScale = 1;
    
    _contentIconView = [[YYBAdjustedImageView alloc] init];
    _contentIconView.frame = _scrollView.bounds;
    _contentIconView.aspectMode = YYBImageAdjustedTypeAspectFit;
    [self.scrollView addSubview:_contentIconView];
    
    return self;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _contentIconView;
}

@end
