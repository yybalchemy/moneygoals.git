//
//  YYBImageTapedBrowserController.h
//  YYBBaseViews
//
//  Created by alchemy on 2021/1/10.
//  Copyright © 2021 Univease Co., Ltd. All rights reserved.
//

#import "YYBImageBrowserController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBImageTapedBrowserController : YYBImageBrowserController

@end

NS_ASSUME_NONNULL_END
