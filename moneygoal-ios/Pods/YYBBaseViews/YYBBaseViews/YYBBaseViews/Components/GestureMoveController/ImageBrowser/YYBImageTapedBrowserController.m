//
//  YYBImageTapedBrowserController.m
//  YYBBaseViews
//
//  Created by alchemy on 2021/1/10.
//  Copyright © 2021 Univease Co., Ltd. All rights reserved.
//

#import "YYBImageTapedBrowserController.h"

@interface YYBImageTapedBrowserController ()

@end

@implementation YYBImageTapedBrowserController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)beforeConfigViewAction {
    [super beforeConfigViewAction];
    
    UITapGestureRecognizer *taped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapAction)];
    [self.view addGestureRecognizer:taped];
}

- (void)handleTapAction {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
