//
//  YYBBaseViews.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBar.h"

@interface YYBNavigationBar ()

@end

@implementation YYBNavigationBar

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
//    self.clipsToBounds = YES;
    
    _heightForBottomView = 0.5f;
    _leftBarContainers = [NSMutableArray new];
    _rightBarContainers = [NSMutableArray new];
    
    _shadowView = [UIView new];
    _shadowView.backgroundColor = [UIColor clearColor];
    [self addSubview:_shadowView];
    
    _contentView = [UIImageView new];
    [_shadowView addSubview:_contentView];
    
    _containerView = [UIView new];
    [_shadowView addSubview:_containerView];
    
    _titleBarLabel = [YYBNavigationBarLabel new];
    _titleBarLabel.label.textAlignment = NSTextAlignmentCenter;
    [_containerView addSubview:_titleBarLabel];
    
    _bottomLayerView = [UIView new];
    [self addSubview:_bottomLayerView];
    
    return self;
}

- (void)setLeftBarContainers:(NSArray *)leftBarContainers
{
    _leftBarContainers = leftBarContainers;
    [self setNeedsLayout];
}

- (void)setRightBarContainers:(NSArray *)rightBarContainers
{
    _rightBarContainers = rightBarContainers;
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGFloat height = self.frame.size.height;
    CGFloat width = self.frame.size.width;
    CGFloat height_container = height - _contentViewTopEdgeInset;
    
    _shadowView.frame = self.bounds;
    _contentView.frame = _shadowView.bounds;
    _containerView.frame = CGRectMake(0, _contentViewTopEdgeInset, width, height_container);
    _bottomLayerView.frame = CGRectMake(0, CGRectGetHeight(self.frame) - _heightForBottomView, CGRectGetWidth(self.frame), _heightForBottomView);
    
    if (_titleCustomView)
    {
        _titleBarLabel.frame = CGRectZero;
        [_containerView addSubview:_titleCustomView];
        
        CGSize contentSize = _titleCustomView.frame.size;
        
        CGFloat x = (width - contentSize.width) / 2;
        CGFloat y = (height_container - contentSize.height) / 2;
        _titleCustomView.frame = CGRectMake(x, y, contentSize.width, contentSize.height);
    }
    else
    {
        if (_titleCustomView)
        {
            [_titleCustomView removeFromSuperview];
        }
        
        CGFloat x = _titleBarLabel.contentEdgeInsets.left - _titleBarLabel.contentEdgeInsets.right;
        CGFloat y = _titleBarLabel.contentEdgeInsets.top - _titleBarLabel.contentEdgeInsets.bottom;
        _titleBarLabel.frame = CGRectMake(x, y, width, height_container);
    }
    
    for (UIView *view in _containerView.subviews)
    {
        if (view != _titleCustomView && view != _titleBarLabel)
        {
            [view removeFromSuperview];
        }
    }
    
    CGFloat max_left_width = 0;
    for (NSInteger index = 0; index < _leftBarContainers.count; index ++)
    {
        YYBNavigationBarContainer *view = [_leftBarContainers objectAtIndex:index];
        CGSize contentSize = view.contentSize;
        if (contentSize.height == 0)
        {
            contentSize.height = height_container;
        }
        
        CGFloat x = max_left_width + view.contentEdgeInsets.left + view.contentEdgeInsets.right;
        CGFloat y = (height_container - contentSize.height) / 2 + view.contentEdgeInsets.top - view.contentEdgeInsets.bottom;
        view.frame = CGRectMake(x, y, contentSize.width, contentSize.height);
        [_containerView addSubview:view];
        
        max_left_width += contentSize.width + view.contentEdgeInsets.left + view.contentEdgeInsets.right;
    }
    
    CGFloat max_right_width = 0;
    for (NSInteger index = 0; index < _rightBarContainers.count; index ++)
    {
        YYBNavigationBarContainer *view = [_rightBarContainers objectAtIndex:index];
        CGSize contentSize = view.contentSize;
        if (contentSize.height == 0)
        {
            contentSize.height = height;
        }
        
        CGFloat x = width - max_right_width - contentSize.width - view.contentEdgeInsets.left - view.contentEdgeInsets.right;
        CGFloat y = (height_container - contentSize.height) / 2 + view.contentEdgeInsets.top - view.contentEdgeInsets.bottom;
        view.frame = CGRectMake(x, y, contentSize.width, contentSize.height);
        [_containerView addSubview:view];
        
        max_right_width += contentSize.width + view.contentEdgeInsets.left + view.contentEdgeInsets.right;
    }
}

@end
