//
//  YYBNavigationBarBadgeView.m
//  YYBBaseViews
//
//  Created by alchemy on 2020/5/16.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarBadgeView.h"

@implementation YYBNavigationBarBadgeView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    
    _position = YYBNavigationBarBadgePositionCenterTop;

    _badgeValueAttrs = [[NSMutableDictionary alloc] init];
    [_badgeValueAttrs setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [_badgeValueAttrs setObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName];
    
    NSMutableParagraphStyle *graph = [[NSMutableParagraphStyle alloc] init];
    graph.alignment = NSTextAlignmentCenter;
    graph.lineBreakMode = NSLineBreakByTruncatingTail;
    
    [_badgeValueAttrs setObject:graph forKey:NSParagraphStyleAttributeName];
    
    _badgeMargin = UIEdgeInsetsMake(6, 15, 0, 0);
    _badgePadding = UIEdgeInsetsMake(2, 6, 2, 6);
    
    return self;
}

- (void)setBadgeValue:(NSInteger)badgeValue
{
    _badgeValue = badgeValue;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (_badgeValue > 0) {
        NSString *badgeValue = @(_badgeValue).stringValue;
        
        CGSize size = [badgeValue sizeWithAttributes:_badgeValueAttrs];
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGSize contentSize = self.frame.size;
        
        CGFloat x = 0;
        CGFloat y = 0;
        
        if (_position == YYBNavigationBarBadgePositionRight) {
            x = contentSize.width - size.width - _badgeMargin.left - _badgeMargin.right - _badgePadding.left - _badgePadding.right;
            y = _badgeMargin.top;
        } else if (_position == YYBNavigationBarBadgePositionCenter) {
            x = (contentSize.width - size.width) / 2 + _badgeMargin.left;
            y = (contentSize.height - size.height) / 2 + _badgeMargin.top;
        } else if (_position == YYBNavigationBarBadgePositionLeft) {
            x = _badgeMargin.left;
            y = _badgeMargin.top;
        } else if (_position == YYBNavigationBarBadgePositionCenterTop) {
            x = (contentSize.width - size.width) / 2 + _badgeMargin.left;
            y = _badgeMargin.top;
        }
        
        CGRect drawRect = CGRectMake(x, y, size.width, size.height);
        
        CGFloat radius = MAX(size.width + _badgePadding.left + _badgePadding.right,
                             size.height + _badgePadding.top + _badgePadding.bottom) / 2;
        
        if (_badgeValue < 10) {
        
            CGContextAddArc(context, CGRectGetMidX(drawRect), CGRectGetMidY(drawRect) , radius, 0, M_PI * 2, NO);
            
        } else {
            
            CGFloat rounded_x = CGRectGetMinX(drawRect) - _badgePadding.left;
            CGFloat rounded_y = CGRectGetMinY(drawRect) - _badgePadding.top;
            CGFloat rounded_width = drawRect.size.width + _badgePadding.left + _badgePadding.right;
            CGFloat rounded_height = drawRect.size.height + _badgePadding.top + _badgePadding.bottom;
             
            CGRect roundedRect = CGRectMake(rounded_x, rounded_y, rounded_width, rounded_height);
            
            UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:roundedRect cornerRadius:radius];
            CGContextAddPath(context, path.CGPath);

        }
        
        CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
        CGContextFillPath(context);
        
        // 绘制文字
        CGRect textDrawRect = drawRect;
//        textDrawRect.origin.x += _badgePadding.left;
//        textDrawRect.origin.y += _badgePadding.top;
        
        [badgeValue drawInRect:textDrawRect withAttributes:_badgeValueAttrs];
    }
}

@end
