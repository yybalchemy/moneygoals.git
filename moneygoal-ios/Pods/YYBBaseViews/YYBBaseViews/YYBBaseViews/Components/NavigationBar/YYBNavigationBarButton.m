//
//  YYBNavigationBarButton.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarButton.h"

@implementation YYBNavigationBarButton

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _button = [UIButton new];
    [self.contentView addSubview:_button];
    
    [_button addTarget:self action:@selector(barButtonTapedAction) forControlEvents:1<<6];
    
    return self;
}

- (void)barButtonTapedAction
{
    if (self.barButtonTapedBlock) {
        self.barButtonTapedBlock(self);
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = CGRectGetWidth(self.contentView.frame);
    CGFloat height = CGRectGetHeight(self.contentView.frame);
    
    _button.frame = CGRectMake(self.containerPadding.left + self.containerMargin.left - self.containerMargin.right,
                               self.containerPadding.top + self.containerMargin.top - self.containerMargin.bottom,
                               width - self.containerPadding.left - self.containerPadding.right,
                               height - self.containerPadding.top - self.containerPadding.bottom);
}

@end
