//
//  YYBNavigationBarContainer+Create.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer+Create.h"

@implementation YYBNavigationBarContainer (Create)

+ (YYBNavigationBarLabel *)labelWithConfigureBlock:(void (^)(YYBNavigationBarLabel *, UILabel *))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *))tapedActionBlock
{
    YYBNavigationBarLabel *view = [[YYBNavigationBarLabel alloc] init];
    if (configureBlock)
    {
        configureBlock(view,view.label);
    }
    view.tapedActionBlock = tapedActionBlock;
    return view;
}

+ (YYBNavigationBarImageView *)imageViewWithConfigureBlock:(void (^)(YYBNavigationBarImageView *, UIImageView *))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *))tapedActionBlock
{
    YYBNavigationBarImageView *view = [[YYBNavigationBarImageView alloc] init];
    if (configureBlock)
    {
        configureBlock(view,view.imageView);
    }
    view.tapedActionBlock = tapedActionBlock;
    return view;
}

+ (YYBNavigationBarButton *)buttonWithConfigureBlock:(void (^)(YYBNavigationBarButton *, UIButton *))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *))tapedActionBlock
{
    YYBNavigationBarButton *view = [[YYBNavigationBarButton alloc] init];
    if (configureBlock)
    {
        configureBlock(view,view.button);
    }
    view.tapedActionBlock = tapedActionBlock;
    return view;
}

+ (YYBNavigationBarControl *)controlWithConfigureBlock:(void (^)(YYBNavigationBarControl *, UIImageView *, UILabel *))configureBlock tapedActionBlock:(void (^)(YYBNavigationBarContainer *))tapedActionBlock
{
    YYBNavigationBarControl *view = [[YYBNavigationBarControl alloc] init];
    if (configureBlock)
    {
        configureBlock(view,view.iconView,view.label);
    }
    view.tapedActionBlock = tapedActionBlock;
    return view;
}

@end
