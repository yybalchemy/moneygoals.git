//
//  YYBNavigationBarContainer.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer.h"

@implementation YYBNavigationBarContainer

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor clearColor];
    [self addTarget:self action:@selector(tapedAction) forControlEvents:1<<6];
    
    _contentView = [[UIView alloc] init];
    _contentView.userInteractionEnabled = NO;
    [self addSubview:_contentView];
    
    _badgeView = [[YYBNavigationBarBadgeView alloc] init];
    _badgeView.userInteractionEnabled = NO;
    [self addSubview:_badgeView];

    return self;
}

- (void)tapedAction
{
    if (self.tapedActionBlock) {
        self.tapedActionBlock(self);
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    _contentView.frame = CGRectMake(_contentPadding.left + _contentMargin.left - _contentMargin.right,
                                    _contentPadding.top + _contentMargin.top + -_contentMargin.bottom,
                                    width - _contentPadding.left - _contentPadding.right,
                                    height - _contentPadding.top - _contentPadding.bottom);
    _badgeView.frame = self.bounds;
}

@end
