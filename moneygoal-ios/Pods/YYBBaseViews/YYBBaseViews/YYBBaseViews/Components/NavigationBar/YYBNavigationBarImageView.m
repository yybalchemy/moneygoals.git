//
//  YYBNavigationBarImageView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarImageView.h"

@implementation YYBNavigationBarImageView

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _imageView = [UIImageView new];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.contentView addSubview:_imageView];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = CGRectGetWidth(self.contentView.frame);
    CGFloat height = CGRectGetHeight(self.contentView.frame);
    
    _imageView.frame = CGRectMake(self.containerPadding.left + self.containerMargin.left - self.containerMargin.right,
                                  self.containerPadding.top + self.containerMargin.top - self.containerMargin.bottom,
                                  width - self.containerPadding.left - self.containerPadding.right,
                                  height - self.containerPadding.top - self.containerPadding.bottom);
}

@end
