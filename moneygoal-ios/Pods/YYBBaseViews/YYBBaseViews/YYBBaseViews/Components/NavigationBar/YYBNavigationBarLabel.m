//
//  YYBNavigationBarLabel.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/28.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarLabel.h"

@implementation YYBNavigationBarLabel

- (CGSize)contentSize
{
    [_label sizeToFit];
    
    CGSize labelSize = _label.frame.size;
    return CGSizeMake(self.contentPadding.left + self.contentPadding.right + labelSize.width,
                      self.contentPadding.top + self.contentPadding.bottom + labelSize.height);
}

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _label = [UILabel new];
    [self.contentView addSubview:_label];
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize contentSize = [self contentSize];
    
    CGFloat width = CGRectGetWidth(self.contentView.frame);
    CGFloat height = CGRectGetHeight(self.contentView.frame);

    _label.frame = CGRectMake((width - contentSize.width) / 2 + self.containerMargin.left - self.containerMargin.right,
                              (height - contentSize.height) / 2 + self.containerMargin.top - self.containerMargin.bottom,
                              contentSize.width - self.containerPadding.left - self.containerPadding.right,
                              contentSize.height - self.containerPadding.top - self.containerPadding.bottom);
}

@end
