//
//  YYBPermission.h
//  YYBBaseViews
//
//  Created by alchemy on 2019/4/2.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "YYBCategory.h"
#import "YYBAlertView+YYBCommon.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBPermission : NSObject

// 相册权限
+ (void)photoPermissionWithCompleteBlock:(void(^)(void))completeBlock;

// 生物识别权限
+ (void)permissionUserUnLock;
+ (void)getUserUnLockPermissionWithCompleteBlock:(void (^)(BOOL success, NSError * _Nullable error))completionBlock;

@end

NS_ASSUME_NONNULL_END
