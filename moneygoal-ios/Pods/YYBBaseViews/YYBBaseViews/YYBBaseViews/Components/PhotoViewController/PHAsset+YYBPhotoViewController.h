//
//  PHAsset+YYBPhoto.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <Photos/Photos.h>
#import <CoreServices/CoreServices.h>

@interface PHAsset (YYBPhotoViewController)

// 获取相册的缩略图
- (void)thumbnailImageWithTargetSize:(CGSize)targetSize completionBlock:(void (^)(UIImage *thumbnailImage))completionBlock;

// 获取完成的图像
- (void)queryImageWithCompletionBlock:(void (^)(UIImage *targetImage))completionBlock;

@end
