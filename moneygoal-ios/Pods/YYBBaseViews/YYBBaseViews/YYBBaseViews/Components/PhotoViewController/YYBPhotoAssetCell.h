//
//  YYBPhotoAssetCell.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <Masonry/Masonry.h>
#import "YYBCategory.h"
#import "PHAsset+YYBPhotoViewController.h"

@protocol YYBPhotoAssetCellDelegate <NSObject>

- (NSInteger)selectedIndexWithAsset:(PHAsset *)asset;
- (BOOL)isAppendPhotoAssetEnableWithAsset:(PHAsset *)asset;
- (BOOL)isAppendVideoAssetEnableWithAsset:(PHAsset *)asset;

- (void)didSelectedAssetWithAsset:(PHAsset *)asset;

// 是否可以选择多张图片 比如选择头像仅仅只能选择一张图片，这个时候需要隐藏选择按钮
- (BOOL)isAppendMultiAssetsEnable;

@end

@interface YYBPhotoAssetCell : UICollectionViewCell

@property (nonatomic, weak) id<YYBPhotoAssetCellDelegate> delegate;

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UIButton *selectedStatusButton;

@property (nonatomic, strong) CAGradientLayer *videoBackgroundLayer;
@property (nonatomic, strong) UIButton *videoImageButton;
// 不能添加更多了的 遮罩
@property (nonatomic, strong) UIView *bannedBackgroundView;

// ========================
- (void)refreshViewWithAsset:(PHAsset *)asset;

@property (nonatomic, strong) UIImage *assetImage;
@property (nonatomic, strong) PHAsset *asset;


@end
