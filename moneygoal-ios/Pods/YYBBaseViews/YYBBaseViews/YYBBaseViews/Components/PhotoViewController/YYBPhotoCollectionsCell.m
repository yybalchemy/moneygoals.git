//
//  YYBPhotoCollectionsCell.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBPhotoCollectionsCell.h"

@interface YYBPhotoCollectionsCell ()

@end

@implementation YYBPhotoCollectionsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _iconView = [UIImageView createAtSuperView:self.contentView iconName:nil constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(12);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    } configureBlock:^(UIImageView *view) {
        view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds = YES;
    }];
    
    _albumNameLabel = [UILabel createAtSuperView:self.contentView fontValue:[UIFont systemFontOfSize:16] textColor:[UIColor blackColor] constraintBlock:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconView.mas_right).offset(10);
        make.centerY.equalTo(self.iconView);
    } configureBlock:nil];
    
    return self;
}

- (void)setCollection:(PHAssetCollection *)collection
{
    _collection = collection;
    
    PHFetchOptions *option = [[PHFetchOptions alloc] init];
    option.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    PHFetchResult *result = [PHAsset fetchAssetsInAssetCollection:collection options:option];
    
    _albumNameLabel.text = [NSString stringWithFormat:@"%@ (%@)",collection.localizedTitle,@(result.count).stringValue];
    
    if (result.count > 0) {
        @weakify(self);
        PHAsset *asset = [result firstObject];
        [asset thumbnailImageWithTargetSize:CGSizeMake(80, 80) completionBlock:^(UIImage *thumbnailImage) {
            @strongify(self);
            self.iconView.image = thumbnailImage;
        }];
    } else {
        self.iconView.image = nil;
    }
}

@end
