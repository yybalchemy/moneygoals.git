//
//  YYBPhotoNavigationSelectionView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBNavigationBarContainer.h"
#import <Masonry/Masonry.h>
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

@protocol YYBPhotoToolbarViewDelegate <NSObject>

// status = YES 表示展开
- (void)photoSelectionViewWithStatus:(BOOL)status;

@end

@interface YYBPhotoNavigationSelectionView : YYBNavigationBarContainer

@property (nonatomic, weak) id <YYBPhotoToolbarViewDelegate> delegate;

@property (nonatomic, strong) UIImageView *switchIconView;
@property (nonatomic, strong) UILabel *collectionTextLabel;

- (CGSize)preferSizeWithCollectionTextValue:(NSString *)textValue;
- (void)handlePhotoSelectionStatus;

@end

NS_ASSUME_NONNULL_END
