//
//  YYBPhotoToolbarView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/11/3.
//  Copyright © 2018 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import <ReactiveObjC/RACEXTScope.h>
#import "YYBCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBPhotoToolbarView : UIView

@property (nonatomic,strong) UIButton *finishButton;
- (void)refreshViewWithImagesCount:(NSInteger)count;

@property (nonatomic, copy) void (^ finishSelectedBlock)(void);

@end

NS_ASSUME_NONNULL_END
