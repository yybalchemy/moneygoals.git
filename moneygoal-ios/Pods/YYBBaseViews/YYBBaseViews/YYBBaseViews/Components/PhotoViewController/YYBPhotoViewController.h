//
//  YYBPhotoViewController.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/9/27.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBCollectionViewController.h"
#import <Photos/Photos.h>
#import <ReactiveObjC/RACEXTScope.h>
#import "PHAsset+YYBPhotoViewController.h"
#import "UIViewController+Present.h"
#import "YYBAlertView+YYBCommon.h"
#import "YYBCategory.h"

typedef NS_ENUM(NSInteger, YYBPhotoViewSelectionStyle)
{
    YYBPhotoViewSelectionStyleOnlyOneAsset,
    YYBPhotoViewSelectionStyleMultiAssets,
};

extern NSString * const YYBPhotoViewSelectNotification;

@class YYBPhotoViewController;

@protocol YYBPhotoViewControllerDelegate <NSObject>
 
// assets 可能是UIImage 也可能是 PHAsset
- (void)photoView:(YYBPhotoViewController *)photoView didCompleteFetchAssets:(NSArray *)assets originalAssets:(NSArray *)originalAssets;

- (void)photoView:(YYBPhotoViewController *)photoView appendAsset:(PHAsset *)asset;
- (void)photoView:(YYBPhotoViewController *)photoView removeAsset:(PHAsset *)asset;

// 该asset是否可以被添加
- (BOOL)photoView:(YYBPhotoViewController *)photoView appendEnableWithAsset:(PHAsset *)asset;
- (void)photoView:(YYBPhotoViewController *)photoView previewAsset:(PHAsset *)asset index:(NSInteger)index;

@end

@interface YYBPhotoViewController : YYBCollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) id<YYBPhotoViewControllerDelegate> delegate;
@property (nonatomic) YYBPhotoViewSelectionStyle selectionStyle;

@property (nonatomic, strong, readonly) NSMutableArray *selectedAssets;

@property (nonatomic, strong, readonly) NSMutableArray *assetCollections;
@property (nonatomic, strong, readonly) PHFetchResult *fetchResult;
@property (nonatomic, strong, readonly) PHAssetCollection *assetCollection;

// 最大可允许选择的图片数量
// 默认为9
@property (nonatomic) NSInteger maxSelectAssetsCount;

// 是否最后得到的是UIImage, 否则输出的是PHAsset
@property (nonatomic) BOOL isProduceAsUIImage;

// 最初选择的数据类型
// 默认是 PHAssetCollectionSubtypeSmartAlbumUserLibrary
@property (nonatomic) PHAssetCollectionSubtype requiredSubtype;

@property (nonatomic, strong) NSMutableArray *initialSelectedAssets;

// Custom 定制方法
- (Class)customPhotoItemCellClass;
- (YYBAlertView *)customPhotoProcessAlertView;

// 获取图像资源
- (void)queryPhotoCollectionsAction;

@end
