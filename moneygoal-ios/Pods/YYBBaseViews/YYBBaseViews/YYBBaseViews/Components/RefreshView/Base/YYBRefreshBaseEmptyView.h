//
//  YYBRefreshBaseEmptyView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/3.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBRefreshBaseEmptyView : UIView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView;

@property (nonatomic, strong, readonly) UIScrollView *scrollView;
@property (nonatomic) UIEdgeInsets scrollEdgeInsets;

- (void)renderInitialEdgeInsets;
- (void)renderAnimationEdgeInsets;

@end
