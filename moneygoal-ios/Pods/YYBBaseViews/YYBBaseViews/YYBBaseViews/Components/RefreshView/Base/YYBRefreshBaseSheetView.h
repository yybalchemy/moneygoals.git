//
//  YYBRefreshBaseSheetView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseView.h"
#import "UIDevice+YYBAdd.h"
#import <Masonry/Masonry.h>

@interface YYBRefreshBaseSheetView : YYBRefreshBaseView

// 是否自动控制加载的方式,一般方式为如果拉动距离超过最大内容高度,就自动加载数据
// 此时不需要松手
// 经典方式为手动拉过这段距离放开后才会被夹在
@property (nonatomic) BOOL autoLoadData;

@end
