//
//  YYBRefreshBaseSheetView.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseSheetView.h"

@implementation YYBRefreshBaseSheetView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView
{
    self = [super initWithScrollView:scrollView];
    _autoLoadData = YES;
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentOffset"])
    {
        if (self.status != YYBRefreshStatusRefreshing)
        {
            CGFloat offset = [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue].y;
            CGFloat state1 = [self stateInitToStatePull];
            CGFloat state2 = [self statePullToStateRefresh];
            self.offset = offset;
            
            if (offset < state1)
            {
                self.status = YYBRefreshStatusInitial;
                self.percent = 0;
            }
            else if (offset >= state1 && offset < state2)
            {
                self.status = YYBRefreshStatusInitial;
                self.percent = (offset - state1) / CGRectGetHeight(self.frame);
            }
            else if (offset > state2)
            {
                self.percent = 1;
                if (self.autoLoadData == YES && self.scrollView.isDragging) {
                    self.status = YYBRefreshStatusRefreshing;
                } else {
                    if (self.scrollView.isDragging) {
                        self.status = YYBRefreshStatusPulling;
                    } else {
                        if (self.status == YYBRefreshStatusPulling) {
                            self.status = YYBRefreshStatusRefreshing;
                        }
                    }
                }
            }
            
            if (self.isAllowHandleOffset && [self respondsToSelector:@selector(offsetDidChanged:percent:)])
            {
                [self offsetDidChanged:self.offset percent:self.percent];
            }
        }
    }
    else if ([keyPath isEqualToString:@"contentSize"])
    {
        CGFloat insetOffset = self.scrollView.contentInset.bottom - self.scrollEdgeInsets.bottom;
        if (insetOffset > 0) {
            UIEdgeInsets changeEdgeInsets = self.scrollView.contentInset;
            changeEdgeInsets.bottom = self.scrollEdgeInsets.bottom;
            
            self.scrollView.contentInset = changeEdgeInsets;
        } else {
            CGFloat contentHeight = self.scrollView.contentSize.height;
            CGFloat height = CGRectGetHeight(self.scrollView.frame);

            // 如果内容视图的高度比tableView的高度还小, 则隐藏
            self.hidden = contentHeight < height && self.status == YYBRefreshStatusInitial;
            UIEdgeInsets edgeInsets = self.scrollView.contentInset;
            
            CGFloat width = CGRectGetWidth(self.scrollView.frame) - edgeInsets.left - edgeInsets.right;
            self.frame = CGRectMake(0, contentHeight, width, CGRectGetHeight(self.frame));
        }
    }
}

- (CGFloat)stateInitToStatePull
{
    CGFloat scrollc = self.scrollView.contentSize.height;
    CGFloat scrollh = CGRectGetHeight(self.scrollView.frame);
    if (scrollc < scrollh) return self.scrollEdgeInsets.top;
    return scrollc - scrollh;
}

- (CGFloat)statePullToStateRefresh
{
    return [self stateInitToStatePull] + CGRectGetHeight(self.frame);
}

- (void)renderInitialEdgeInsets
{
    if (self.refreshType == YYBRefreshTypeChangeInset) {
        UIEdgeInsets edgeInsets = self.scrollView.contentInset;
//        edgeInsets.bottom = self.scrollEdgeInsets.bottom;
        [self.scrollView setContentInset:edgeInsets];
        
        NSLog(@"111111");
    } else if (self.refreshType == YYBRefreshTypeStatic) {
        self.hidden = YES;
    }
}

- (void)renderRefreshingStateEdgeInsets
{
    if (self.refreshType == YYBRefreshTypeChangeInset) {
        UIEdgeInsets edgeInsets = self.scrollView.contentInset;
        edgeInsets.bottom += CGRectGetHeight(self.frame);
        [self.scrollView setContentInset:edgeInsets];
    } else if (self.refreshType == YYBRefreshTypeStatic) {
        self.hidden = NO;
    }
}

@end
