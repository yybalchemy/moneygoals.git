//
//  UIScrollView+YYBRefreshTextEmptyView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBRefreshBaseEmptyView.h"

@interface UIScrollView (YYBRefreshEmpty)

@property (nonatomic,strong) YYBRefreshBaseEmptyView *emptyView;
- (void)removeRefreshEmptyView;

- (void)createRefreshEmptyView;
- (void)createRefreshEmptyViewWithClass:(Class)viewClass;
- (void)createRefreshEmptyViewWithClass:(Class)viewClass height:(CGFloat)height;

@end
