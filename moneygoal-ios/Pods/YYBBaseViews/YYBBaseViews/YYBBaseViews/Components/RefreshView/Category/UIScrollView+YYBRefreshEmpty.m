//
//  UIScrollView+YYBRefreshTextEmptyView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIScrollView+YYBRefreshEmpty.h"
#import <objc/runtime.h>

static CGFloat const YYBREFRESH_DONE_HEIGHT = 60;
static char const YYBREFRESH_DONE_KEY;

@implementation UIScrollView (YYBRefreshEmpty)

- (void)createRefreshEmptyView
{
    [self createRefreshEmptyViewWithClass:NSClassFromString(@"YYBRefreshTextEmptyView")];
}

- (void)createRefreshEmptyViewWithClass:(Class)viewClass
{
    [self createRefreshEmptyViewWithClass:viewClass height:YYBREFRESH_DONE_HEIGHT];
}

- (void)createRefreshEmptyViewWithClass:(Class)viewClass height:(CGFloat)height
{
    [self removeRefreshEmptyView];
    
    YYBRefreshBaseEmptyView *view = [[viewClass alloc] initWithScrollView:self];
    UIEdgeInsets edgeInsets = self.contentInset;
    CGFloat width = CGRectGetWidth(self.frame) - edgeInsets.left - edgeInsets.right;
    CGFloat y = self.contentSize.height;
    
    view.frame = CGRectMake(0, y, width, height);
    [self addSubview:view];
    [view renderAnimationEdgeInsets];
    
    self.emptyView = view;
    
    [self addObserver:self.emptyView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setEmptyView:(YYBRefreshBaseEmptyView *)emptyView
{
    objc_setAssociatedObject(self, &YYBREFRESH_DONE_KEY, emptyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YYBRefreshBaseEmptyView *)emptyView
{
    return objc_getAssociatedObject(self, &YYBREFRESH_DONE_KEY);
}

- (void)removeRefreshEmptyView
{
    if (!self.emptyView) return;
    
    [self removeObserver:self.emptyView forKeyPath:@"contentSize"];
    
    [self.emptyView renderInitialEdgeInsets];
    [self.emptyView removeFromSuperview];
    
    objc_setAssociatedObject(self, &YYBREFRESH_DONE_KEY, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
