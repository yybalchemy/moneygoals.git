//
//  UIScrollView+YYBRefreshHeader.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIScrollView+YYBRefreshHeader.h"
#import <objc/runtime.h>

static CGFloat const YYBREFRESH_HEADER_HEIGHT = 60;
static char const YYBREFRESH_HEADER_KEY;

@implementation UIScrollView (YYBRefreshHeader)

- (void)handleCreateHeaderViewWithBlock:(YYBRefreshBeginRefreshBlock)startRefreshBlock
{
    [self handleCreateHeaderViewWithClass:NSClassFromString(@"YYBRefreshRotateHeaderView") block:startRefreshBlock];
}

- (void)handleCreateHeaderViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock
{
    [self handleCreateHeaderViewWithClass:viewClass block:startRefreshBlock height:YYBREFRESH_HEADER_HEIGHT];
}

- (void)handleCreateHeaderViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock height:(CGFloat)height
{
    [self removeRefreshHeaderView];
    
    YYBRefreshBaseHeaderView *view = [[viewClass alloc] initWithScrollView:self];
    
    UIEdgeInsets edgeInsets = self.contentInset;
    view.startRefreshBlock = startRefreshBlock;
    [self addSubview:view];
    
    view.frame = CGRectMake(0, - height, CGRectGetWidth(self.frame) - edgeInsets.left - edgeInsets.right, height);
    
    self.headerView = view;
    
    [self addObserver:self.headerView forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self.headerView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setHeaderView:(YYBRefreshBaseHeaderView *)headerView
{
    objc_setAssociatedObject(self, &YYBREFRESH_HEADER_KEY, headerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YYBRefreshBaseHeaderView *)headerView
{
    return objc_getAssociatedObject(self, &YYBREFRESH_HEADER_KEY);
}

- (void)removeRefreshHeaderView
{
    if (!self.headerView) return;

    self.headerView.status = YYBRefreshStatusInitial;
    self.headerView.startRefreshBlock = nil;
    [self.headerView renderInitialEdgeInsets];
    
    [self removeObserver:self.headerView forKeyPath:@"contentSize"];
    [self removeObserver:self.headerView forKeyPath:@"contentOffset"];
    
    [self.headerView removeFromSuperview];
    
    objc_setAssociatedObject(self, &YYBREFRESH_HEADER_KEY, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
