//
//  UIScrollView+YYBRefreshFooter.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>

#import "YYBRefreshBaseView.h"
#import "YYBRefreshBaseSheetView.h"
#import "YYBCategory.h"

@interface UIScrollView (YYBRefreshSheet)

@property (nonatomic,strong) YYBRefreshBaseSheetView *sheetView;
- (void)removeRefreshSheetView;

- (void)handleCreateSheetViewWithBlock:(YYBRefreshBeginRefreshBlock)startRefreshBlock;
- (void)handleCreateSheetViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock;
- (void)handleCreateSheetViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock height:(CGFloat)height;

@end
