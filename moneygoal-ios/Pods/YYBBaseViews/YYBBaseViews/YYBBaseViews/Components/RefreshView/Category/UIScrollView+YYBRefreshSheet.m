//
//  UIScrollView+YYBRefreshFooter.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "UIScrollView+YYBRefreshSheet.h"
#import <objc/runtime.h>

static CGFloat const YYBREFESH_FOOTER_HEIGHT = 60;
static char const YYBREFRESH_FOOTER_KEY;

@implementation UIScrollView (YYBRefreshSheet)

- (void)handleCreateSheetViewWithBlock:(YYBRefreshBeginRefreshBlock)startRefreshBlock
{
    [self handleCreateSheetViewWithClass:NSClassFromString(@"YYBRefreshRotateSheetView") block:startRefreshBlock];
}

- (void)handleCreateSheetViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock
{
    [self handleCreateSheetViewWithClass:[viewClass class] block:startRefreshBlock height:YYBREFESH_FOOTER_HEIGHT];
}

- (void)handleCreateSheetViewWithClass:(Class)viewClass block:(YYBRefreshBeginRefreshBlock)startRefreshBlock height:(CGFloat)height
{
    [self removeRefreshSheetView];
    
    YYBRefreshBaseSheetView *view = [[viewClass alloc] initWithScrollView:self];
    
    view.startRefreshBlock = startRefreshBlock;
    [self addSubview:view];
    
    UIEdgeInsets edgeInsets = self.contentInset;
    CGFloat width = CGRectGetWidth(self.frame) - edgeInsets.left - edgeInsets.right;
    view.frame = CGRectMake(0, self.contentSize.height, width, height);
    
    self.sheetView = view;
    
    [self addObserver:self.sheetView forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self.sheetView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setSheetView:(YYBRefreshBaseSheetView *)sheetView
{
    objc_setAssociatedObject(self, &YYBREFRESH_FOOTER_KEY, sheetView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YYBRefreshBaseSheetView *)sheetView
{
    return objc_getAssociatedObject(self, &YYBREFRESH_FOOTER_KEY);
}

- (void)removeRefreshSheetView
{
    if (!self.sheetView) return;
    
    self.sheetView.status = YYBRefreshStatusInitial;
    self.sheetView.startRefreshBlock = nil;
    
    [self removeObserver:self.sheetView forKeyPath:@"contentOffset"];
    [self removeObserver:self.sheetView forKeyPath:@"contentSize"];
    
    [self.sheetView renderInitialEdgeInsets];
    [self.sheetView removeFromSuperview];
    
    objc_setAssociatedObject(self, &YYBREFRESH_FOOTER_KEY, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
