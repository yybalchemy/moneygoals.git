//
//  YYBRefreshPopHeaderView.m
//  YYBBaseViews
//
//  Created by Vincin on 2019/5/23.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshPopHeaderView.h"

@implementation YYBRefreshPopHeaderView {
    NSInteger _countingTimeInterval;
    // 该时间段内表示一次完整的动画流程
    NSInteger _maxLoopTimeInterval;
    
    CADisplayLink *_scheduler;
}

// 120帧作为一个loop
// 15帧（1/8）达到最大
// 45帧（3/8）达到最小
// 75帧 (5/8) 达到最大
// 105帧(7/8) 达到最小
- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    _maxLoopTimeInterval = 120;
    _countingTimeInterval = 0;
    
    return self;
}

- (void)statusDidChanged:(YYBRefreshStatus)status {
    switch (status) {
        case YYBRefreshStatusInitial: {
            [_scheduler invalidate];
            _scheduler = nil;
            _countingTimeInterval = 0;
            [self setNeedsDisplay];
        }
            break;
        case YYBRefreshStatusPulling: {
            
        }
            break;
        case YYBRefreshStatusRefreshing: {
            _scheduler = [CADisplayLink displayLinkWithTarget:self selector:@selector(schedulerAction)];
            [_scheduler addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
            _scheduler.preferredFramesPerSecond = 60;
        }
            break;
            
        default:
            break;
    }
}

- (void)schedulerAction {
    _countingTimeInterval ++;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    NSInteger frame = _countingTimeInterval % _maxLoopTimeInterval;
    
    float min = 1;
    float max = 2;
    
    CGSize item_size = CGSizeMake(5, 5);
    
    CGFloat content_width = 40;
    CGFloat startPosition = (width - content_width) / 2;
    NSInteger once_loop = _maxLoopTimeInterval / 2;
    
    NSInteger max_size_in_loop_frame = once_loop / 4;
    NSInteger min_size_in_loop_frame = once_loop / 4 * 3;
    
    NSInteger max_size_in_loop_frame_2 = once_loop / 4 + once_loop;
    NSInteger min_size_in_loop_frame_2 = once_loop / 4 * 3 + once_loop;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextSetLineWidth(context, 0);
    
    NSArray *colors = @[[UIColor colorWithHexValue:0x4F8EF6],
                        [UIColor colorWithHexValue:0xF9D931],
                        [UIColor colorWithHexValue:0xEB5852]];
    
    for (NSInteger index = 0; index < 3; index ++) {
        // 当前这个item所在的帧
        NSInteger item_frame = (frame + index * (_maxLoopTimeInterval / 3)) % _maxLoopTimeInterval;
        // // 一个loop有两个小loop 先到1/4变大 然后到3/4逐渐变小 最后到4/4再变大
        NSInteger frame_once_loop = item_frame % once_loop;
    
        CGFloat size = 0;
        
        if (item_frame < max_size_in_loop_frame) { // 第一次递增
            size = (float)(item_frame + once_loop / 4) / (float)(once_loop / 2) * min + min;
        } else if ((item_frame >= max_size_in_loop_frame && item_frame < min_size_in_loop_frame)) { // 第一次递减
            size = max - (float)(item_frame - max_size_in_loop_frame) / (float)(once_loop / 2) * min;
        } else if (item_frame >= min_size_in_loop_frame && item_frame < max_size_in_loop_frame_2) { // 第二次递增
            size = (float)(item_frame - min_size_in_loop_frame) / (float)(once_loop / 2) * min + min;
        } else if (item_frame >= max_size_in_loop_frame_2 && item_frame <= min_size_in_loop_frame_2) { // 第二次递减
            size = max - (float)(item_frame - max_size_in_loop_frame_2) / (float)(once_loop / 2) * min;
        } else {
            size = (float)(item_frame - min_size_in_loop_frame_2) / (float)(once_loop / 2) * min + min;
        }
        
        CGFloat passed_width = item_frame > frame_once_loop ? (content_width - ((float)(item_frame - once_loop) / (float)once_loop) * content_width) : ((float)item_frame / (float)once_loop) * content_width;
        
        CGFloat x = startPosition - size * item_size.width / 2 + passed_width;
        CGFloat y = (height - size * item_size.height) / 2;
        
        CGContextMoveToPoint(context, x, y);
        CGContextSetFillColorWithColor(context, [(UIColor *)[colors objectAtIndex:index] CGColor]);
        
        CGFloat r = item_size.width / 2 * size;
        CGContextAddArc(context, x + size * item_size.width / 2, height / 2, r, 0, M_PI * 2, NO);
        CGContextDrawPath(context, kCGPathFillStroke);
    }
}

@end
