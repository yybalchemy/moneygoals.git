//
//  YYBRefreshRectHeaderView.h
//  YYBBaseViews
//
//  Created by Vincin on 2019/5/21.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshRectHeaderView : YYBRefreshBaseHeaderView

@end

NS_ASSUME_NONNULL_END
