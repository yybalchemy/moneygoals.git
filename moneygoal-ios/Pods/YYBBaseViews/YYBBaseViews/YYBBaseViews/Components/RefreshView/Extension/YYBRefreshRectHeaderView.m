//
//  YYBRefreshRectHeaderView.m
//  YYBBaseViews
//
//  Created by Vincin on 2019/5/21.
//  Copyright © 2019 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshRectHeaderView.h"

@implementation YYBRefreshRectHeaderView {
    NSInteger _countingTimeInterval;
    // 该时间段内表示一次完整的动画流程
    NSInteger _maxLoopTimeInterval;
    
    CADisplayLink *_scheduler;
}

- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    _maxLoopTimeInterval = 40;
    _countingTimeInterval = _maxLoopTimeInterval;
    
    return self;
}

- (void)statusDidChanged:(YYBRefreshStatus)status {
    switch (status) {
        case YYBRefreshStatusInitial: {
            [_scheduler invalidate];
            _scheduler = nil;
            _countingTimeInterval = 0;
            [self setNeedsDisplay];
        }
            break;
        case YYBRefreshStatusPulling: {
            
        }
            break;
        case YYBRefreshStatusRefreshing: {
            _scheduler = [CADisplayLink displayLinkWithTarget:self selector:@selector(schedulerAction)];
            [_scheduler addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
            _scheduler.preferredFramesPerSecond = 60;
        }
            break;
            
        default:
            break;
    }
}

- (void)schedulerAction {
    _countingTimeInterval ++;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    // 各个方块之间的距离
    CGFloat itemOffset = 4;
    NSInteger itemsCount = 5;
    
    NSInteger dist = _maxLoopTimeInterval / itemsCount;
    
    // 方块的大小
    CGFloat itemSize = 6;
    CGPoint itemStartPosition = CGPointMake((width - itemSize * itemsCount - itemOffset * (itemsCount - 1)) / 2, (height - itemSize) / 2);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextSetLineWidth(context, 0);
    
    for (NSInteger index = 0; index < itemsCount; index ++) {
        CGFloat x = itemStartPosition.x + (itemOffset + itemSize) * index;
        
        CGContextMoveToPoint(context, x, itemStartPosition.y);
        
        CGFloat alpha = (float)((_countingTimeInterval - index * dist) % _maxLoopTimeInterval) / (float)_maxLoopTimeInterval;
        CGContextSetFillColorWithColor(context, [UIColor colorWithHexValue:0x69B2FD alpha:alpha].CGColor);
        
        CGContextAddRect(context, CGRectMake(x, itemStartPosition.y, itemSize, itemSize));
//        CGContextAddArc(context, x + itemSize / 2, itemStartPosition.y + itemSize / 2, itemSize / 2, 0, M_PI * 2, NO);
        
        CGContextDrawPath(context, kCGPathFillStroke);
    }
}

@end
