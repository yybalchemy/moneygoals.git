//
//  YYBRefreshRotateHeaderView.m
//  YYBBaseViews
//
//  Created by yyb on 2023/1/7.
//  Copyright © 2023 Univease Co., Ltd. All rights reserved.
//

#import "YYBRefreshRotateHeaderView.h"

@implementation YYBRefreshRotateHeaderView  {
    NSInteger _countingTimeInterval;
    // 该时间段内表示一次完整的动画流程
    NSInteger _maxLoopTimeInterval;
    
    CADisplayLink *_scheduler;
}

- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    _maxLoopTimeInterval = 60;
    _countingTimeInterval = _maxLoopTimeInterval;
    
    return self;
}

- (void)statusDidChanged:(YYBRefreshStatus)status {
    switch (status) {
        case YYBRefreshStatusInitial: {
            [_scheduler invalidate];
            _scheduler = nil;
            _countingTimeInterval = 0;
            [self setNeedsDisplay];
        }
            break;
        case YYBRefreshStatusPulling: {
            
        }
            break;
        case YYBRefreshStatusRefreshing: {
            _scheduler = [CADisplayLink displayLinkWithTarget:self selector:@selector(schedulerAction)];
            [_scheduler addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
            _scheduler.preferredFramesPerSecond = 60;
        }
            break;
            
        default:
            break;
    }
}

- (void)schedulerAction {
    _countingTimeInterval ++;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    
    CGFloat arcWidth = 3;
    CGFloat borderRadius = 16 - arcWidth;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextSetLineWidth(context, arcWidth);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithHexValue:0xDBDBDB].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    // 2秒2圈半，1圈半的时候最小
    // 画第一个圆弧
    CGFloat arcMargin = M_PI / 10;
    CGFloat progress = (CGFloat)(_countingTimeInterval % _maxLoopTimeInterval) / (CGFloat)_maxLoopTimeInterval;
    
    CGFloat ratio = 0.6;
    CGFloat arcOffset = progress * (5 * M_PI - arcMargin * 2);
    
    CGFloat arcEndOffset = M_PI_4;
    // 最大的弧度
    CGFloat arcMax = M_PI - arcMargin * 2;
    
    CGFloat arcStart = -M_PI + arcOffset + arcMargin;
    CGFloat arcEnd = 0;
    CGFloat arc2Start = arcOffset + arcMargin;
    
    if (progress < ratio) {
        arcEndOffset = (1 - progress / ratio) * arcMax;
        arcEnd = arcStart + arcEndOffset;
        
        CGContextAddArc(context, width / 2, height / 2, borderRadius, arcStart, arcEnd, NO);
        CGContextDrawPath(context, kCGPathFillStroke);
        
        CGFloat arc2End = arc2Start + arcEndOffset;
        CGContextAddArc(context, width / 2, height / 2, borderRadius, arc2Start, arc2End, NO);
        CGContextDrawPath(context, kCGPathFillStroke);
    } else {
        arcEndOffset = (progress - ratio) / (1 - ratio) * arcMax;
        arcEnd = arcStart - arcEndOffset;
        
        CGContextAddArc(context, width / 2, height / 2, borderRadius, arcStart, arcEnd, YES);
        CGContextDrawPath(context, kCGPathFillStroke);
        
        CGFloat arc2End = arc2Start - arcEndOffset;
        CGContextAddArc(context, width / 2, height / 2, borderRadius, arc2Start, arc2End, YES);
        CGContextDrawPath(context, kCGPathFillStroke);
    }
}

@end
