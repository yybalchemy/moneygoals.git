//
//  YYBRefreshRoundHeaderView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseHeaderView.h"
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshRoundHeaderView : YYBRefreshBaseHeaderView

@end

NS_ASSUME_NONNULL_END
