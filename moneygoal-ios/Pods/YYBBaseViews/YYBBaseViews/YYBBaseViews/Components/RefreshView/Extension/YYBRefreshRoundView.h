//
//  YYBRefreshRoundView.h
//  YYBBaseViews
//
//  Created by alchemy on 2020/9/18.
//  Copyright © 2020 Moneyease Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+YYBAdd.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYBRefreshRoundView : UIView

- (void)beginAnimation;
- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
