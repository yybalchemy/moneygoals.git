//
//  YYBRefreshSpotSheetView.h
//  YYBBaseViews
//
//  Created by alchemy on 2017/12/23.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseSheetView.h"
#import "UIColor+YYBAdd.h"

@interface YYBRefreshSpotSheetView : YYBRefreshBaseSheetView

@end
