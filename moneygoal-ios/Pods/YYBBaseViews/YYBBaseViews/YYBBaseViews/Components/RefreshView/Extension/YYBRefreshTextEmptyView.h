//
//  YYBRefreshTextEmptyView.h
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshBaseEmptyView.h"
#import <Masonry/Masonry.h>
#import "YYBCategory.h"

@interface YYBRefreshTextEmptyView : YYBRefreshBaseEmptyView

@property (nonatomic, strong, readonly) UILabel *titleLabel;

@end
