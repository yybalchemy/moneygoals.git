//
//  YYBRefreshTextEmptyView.m
//  YYBBaseViews
//
//  Created by alchemy on 2018/7/4.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshTextEmptyView.h"

@implementation YYBRefreshTextEmptyView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:14];
    _titleLabel.textColor = [UIColor colorWithHexValue:0xD6E0EF];
    _titleLabel.text = @"到底了~";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLabel];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    return self;
}

@end
