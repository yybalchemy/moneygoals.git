//
//  YYBRefreshTextSheetView.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/22.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBRefreshTextSheetView.h"
#import <Masonry/Masonry.h>

@interface YYBRefreshTextSheetView ()
@property (nonatomic,strong) UIActivityIndicatorView *activityView;
@property (nonatomic,strong) UILabel *textLabel;
@property (nonatomic,strong) NSMutableDictionary *stateKeys;

@end
@implementation YYBRefreshTextSheetView

- (instancetype)initWithScrollView:(UIScrollView *)scrollView {
    self = [super initWithScrollView:scrollView];
    if (!self) return nil;
    
    [self addSubview:self.textLabel];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
    
    [self addSubview:self.activityView];
    [self.activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
    
    [self _refreshInitialize];
    
    return self;
}

- (void)renderViewWithTitle:(NSString *)title status:(YYBRefreshStatus)status {
    if (!title) return;
    [_stateKeys setValue:title forKey:YYBRefreshStatusString(status)];
}

- (void)_refreshInitialize {
    self.isAllowHandleOffset = NO;
    _stateKeys = [[NSMutableDictionary alloc] initWithDictionary:@{YYBRefreshStatusString(YYBRefreshStatusInitial) : BTRV_BOTTOM_INITIALIZE,
                                                                   YYBRefreshStatusString(YYBRefreshStatusPulling) : BTRV_BOTTOM_PULLING,
                                                                   YYBRefreshStatusString(YYBRefreshStatusRefreshing) : BTRV_BOTTOM_REFRESHING}];
    _textLabel.text = [self keyTitleAttachState:self.status];
}

- (void)statusDidChanged:(YYBRefreshStatus)status {
    _textLabel.text = [self keyTitleAttachState:status];
    switch (status) {
        case YYBRefreshStatusInitial: {
            _textLabel.hidden = NO;
            [_activityView stopAnimating];
        }
            break;
        case YYBRefreshStatusPulling: {
            
        }
            break;
        case YYBRefreshStatusRefreshing: {
            _textLabel.hidden = YES;
            [_activityView startAnimating];
        }
            break;
            
        default:
            break;
    }
}

- (NSString *)keyTitleAttachState:(YYBRefreshStatus)state {
    NSString *key = YYBRefreshStatusString(state);
    if ([_stateKeys.allKeys containsObject:key]) {
        return [_stateKeys objectForKey:key];
    }
    return @"";
}

- (UIActivityIndicatorView *)activityView {
    if (!_activityView) {
        _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    return _activityView;
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = [UIFont fontWithName:@"Lao Sangam MN" size:14];
        _textLabel.textColor = [UIColor colorWithRed:87.0/255.0 green:87.0/255.0 blue:87.0/255.0 alpha:1];
    }
    return _textLabel;
}

@end
