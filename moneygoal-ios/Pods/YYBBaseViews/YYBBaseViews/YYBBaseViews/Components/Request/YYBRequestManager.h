//
//  YYBRequestManager.h
//  YYBBaseViews
//
//  Created by Aokura on 2018/7/31.
//  Copyright © 2018年 Moneyease Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef void (^ YYBResponseSuccBlock)(id data, NSDictionary *params);
typedef void (^ YYBResponseErrorBlock)(NSError *error, NSDictionary *params);

typedef void (^ YYBRequestConfigBlock)(AFHTTPSessionManager *manager, NSString *URLString);
typedef void (^ YYBRequestConfigResultsBlock)(id data, NSError *error, NSDictionary *params, NSString *path, YYBResponseSuccBlock succBlock, YYBResponseErrorBlock errorBlock);
typedef BOOL (^ YYBRequestConfigResultsValidBlock)(id data, NSError *error, NSDictionary *params, NSString *path, YYBResponseSuccBlock succBlock, YYBResponseErrorBlock errorBlock);

@interface YYBRequestManager : NSObject

+ (YYBRequestManager *)shared;
@property (nonatomic) BOOL enableLog;

@property (nonatomic, copy) YYBRequestConfigBlock configManagerBlock;
@property (nonatomic, copy) YYBRequestConfigResultsValidBlock configResultsBlock;

- (void)handleRequestWithMethod:(NSString *)method URLString:(NSString *)URLString params:(NSDictionary *)params
             configManagerBlock:(YYBRequestConfigBlock)configManagerBlock
             configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock
                   successBlock:(YYBResponseSuccBlock)successBlock
                     errorBlock:(YYBResponseErrorBlock)errorBlock;

- (void)handleRequestWithMethod:(NSString *)method URLString:(NSString *)URLString params:(id)params paramsInBody:(BOOL)paramsInBody
             configManagerBlock:(YYBRequestConfigBlock)configManagerBlock
             configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock
                   successBlock:(YYBResponseSuccBlock)successBlock
                     errorBlock:(YYBResponseErrorBlock)errorBlock;

// Form表单上传图片
// 默认可以上传1024KB一张图
// 如果实现了appendImageDataBlock函数就不会自动拼接
- (void)handleFormDataWithURLString:(NSString *)URLString params:(NSDictionary *)params images:(NSArray *)images
                    appendImageDataBlock:(void (^)(id<AFMultipartFormData> formData))appendImageDataBlock
                     successBlock:(YYBResponseSuccBlock)successBlock
                       errorBlock:(YYBResponseErrorBlock)errorBlock
               configResultsBlock:(YYBRequestConfigResultsValidBlock)configResultsBlock;

+ (NSData *)compressImage:(UIImage *)image toMaxFileSize:(NSInteger)maxFileSize;

@end
