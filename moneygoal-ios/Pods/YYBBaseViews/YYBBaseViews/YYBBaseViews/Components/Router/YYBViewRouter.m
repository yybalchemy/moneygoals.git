//
//  YYBViewRouter.m
//  YYBBaseViews
//
//  Created by alchemy on 2017/11/24.
//  Copyright © 2017年 Moneyease Co., Ltd. All rights reserved.
//

#import "YYBViewRouter.h"

@implementation YYBViewRouter

+ (YYBViewRouter *)shared {
    static YYBViewRouter *router = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[YYBViewRouter alloc] init];
    });
    return router;
}

- (NSString *)_checkMapedViewController:(NSString *)domain {
    if (!domain) return nil;
    if (self.mapppedBlock) {
        NSString *mapped = self.mapppedBlock(domain);
        if (mapped != nil) {
            return mapped;
        }
    }
    
    if (!_mappedDictionary || _mappedDictionary.count == 0) return nil;
    if ([_mappedDictionary.allKeys containsObject:domain]) {
        return [_mappedDictionary objectForKey:domain];
    }
    
    return nil;
}

+ (void)openURL:(NSString *)URLString {
    [self openURL:URLString configureBlock:^(id  _Nonnull routerObject) {
        
    }];
}

+ (void)openURL:(NSString *)URLString configureBlock:(void (^)(id))configureBlock {
    [self openURL:URLString configureBlock:configureBlock appearType:YYBRouterAppearTypePush];
}

+ (void)openURL:(NSString *)URLString configureBlock:(void (^)(id))configureBlock appearType:(YYBRouterAppearType)appearType {
    [self openURL:URLString configureBlock:configureBlock parameters:nil isAnimated:YES appearType:appearType navigationBlock:nil];
}

+ (void)openURL:(NSString *)URLString configureBlock:(void(^)(id))configureBlock parameters:(NSDictionary *)parameters isAnimated:(BOOL)isAnimated appearType:(YYBRouterAppearType)appearType navigationBlock:(nullable void (^)(UINavigationController *))navigationBlock {
    if ([URLString hasPrefix:YYBRouterProtocol]) { // 是否协议包含协议
        NSString *domain = [URLString substringFromIndex:YYBRouterProtocol.length];
        NSString *controller = [[YYBViewRouter shared] _checkMapedViewController:domain];
        if (controller) {
            UIViewController *topViewController = [self topViewController];
            UIViewController *vc = [self _generateViewControllerWithClassType:controller parameters:parameters];
            if (configureBlock) {
                configureBlock(vc);
            }

            if (vc) {
                [self _animateViewController:vc actionViewController:topViewController isAnimated:isAnimated appearType:appearType navigationBlock:navigationBlock];
            }
        }
    }
}

+ (void)openOnClass:(Class)className {
    [self openOnClass:className configureBlock:^(id  _Nonnull routerObject) {
        
    }];
}

+ (void)openOnClass:(Class)className configureBlock:(void (^)(id))configureBlock {
    [self openOnClass:className configureBlock:configureBlock isAnimated:YES appearType:YYBRouterAppearTypePush navigationBlock:^(UINavigationController * _Nonnull nav) {
        
    }];
}

+ (void)openFullScreenPresentPageWithClass:(Class)className configureBlock:(void (^)(id))configureBlock {
    [self openOnClass:className configureBlock:configureBlock isAnimated:YES appearType:YYBRouterAppearTypePresentWithNavigation navigationBlock:^(UINavigationController *nav) {
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
    }];
}

+ (void)openOnClass:(Class)className configureBlock:(void (^)(id))configureBlock isAnimated:(BOOL)isAnimated appearType:(YYBRouterAppearType)appearType navigationBlock:(void (^)(UINavigationController *))navigationBlock {
    UIViewController *topViewController = [self topViewController];
    UIViewController *vc = [[className alloc] init];
    
    if (configureBlock) {
        configureBlock(vc);
    }
    
    if (vc) {
        [self _animateViewController:vc actionViewController:topViewController isAnimated:isAnimated appearType:appearType navigationBlock:navigationBlock];
    }
}

+ (UIViewController *)_generateViewControllerWithClassType:(NSString *)classType parameters:(NSDictionary *)parameters {
    if (classType == nil && parameters == nil) {
        return nil;
    }
    
    id controller = [[NSClassFromString(classType) alloc] init];
    if (controller) {
        if (parameters && parameters.allKeys.count > 0) {
            [controller assignValues:parameters];
        }
    }
    return controller;
}

+ (void)_animateViewController:(UIViewController *)controller actionViewController:(UIViewController *)actionViewController isAnimated:(BOOL)isAnimated appearType:(YYBRouterAppearType)appearType navigationBlock:(nullable void (^)(UINavigationController *))navigationBlock {
    switch (appearType) {
        case YYBRouterAppearTypePush: {
            dispatch_async(dispatch_get_main_queue(), ^{
                [actionViewController.navigationController pushViewController:controller animated:isAnimated];
            });
        }
            break;
        case YYBRouterAppearTypePresent: {
            [actionViewController presentViewController:controller animated:isAnimated completion:nil];
        }
            break;
        case YYBRouterAppearTypePresentWithNavigation: {
            UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:controller];
            if (navigationBlock) {
                navigationBlock(navi);
            }
            
            [actionViewController presentViewController:navi animated:isAnimated completion:nil];
        }
            break;
        default:
            break;
    }
}

+ (void)dismissViewControllers:(NSInteger)pageNumber {
    UINavigationController *navi = [self topViewController].navigationController;
    NSArray *vcs = [navi viewControllers];
    if (vcs.count > pageNumber) {
        UIViewController *vc = [vcs objectAtIndex:vcs.count - pageNumber - 1];
        [navi popToViewController:vc animated:YES];
    }
}

+ (void)dismissToViewController:(NSString *)URLString {
    if ([URLString hasPrefix:YYBRouterProtocol]) { // 是否协议包含协议
        NSString *domain = [URLString substringFromIndex:YYBRouterProtocol.length];
        NSString *controller = [[YYBViewRouter shared] _checkMapedViewController:domain];
        if (controller) {
            UIViewController *top = [self topViewController];
            NSArray *controllers = top.navigationController.viewControllers;
            
            UIViewController *searched = nil;
            for (UIViewController *scan in controllers) {
                if ([NSStringFromClass([scan class]) isEqualToString:NSStringFromClass([controller class])]) {
                    searched = scan;
                    break;
                }
            }
            
            [top.navigationController popToViewController:searched animated:YES];
        }
    }
}

+ (NSString *)stringFromViewControllerName:(NSString *)name {
    return [NSString stringWithFormat:@"%@%@",name,YYBRouterClassSuffix];
}

+ (UIViewController *)topViewController {
    UIViewController *resultVC;
    Class UIApplicationClass = NSClassFromString(@"UIApplication");
    BOOL hasApplication = UIApplicationClass && [UIApplicationClass respondsToSelector:@selector(sharedApplication)];
    if (hasApplication) {
        UIApplication * app = [UIApplicationClass performSelector:@selector(sharedApplication)];
        
        resultVC = [self _topViewController:[app.keyWindow rootViewController]];
        while (resultVC.presentedViewController) {
            resultVC = [self _topViewController:resultVC.presentedViewController];
        }
    }
    return resultVC;
}

+ (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

+ (void)dismissModalViewControllers {
    [self dismissModalViewControllersWithAnimation:YES];
}

+ (void)dismissModalViewControllersWithAnimation:(BOOL)animation {
    UIViewController *presentingVC = [self topViewController].presentingViewController;
    while (presentingVC.presentingViewController) {
        presentingVC = presentingVC.presentingViewController;
    }
    if (presentingVC) {
        [presentingVC dismissViewControllerAnimated:animation completion:nil];
    }
}

+ (void)dismissModalViewControllerUntilClass:(Class)aCalss {
    UIViewController *presentingVC = [self topViewController];
    
    while (presentingVC.presentingViewController) {
        UIViewController *vc = presentingVC.presentingViewController;
        
        if ([vc isKindOfClass:[UINavigationController class]]) {
            UIViewController *firstViewController = [(UINavigationController *)vc viewControllers].firstObject;
            if (firstViewController && firstViewController.class == aCalss) {
                presentingVC = presentingVC.presentingViewController;
                break;
            }
        } else if ([vc isKindOfClass:[UIViewController class]]) {
            if (aCalss && vc.class == aCalss) {
                presentingVC = presentingVC.presentingViewController;
                break;
            }
        }
        
        presentingVC = presentingVC.presentingViewController;
    }
    
    if (presentingVC) {
        [presentingVC dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
